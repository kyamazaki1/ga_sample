import { renderHook, act } from "@testing-library/react-hooks";
import { useToggle } from "./useToggle";
import { useRerender } from "./useRerender";

test("return new state", async () => {
  let value = { mock: "test" };
  let newState = {
    mock: "udapted"
  };
  let updated;
  const { result, rerender, waitForNextUpdate } = renderHook(() => useRerender(value, state => state));

  act(() => {
    updated = renderHook(() => useRerender(newState, state => state));
  });
  rerender();

  expect(updated.result.current[0]).toMatchObject(newState);
});
