import { renderHook, act } from "@testing-library/react-hooks";

import { useLatestValue } from "./useLatestValue";
import { useDispachToProps } from "./useDispatchToProps";

test("apply dispatcher", () => {
  const payload = ["babou"];
  const func = jest.fn();

  const { result, rerender, waitForNextUpdate } = renderHook(() => useDispachToProps(payload, func));
  rerender();
  expect(func).toBeCalled();
  expect(func).toHaveBeenCalledTimes(1);
});
test("not apply dispatcher on empty list", () => {
  const payload = [];
  const func = jest.fn();
  const { result, rerender, waitForNextUpdate } = renderHook(() => useDispachToProps(payload, func));
  rerender();
  expect(func).not.toBeCalled();
});

test("apply format data if available", () => {
  const payload = [{ mock: "test" }];
  const func = jest.fn();
  const formatData = jest.fn();

  const { result, rerender, waitForNextUpdate } = renderHook(() => useDispachToProps(payload, func, formatData));
  rerender();
  expect(formatData).toBeCalled();
  expect(formatData).toHaveBeenCalledTimes(1);
});
