import * as React from "react";
import { Fragment } from "react";
import { Grid, Paper, Typography } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { trySetUserDetailAction } from "@home/store";

interface AppComponentProps {
  //  fetchDemo?: Function;
  setAppTheme?: Function;
  //DemoReducer?: any;
  app?: any;
}
let App = (props: AppComponentProps): JSX.Element => {
  const {} = props;

  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(trySetUserDetailAction());
  }, []);

  return (
    <Fragment>
      <Grid item xs={12}>
        <Typography color="inherit" variant="h2" data-testid="app-title">
          App Page
        </Typography>
      </Grid>
      <Grid item xs={12}></Grid>
    </Fragment>
  );
};

export default App;
