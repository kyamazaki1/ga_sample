import * as React from "react";
import App from "./App.container";
import { cleanup, render } from "@testing-library/react";
import { renderWithRedux, testIdGetter } from "@mock/utils.mock";

describe("Access to app", () => {
  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);
  test("It should renders the app title", () => {
    const { getByTestId } = renderWithRedux(<App />);
    const { appTitle } = testIdGetter(["appTitle", "app-title"], getByTestId);
    expect(appTitle.textContent).toBe("App Page");
  });
});
