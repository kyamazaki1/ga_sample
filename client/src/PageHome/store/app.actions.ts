import { UserResponse } from "../../model/user";
import { Partial } from "../../model/utils";
import {
  TOGGLE_THEME,
  SET_USER,
  REFRESH_TOKEN,
  TRY_CLEAR_TOKEN,
  CLEAR_TOKEN,
  TRY_USER_DETAIL,
  SET_USER_DETAIL,
  TRY_BUSINESS_DETAIL,
  SET_BUSINESS_DETAIL
} from "./app.actionTypes";

// action creators
export const setAppThemeAction = payload => ({ type: TOGGLE_THEME, payload });

export const setAppUserAction = payload => ({ type: SET_USER, payload });
//token
export const refreshUserTokenAction = payload => ({ type: REFRESH_TOKEN, payload });

export const tryClearUserTokenAction = (payload: Partial<UserResponse>) => ({ type: TRY_CLEAR_TOKEN, payload });
export const clearUserTokenAction = () => ({ type: CLEAR_TOKEN, payload: null });

export const trySetUserDetailAction = () => ({ type: TRY_USER_DETAIL, payload: null });
export const setUserDetailAction = payload => ({ type: SET_USER_DETAIL, payload });

export const trySetBusinessDetailAction = () => ({ type: TRY_BUSINESS_DETAIL, payload: null });
export const setBusinessDetailAction = payload => ({ type: SET_BUSINESS_DETAIL, payload });
