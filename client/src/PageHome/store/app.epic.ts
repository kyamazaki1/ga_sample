import { UserResponse } from "../../model/user";
import { autoRefreshGet, clearPersistToken, revokeUserToken } from "@src/store/store.utils";
import { clearUserTokenAction, setUserDetailAction, setBusinessDetailAction } from "./app.actions";
import { mergeMap, map, tap, filter, concatMap } from "rxjs/operators";
import { ofType, StateObservable } from "redux-observable";
import { TRY_CLEAR_TOKEN, TRY_USER_DETAIL, TRY_BUSINESS_DETAIL } from "./app.actionTypes";
import { DispatchAction } from "@model/index";
import { Observable } from "rxjs";
import { USER_GET_API, BUSINESS_GET_API } from "@shared/const";
import { ajax } from "rxjs/ajax";

export const clearTokenEpic$ = (action$, state$: StateObservable<any>, client = ajax): Observable<void> =>
  action$.pipe(
    ofType(TRY_CLEAR_TOKEN),
    concatMap((action: DispatchAction<Partial<UserResponse>>) =>
      revokeUserToken(action.payload, client).pipe(
        tap(() => clearPersistToken()),
        map(_ => clearUserTokenAction())
      )
    )
  );

export const userDetailEpic$ = (action$: any, state$: StateObservable<any>, client = ajax): Observable<void> =>
  action$.pipe(
    ofType(TRY_USER_DETAIL),
    mergeMap((action: any) => {
      return autoRefreshGet(state$, USER_GET_API, setUserDetailAction, client);
    })
  );

export const businessDetailEpic$ = (action$: any, state$: StateObservable<any>, client = ajax): Observable<void> =>
  action$.pipe(
    ofType(TRY_BUSINESS_DETAIL),
    mergeMap((action: any) => {
      return autoRefreshGet(state$, BUSINESS_GET_API, setBusinessDetailAction, client);
    })
  );
