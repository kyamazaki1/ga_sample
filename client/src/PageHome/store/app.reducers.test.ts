import { MOCK_APP_STATE, UPDATED_APP_USER, MOCK_INIT_APP_STATE, UPDATED_TOKEN_USER } from "../../fixtures/index";
import { AppReducer } from "./app.reducers";
import {
  setAppThemeAction,
  setAppUserAction,
  refreshUserTokenAction,
  setUserDetailAction,
  setBusinessDetailAction,
  clearUserTokenAction
} from "./app.actions";

afterEach(() => {
  jest.clearAllMocks();
});

describe("app reducer", () => {
  it("apply theme ", () => {
    const updatedReducer = AppReducer(MOCK_APP_STATE, setAppThemeAction("light"));
    const expected = { ...MOCK_APP_STATE, theme: "light" };
    expect(updatedReducer).toMatchObject(expected);
    expect(updatedReducer.theme).toBe("light");
  });

  it("update user ", () => {
    const updatedReducer = AppReducer(MOCK_APP_STATE, setAppUserAction(UPDATED_APP_USER));
    const expected = { ...MOCK_APP_STATE, user: UPDATED_APP_USER };
    expect(updatedReducer).toMatchObject(expected);
    expect(updatedReducer.user.username).toBe(UPDATED_APP_USER.username);
  });

  it("update token", () => {
    const updatedReducer = AppReducer(MOCK_APP_STATE, refreshUserTokenAction(UPDATED_TOKEN_USER));
    const expected = { ...MOCK_APP_STATE, user: UPDATED_TOKEN_USER };
    expect(updatedReducer).toMatchObject(expected);
    expect(updatedReducer.user.access_token).toBe(UPDATED_TOKEN_USER.access_token);
  });

  it("set user etail", () => {
    const FAKE_USER_DETAIL = { user: "mock" };
    const updatedReducer = AppReducer(MOCK_APP_STATE, setUserDetailAction(FAKE_USER_DETAIL));
    const expected = { ...MOCK_APP_STATE, userDetail: FAKE_USER_DETAIL };
    expect(updatedReducer).toMatchObject(expected);
    expect(updatedReducer.userDetail).toBe(FAKE_USER_DETAIL);
  });
  it("set business detail", () => {
    const FAKE_BUSINESS = { user: "mock" };
    const updatedReducer = AppReducer(MOCK_APP_STATE, setBusinessDetailAction(FAKE_BUSINESS));
    const expected = { ...MOCK_APP_STATE, business: FAKE_BUSINESS };
    expect(updatedReducer).toMatchObject(expected);
    expect(updatedReducer.business).toBe(FAKE_BUSINESS);
  });

  it("clear app", () => {
    const updatedReducer = AppReducer(MOCK_APP_STATE, clearUserTokenAction());
    expect(updatedReducer).toMatchObject(MOCK_INIT_APP_STATE);
  });
});
