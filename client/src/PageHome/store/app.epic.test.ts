import { businessDetailEpic$, clearTokenEpic$, userDetailEpic$ } from "./app.epic";
import { MOCK_STORE_STATE, MOCK_STORE_STATE_FRESH } from "../../fixtures/index";
import { TRY_USER_DETAIL, TRY_BUSINESS_DETAIL, TRY_CLEAR_TOKEN, CLEAR_TOKEN, TOGGLE_THEME } from "./app.actionTypes";

import { of, Observable, Subject } from "rxjs";
import {
  trySetUserDetailAction,
  setUserDetailAction,
  refreshUserTokenAction,
  setBusinessDetailAction,
  clearUserTokenAction,
  setAppThemeAction
} from "./app.actions";
import { TestScheduler } from "rxjs/testing";

/* const MockuserDetailEpic = (action$: any, state$: StateObservable<any>, client = ajax): Observable<void> =>
  action$.pipe(
    ofType(TRY_USER_DETAIL),
    mergeMap((action: any) => {
      return autoRefreshGet(state$, USER_GET_API, setUserDetailAction, client);
    })
  );
 */
describe("epic app actions ", () => {
  // automatically unmount and cleanup DOM after the test is finished.

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe("when accesssing a protected", () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it("refresh token action to have bell called on token below threshold", done => {
      const testScheduler = new TestScheduler((actual, expected) => {
        // somehow assert the two objects are equal
        // e.g. with chai `expect(actual).deep.equal(expected)`
        const refresTkAction = actual[0].notification.value;
        expect(refresTkAction).toEqual(refreshUserTokenAction(MOCK_STORE_STATE.app.user));
        expect(actual.length).toBe(2);
      });

      testScheduler.run(({ hot, expectObservable }) => {
        const action$ = hot("-a", {
          a: { type: TRY_USER_DETAIL }
        });
        const storeState = MOCK_STORE_STATE;
        const userStae = storeState.app.user;
        const state$ = { value: storeState };
        const mockAjax = () => of({ response: userStae });
        const output$ = userDetailEpic$(action$, state$ as any, mockAjax as any);

        expectObservable(output$).toBe("-a", {
          a: setUserDetailAction(userStae)
        });

        done();
      });
    });

    it("fetch user details when token is valid", done => {
      const testScheduler = new TestScheduler((actual, expected) => {
        // somehow assert the two objects are equal
        expect(actual.length).toBe(1);
        expect(actual).toEqual(expected);
      });

      testScheduler.run(({ hot, expectObservable }) => {
        const action$ = hot("-a", {
          a: { type: TRY_USER_DETAIL }
        });
        const storeState = MOCK_STORE_STATE_FRESH;
        const state$ = { value: storeState };
        const userState = storeState.app.user;
        const mockAjax = () => of({ response: userState });
        const output$ = userDetailEpic$(action$, state$ as any, mockAjax as any);
        expectObservable(output$).toBe("-a", {
          a: setUserDetailAction(userState)
        });

        done();
      });
    });

    it("fetch business details when token is valid", done => {
      const testScheduler = new TestScheduler((actual, expected) => {
        // somehow assert the two objects are equal
        expect(actual.length).toBe(1);
        expect(actual).toEqual(expected);
      });

      testScheduler.run(({ hot, expectObservable }) => {
        const action$ = hot("-a", {
          a: { type: TRY_BUSINESS_DETAIL }
        });
        const storeState = MOCK_STORE_STATE_FRESH;
        const state$ = { value: storeState };
        const userState = storeState.app.business;
        const mockAjax = () => of({ response: null });
        const output$ = businessDetailEpic$(action$, state$ as any, mockAjax as any);
        expectObservable(output$).toBe("-a", {
          a: setBusinessDetailAction(userState)
        });
        done();
      });
    });
    it("clear token ", done => {
      const testScheduler = new TestScheduler((actual, expected) => {
        // somehow assert the two objects are equal
        expect(actual[0].notification.value.type).toEqual(CLEAR_TOKEN);
        expect(actual).toEqual(expected);
      });

      testScheduler.run(({ hot, expectObservable }) => {
        const action$ = hot("-a", {
          a: { type: TRY_CLEAR_TOKEN, payload: MOCK_STORE_STATE_FRESH.app.user }
        });
        const storeState = MOCK_STORE_STATE_FRESH;

        const state$ = { value: storeState };

        const mockAjax = () => of({ response: null });

        const output$ = clearTokenEpic$(action$, state$ as any, mockAjax as any);

        expectObservable(output$).toBe("-a", {
          a: clearUserTokenAction()
        });

        done();
      });
    });
  });
});
