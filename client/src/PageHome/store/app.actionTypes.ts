export const TOGGLE_THEME = "TOGGLE_THEME";
export const SET_USER = "SET_USER";
//user

//token
export const TRY_REFRESH_TOKEN = "TRY_REFRESH_TOKEN";
export const REFRESH_TOKEN = "REFRESH_TOKEN";

export const CLEAR_TOKEN = "CLEAR_TOKEN";
export const TRY_CLEAR_TOKEN = "TRY_CLEAR_TOKEN";
// User detail

export const SET_USER_DETAIL = "SET_USER_DETAIL";
export const TRY_USER_DETAIL = "TRY_USER_DETAIL";

//business
export const SET_BUSINESS_DETAIL = "SET_BUSINESS_DETAIL";
export const TRY_BUSINESS_DETAIL = "TRY_BUSINESS_DETAIL";
