import { MOCK_APP_USER } from "../../fixtures/index";
import { MOCK_APP_STATE } from "@src/fixtures";
import { UserResponse, INIT_USER_RESPONSE } from "@model/user";
import {
  TOGGLE_THEME,
  SET_USER,
  REFRESH_TOKEN,
  CLEAR_TOKEN,
  SET_USER_DETAIL,
  SET_BUSINESS_DETAIL
} from "./app.actionTypes";
import { DispatchAction } from "@model/index";
import produce from "immer";
type theme = "dark" | "light";
export interface AppState {
  theme: theme;
  user: UserResponse;
  userDetail: any;
  business: any;
}

export const initialAppState: AppState = {
  theme: "dark",
  user: INIT_USER_RESPONSE,
  userDetail: null,
  business: null
};

function appReducer(state = initialAppState, action: DispatchAction<any>): AppState {
  return produce(state, draftState => {
    const { payload } = action;
    switch (action.type) {
      case TOGGLE_THEME: {
        draftState.theme = payload;
        return;
      }
      case SET_USER:
      case REFRESH_TOKEN: {
        draftState.user = payload;
        return;
      }

      case SET_USER_DETAIL: {
        draftState.userDetail = payload;
        return;
      }

      case SET_BUSINESS_DETAIL: {
        draftState.business = payload;
        return;
      }

      case CLEAR_TOKEN: {
        draftState = null;
        return;
      }
    }
  });
}

export { appReducer as AppReducer };
