export const API_ROOT: string = process.env.REACT_APP_API_URI;
//auth
export const GET_TOKEN_API = `${API_ROOT}auth/signin`;
export const REFRESH_TOKEN_API = `${API_ROOT}auth/refresh-token/`;

export const REVOKE_TOKEN_API = `${API_ROOT}account/revoke-token/`;

//user

export const USER_API: string = `${API_ROOT}api/account/user/`;
export const USER_ADD_API: string = `${USER_API}create/`;
export const USER_GET_API: string = `${API_ROOT}api/account/`;

//business
export const BUSINESS_API: string = `${API_ROOT}api/account/business/`;
export const BUSINESS_ADD_API: string = `${BUSINESS_API}create/`;
export const BUSINESS_GET_API: string = `${BUSINESS_API}detail/`;
export const BUSINESS_LIST_API: string = `${BUSINESS_API}list/`;
//product
export const CATALOG_API: string = `${API_ROOT}api/product/catalogs/`;

export const SETUPS_API: string = `${API_ROOT}api/product/setups/`;
//template
export const TEMPLATE_API: string = `${API_ROOT}api/product/landing-pages/`;
export const TEMPLATE_ITEM_API: string = `${API_ROOT}api/product/landing-page/`;
//insight
export const INSIGHT_API: string = `${API_ROOT}api/product/insight/`;

//template
export const LP_API: string = `${API_ROOT}api/product/landing-pages-titles/`;

//
export const PAGINATION_CHUNK: number = 10;
