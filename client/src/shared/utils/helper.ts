export function safeGet<T extends Object>(value: Object, ...path: string[]): T {
  return path.reduce((prev: Object, prop: string) => {
    if (prev && !!prev[prop]) {
      return prev[prop];
    } else {
      return null;
    }
  }, value);
}
/**
 * return string without space && upper case
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function normalizeStr(str: string): string {
  return str
    .replace(/\s+/g, "")
    .trim()
    .toUpperCase();
}

//forms

export const isEmpty = (val: string): boolean => (val ? val.trim() === "" : true);
export const isValidUrl = (u: string): boolean =>
  u
    ? !!u.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)
    : false;

export function isEquivalent(a: Object, b: Object): boolean {
  // Create arrays of property names
  const aProps = Object.getOwnPropertyNames(a);
  const bProps = Object.getOwnPropertyNames(b);

  // If number of properties is different,
  // objects are not equivalent
  if (aProps.length != bProps.length) {
    return false;
  }

  for (var i = 0; i < aProps.length; i++) {
    const propName = aProps[i];

    // If values of same property are not equal,
    // objects are not equivalent
    //console.log(  a[propName], b[propName] propName, a[propName], b[propName]);

    if (typeof a[propName] === "object" && typeof a[propName] === "object") {
      return isEquivalent(a[propName], b[propName]);
    }
    if (a[propName] !== b[propName]) {
      return false;
    }
  }

  // If we made it this far, objects
  // are considered equivalent
  return true;
}
/**
 * simple comparison of list objects
 *
 * @export
 * @template T
 * @template S
 * @param {Array<T>} sourceArr
 * @param {Array<S>} newArr
 * @param {Function} [objCompare=isEquivalent]
 * @returns {boolean}
 */
export function IsArrayObjectEqual<T, S>(
  sourceArr: Array<T>,
  newArr: Array<S>,
  objCompare: Function = isEquivalent
): boolean {
  // Get the value type
  const type = Object.prototype.toString.call(sourceArr);
  // If the two objects are not the same type, return false
  if (type !== Object.prototype.toString.call(newArr)) return false;
  // If items are not an object or array, return false
  if (["[object Array]", "[object Object]"].indexOf(type) < 0) return false;
  if (sourceArr.length !== newArr.length) return false;

  const equals = sourceArr.reduce((accu, curr) => newArr.filter(n => objCompare(n, curr)).length > 0, true);
  return equals;
}

/** date  */

export type FormatTime = "scd" | "min";
/**
 * get remaining time between time and now by unit (scd or min)
 *
 * @export
 * @param {string} time
 * @param {FormatTime} format
 * @returns {number}
 */
export function getRemainTime(time: string, format: FormatTime): number {
  const mapFomart = {
    min: 60000,
    scd: 1000
  };
  const _diff = diffDate(time);
  const unit = mapFomart.hasOwnProperty(format) ? mapFomart[format] : 1;
  return Math.floor(_diff / unit);
}

function diffDate(end: string): number {
  const startDate = new Date().getTime();
  const endDate = new Date(end).getTime();
  return endDate - startDate;
}
/**
 * flatten all object values into single object with path and value
 * not really usefuk but fun to code
 * @param {*} obj
 * @param {*} [acc={ nodes: [], values:[] }]
 * @returns
 */
export function flattenObjValues(
  obj: Object,
  acc: { nodes: Array<string>; values: Array<{ value: any; path: string }> } = { nodes: [], values: [null] }
): { values: Array<{ value: any; path: string }> } {
  const props = Object.keys(obj);
  if (props.length === 0 || typeof obj !== "object") {
    return { values: [] };
  }
  return props.reduce(
    (accu: { nodes?: Array<string>; values: Array<{ value: any; path: string }> }, curr: string) => {
      if (typeof obj[curr] !== "object") {
        return { values: [...(accu.values as any), ...[{ path: [...acc.nodes, ...[curr]], value: obj[curr] }]] };
      }
      return flattenObjValues(obj[curr], { nodes: [...acc.nodes, ...[curr]], values: [...(accu.values as any)] });
    },
    acc as any
  );
}
