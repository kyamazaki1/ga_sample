import { BASE_MOCK_FORM_CONTROL, BASE_INPUT_STATE, MOCK_APP_STATE } from "./../../fixtures/index";
import {
  safeGet,
  normalizeStr,
  isEmpty,
  isValidUrl,
  isEquivalent,
  IsArrayObjectEqual,
  getRemainTime,
  flattenObjValues
} from "./helper";
import {
  displayErrorHint,
  ERROR_MSG,
  uiValidation,
  validate,
  controlsAreValid,
  makeFormState,
  formsCtrlsToPayload,
  formStateToValue,
  formatInputState
} from "./form";

const VALID_INPUT_STATE = { value: true, message: null };

describe("utils", () => {
  it("return object value giben right path", () => {
    const OBJ = {
      nested: {
        inner: "nested"
      }
    };
    const value = safeGet(OBJ, "nested", "inner");
    expect(value).toBe(OBJ.nested.inner);
  });

  it("return null given wrong path", () => {
    const OBJ = {
      nested: {
        inner: "nested"
      }
    };
    const value = safeGet(OBJ, "nested", "nested");

    expect(value).toBe(null);
  });

  it("return normalizestring and remove space", () => {
    const STR = " Fa Y ";
    const expected = "FAY";
    const value = normalizeStr(STR);
    expect(value).toBe(expected);
  });

  it("return true on empty sring", () => {
    const STR = "";
    const value = isEmpty(STR);
    expect(value).toBeTruthy();
  });
  it("return false on non empty sring", () => {
    const STR = "babou";
    const value = isEmpty(STR);
    expect(value).toBeFalsy();
  });

  it("return true on valid url", () => {
    const URL = "https://react-testing-examples.com/";
    const value = isValidUrl(URL);
    expect(value).toBeTruthy();
  });
  it("return false on invalid url", () => {
    const URL = "hts://react-testing-examples";
    const value = isValidUrl(URL);
    expect(value).toBeFalsy();
  });

  it("return true if objects have same value", () => {
    const A = {
      nested: {
        inner: "nested"
      }
    };
    const B = {
      nested: {
        inner: "nested"
      }
    };
    const value = isEquivalent(A, B);
    expect(value).toBeTruthy();
  });

  it("return false if objects have different value", () => {
    const A = {
      nested: {
        inner: "nested"
      }
    };
    const B = {
      nested: {
        inner: "babou"
      }
    };
    const value = isEquivalent(A, B);
    expect(value).toBeFalsy();
  });

  it("return true if Array objects have same value", () => {
    const A = [
      {
        nested: "nested"
      },
      {
        some: "some"
      }
    ];
    const B = [
      {
        nested: "nested"
      },
      {
        some: "some"
      }
    ];
    const value = IsArrayObjectEqual(A, B);
    expect(value).toBeTruthy();
  });

  it("return false if Array objects have different value", () => {
    const A = [
      {
        nested: "nested"
      },
      {
        some: "some"
      }
    ];
    const B = [
      {
        nested: "nested"
      },
      {
        some: "babou"
      }
    ];
    const value = IsArrayObjectEqual(A, B);
    expect(value).toBeFalsy();
  });
  it("return false if Array objects have different length", () => {
    const A = [
      {
        nested: "nested"
      },
      {
        some: "some"
      }
    ];
    const B = [
      {
        nested: "nested"
      }
    ];
    const value = IsArrayObjectEqual(A, B);
    expect(value).toBeFalsy();
  });
  it("return false if Array objects have different type", () => {
    const A = [
      {
        nested: "nested"
      }
    ];
    const B = [
      {
        nested: true
      }
    ];
    const value = IsArrayObjectEqual(A, B);
    expect(value).toBeFalsy();
  });

  it("return remaining time in minutes", () => {
    const REMAINING = 2;

    let START_DATE = new Date();
    START_DATE.setMinutes(new Date().getMinutes() + REMAINING);
    const value = getRemainTime(START_DATE.toISOString(), "min");

    expect(value).toBeLessThanOrEqual(REMAINING);
  });

  it("return remaining time in seconds", () => {
    const REMAINING = 10;

    let START_DATE = new Date();
    START_DATE.setSeconds(new Date().getSeconds() + REMAINING);
    const value = getRemainTime(START_DATE.toISOString(), "scd");

    expect(value).toBeLessThanOrEqual(REMAINING);
  });

  it("return error message", () => {
    let PARTIAL_INPUT_SATE = {
      state: {
        errorDisplay: "required"
      }
    };
    const value = displayErrorHint(<any>PARTIAL_INPUT_SATE);

    expect(value).toBe(ERROR_MSG.required);
  });

  it("return true if input has not been touched", () => {
    let PARTIAL_INPUT_SATE = {
      state: {
        touched: false,
        valid: true
      }
    };
    const value = uiValidation(<any>PARTIAL_INPUT_SATE);

    expect(value).toBeTruthy();
  });

  it("return truthy form valid state if input has been touched", () => {
    let PARTIAL_INPUT_SATE = {
      state: {
        touched: true,
        valid: true
      }
    };
    const value = uiValidation(<any>PARTIAL_INPUT_SATE);

    expect(value).toBeTruthy();
  });

  it("return falsy form valid state if input has been touched", () => {
    let PARTIAL_INPUT_SATE = {
      state: {
        touched: true,
        valid: false
      }
    };
    const value = uiValidation(<any>PARTIAL_INPUT_SATE);
    expect(value).toBeFalsy();
  });
  it("return null on non array input", () => {
    const MOCK_VAL = "";

    const value = validate(MOCK_VAL, <any>"required");
    expect(value).toBeNull();
  });

  it("return valid input state on unknown rules", () => {
    const MOCK_VAL = "";

    const value = validate(MOCK_VAL, <any>["mock"]);
    expect(value).toMatchObject(VALID_INPUT_STATE);
  });

  it("return required input state on falsy value", () => {
    const MOCK_VAL = "";
    const expected = {
      value: false,
      message: "required"
    };
    const value = validate(MOCK_VAL, ["required"]);
    expect(value).toEqual(expected);
  });

  it("return minLength input state on falsy value", () => {
    const MOCK_VAL = "";
    const expected = {
      value: false,
      message: "minLength"
    };
    const value = validate(MOCK_VAL, ["minLength"]);
    expect(value).toEqual(expected);
  });

  it("return minLength input state on value length < 5", () => {
    const MOCK_VAL = "";
    const expected = {
      value: false,
      message: "minLength"
    };
    const value = validate(MOCK_VAL, ["minLength"]);
    expect(value).toEqual(expected);
  });

  it("return truthy input state on value length > 5", () => {
    const MOCK_VAL = "babouuuu";
    const expected = {
      value: true,
      message: "minLength"
    };
    const value = validate(MOCK_VAL, ["minLength"]);
    expect(value).toEqual(expected);
  });

  it("return falsy input state on invalid url", () => {
    const MOCK_VAL = "babouuuu";
    const expected = {
      value: false,
      message: "isUrl"
    };
    const value = validate(MOCK_VAL, ["isUrl"]);
    expect(value).toEqual(expected);
  });

  it("return falsy input state on invalid email", () => {
    const MOCK_VAL = "babouuuu";
    const expected = {
      value: false,
      message: "isEmail"
    };
    const value = validate(MOCK_VAL, ["isEmail"]);
    expect(value).toEqual(expected);
  });

  it("return truthy input state on empty rules", () => {
    const MOCK_VAL = "babouuuu";

    const value = validate(MOCK_VAL, []);
    expect(value).toEqual(VALID_INPUT_STATE);
  });

  it("return true if form controls are valid", () => {
    const mock = {
      ...BASE_MOCK_FORM_CONTROL,
      loginInput: {
        valid: true
      },
      pwdInput: {
        valid: true
      }
    };

    const value = controlsAreValid(<any>mock);
    expect(value).toBeTruthy();
  });

  it("return false if form controls are invalid", () => {
    const mock = {
      ...BASE_MOCK_FORM_CONTROL,
      loginInput: {
        valid: false
      },
      pwdInput: {
        valid: true
      }
    };

    const value = controlsAreValid(<any>mock);
    expect(value).toBeFalsy();
  });

  it("return form control state inputs", () => {
    const setter = _ => null;
    const key = "mock";
    const validations = ["required"];
    const mock = [[key, BASE_INPUT_STATE, setter, validations]];
    const value = makeFormState(<any>mock);
    const expected = {
      [key]: {
        state: BASE_INPUT_STATE,
        setter,
        validations
      }
    };
    expect(value).toMatchObject(expected);
  });

  it("return login payload from forms controls", () => {
    const mock = {
      ...BASE_MOCK_FORM_CONTROL,
      loginInput: {
        state: {
          value: "mock"
        }
      },
      pwdInput: {
        state: {
          value: "mock"
        }
      }
    };

    const value = formsCtrlsToPayload(<any>mock);

    const expected = {
      email: "mock",
      password: "mock"
    };
    expect(value).toMatchObject(expected);
  });

  it("return form value  from forms controls", () => {
    const mock = {
      ...BASE_MOCK_FORM_CONTROL,
      loginInput: {
        state: {
          value: "mock"
        }
      },
      pwdInput: {
        state: {
          value: "mock"
        }
      }
    };

    const value = formStateToValue(<any>mock);

    const expected = {
      loginInput: "mock",
      pwdInput: "mock"
    };
    expect(value).toMatchObject(expected);
  });

  it("return updated validation form state   from forms controls", () => {
    const validations = ["required"];
    const expected = {
      ...BASE_INPUT_STATE,
      validationType: validations
    };

    const value = formatInputState(<any>validations, BASE_INPUT_STATE);

    expect(value).toMatchObject(expected);
  });

  it("return a falatten list of values in an object", () => {
    const mock = {
      message: {
        nested: {
          test: "test",
          babou: "babou"
        },
        massage: {
          other_1: "other",
          nested_massage: {
            other_head: "other_babou",
            other_sub: "other_babou"
          }
        }
      }
    };

    const expected = {
      values: [
        { path: ["message", "nested", "test"], value: "test" },
        { path: ["message", "nested", "babou"], value: "babou" },
        { path: ["message", "massage", "other_1"], value: "other" },
        { path: ["message", "massage", "nested_massage", "other_head"], value: "other_babou" },
        { path: ["message", "massage", "nested_massage", "other_head"], value: "other_babou" }
      ]
    };

    const value = flattenObjValues(mock);

    expect(value.values).toEqual(expect.arrayContaining(expected.values));
  });

  it("return empty values on non object input", () => {
    const mock = "babou";
    const expected = {
      values: []
    };

    const value = flattenObjValues(mock);

    expect(value).toMatchObject(expected);
  });
});
