import { AppState } from "@home/store";

export type ValidPayload = string | number | boolean | Array<any> | Object;

export interface DispatchAction<T extends ValidPayload> {
  type: string;
  payload: T;
}

export interface StoreState {
  app: AppState;
  dashboard?: any;
}
export interface Store {
  getState: Function;
  dispatch: Function;
}

export type Reducer = (
  state: StoreState,
  action: DispatchAction<ValidPayload>
) => StoreState;

export type DispatchToStore<T> = (dispatch: DispatchAction<T>) => void;
