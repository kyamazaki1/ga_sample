export interface UserResponse {
  access_token: string;
  access_token_type: string;
  user_id: number;
  username: string;
  is_admin: boolean;
  business_id: number;
  expires: string;
}
export interface BaseUser {
  email: string;
  first_name: string;
  last_name: string;
  username: string;
}
export interface UserApi extends BaseUser {
  id: number;

  mobile_phone: string;
  office_phone: string;
}
export interface CreateUser extends BaseUser {
  business: number;
  emailConfirm: string;
  password: string;
  passwordConfirm: string;
}

export interface Business {
  id: number;
  name: string;
}
export const INIT_ADD_USER = {
  email: null,
  first_name: null,
  last_name: null,
  username: null,
  business: null,
  emailConfirm: null,
  password: null,
  passwordConfirm: null
};
export const INIT_USER_API = {
  email: null,
  first_name: null,
  id: null,
  last_name: null,
  mobile_phone: null,
  office_phone: null,
  username: null
};

//INITIAL VALUES
export const INIT_USER_RESPONSE: UserResponse = {
  access_token: null,
  access_token_type: null,
  user_id: null,
  username: null,
  is_admin: null,
  business_id: null,
  expires: null
};
