export interface ListTypeReducer<T> {
  list: Array<T>;
  loading: boolean;
  error: boolean;
}
