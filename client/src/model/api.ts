export type BaseApiResponse<T> = T;

export interface ApiInterface<T extends unknown> {
  status: number;
  data: T;
}
export interface FormatedListApi {
  data: Array<ViewFormatedData>;
}

export interface ViewFormatedData {
  [key: string]: JSX.Element;
}
