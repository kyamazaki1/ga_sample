export interface AppNavLink {
  path: string;
  name: string;
}
