import * as React from "react";
import "./Loading.css";

function loading() {
  return (
    <section className="wrap">
      <header className="loading__header">
        <h1 className="loading__title">Dashboard poc</h1>
      </header>
      <main>
        <div className="loader">Loading...</div>
      </main>
    </section>
  );
}

export { loading as Loading };
