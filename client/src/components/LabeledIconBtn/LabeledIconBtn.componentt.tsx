import * as React from "react";
import { makeStyles } from "@material-ui/styles";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
const useStyles = makeStyles({
  inlineBtn: {
    display: "block",
    maxWidth: 60
  },
  label: {
    fontSize: "0.65rem",
    lineHeight: 1,
    fontWeight: 300
  }
});

interface LabeledIconBtnProps {
  label: string;
  children: React.ReactNode;
  onAction: Function;
  size: "small" | "medium";
}

function labeledIconBtn(props: LabeledIconBtnProps): JSX.Element {
  const classes = useStyles(props);
  const { label, children, onAction, size } = props;

  return (
    <IconButton
      aria-label={label}
      classes={{
        label: classes.inlineBtn
      }}
      size={size}
      onClick={(e: React.MouseEvent<HTMLButtonElement>) => onAction(e)}
    >
      {children}
      <Typography display="block" variant="caption" className={classes.label}>
        {label}
      </Typography>
    </IconButton>
  );
}
labeledIconBtn.defaultProps = {
  size: "medium"
};

export { labeledIconBtn as LabeledIconBtn };
