import * as React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import * as PropTypes from "prop-types";

interface DropDownProps {
  graphChange: Function;
}
function dropDown({ graphChange }: DropDownProps): JSX.Element {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (str: string) => {
    setAnchorEl(null);
    graphChange(str);
  };

  return (
    <div>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        change graph
      </Button>
      <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        <MenuItem onClick={_ => handleClose("men")}>Men</MenuItem>
        <MenuItem onClick={_ => handleClose("women")}>Women</MenuItem>
      </Menu>
    </div>
  );
}

dropDown.defaultType = {
  graphChange: PropTypes.func
};

export { dropDown as DropDown };
