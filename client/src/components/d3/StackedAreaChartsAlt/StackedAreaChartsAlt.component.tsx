import * as React from "react";
import { D3Chart } from "./StackedAreaChartsAlt.d3";
import { ParsedAreaDataRaw, BrowserParsed, WorldParsed } from "@components/d3/models";

function stackedAreaChartsAlt({ data, dataKeys }: { data: Array<WorldParsed>; dataKeys: Array<string> }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data, dataKeys);
    }
  }, [data]);

  return <div style={{ padding: "50px" }} ref={d3Container}></div>;
}
export { stackedAreaChartsAlt as StackedAreaChartsAltComponent };
