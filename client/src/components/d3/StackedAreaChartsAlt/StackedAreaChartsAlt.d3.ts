import { StackedAreaChartsComponent } from "@src/components/d3/StackedAreaCharts/StackedAreaCharts.component";
import { WorldParsed } from "@components/d3/models";
import { axisLeft } from "d3-axis";
import * as d3 from "d3";
import d3Tip from "d3-tip";

import { ScaleBand, ScaleLinear, ScaleOrdinal } from "d3-scale";
import { height } from "@material-ui/system";
const men = "https://udemy-react-d3.firebaseio.com/data.json";

import { applyAttr, setSvgScene } from "../utils";
import { Area, Stack } from "d3";
const margin = { top: 20, right: 50, bottom: 40, left: 20 };

const SVG_WIDTH = 600 - (margin.left + margin.right);
const SVG_HEIGHT = 400 - (margin.top + margin.bottom);

const formatSi = d3.format(".3s");
const formatNumber = d3.format(".1f");
const formatBillion = x => formatNumber(x / 1e9);

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;
  data: Array<WorldParsed> = [];
  label: d3.Selection<any, any, any, any>;
  x: d3.ScaleTime<any, any>;
  y: d3.ScaleLinear<any, any>;
  stack: Stack<any, { [key: string]: number }, string>;
  area: Area<any>;
  z: ScaleOrdinal<any, any>;
  layer;
  color;

  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = setSvgScene(el, { height: SVG_HEIGHT, width: SVG_WIDTH }, margin);
    this.color = d3.scaleOrdinal(d3.schemeCategory10);

    this.x = d3.scaleTime().range([0, SVG_WIDTH]);
    this.y = d3.scaleLinear().range([SVG_HEIGHT, 0]);
    this.z = d3.scaleOrdinal(this.color);

    this.stack = d3.stack();

    this.area = d3
      .area()
      .x((d: [number, number]) => this.x((d as any).data.date))
      .y0((d: [number, number]) => this.y(d[0]))
      .y1((d: [number, number]) => this.y(d[1]));

    //axis
    this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g");

    //tip

    this.update(this.data, []);
  };

  update = (data: Array<WorldParsed>, keys: Array<string>) => {
    if (data.length === 0) {
      return null;
    }
    this.color.domain(keys);

    this.data = data;
    // X & Y

    const maxXDomain = d3.extent(data, (d: WorldParsed) => d.date);

    const maxDateVal = d3.max(data, function(d) {
      var vals = d3.keys(d).map(function(key) {
        return key !== "date" ? d[key] : 0;
      });

      return d3.sum(vals);
    });
    this.x.domain(maxXDomain);
    this.y.domain([0, maxDateVal]);

    //   this.z.domain(keys);
    // const z = this.stack(data as any);
    this.stack.keys(keys);
    this.stack.order(d3.stackOrderNone);
    this.stack.offset(d3.stackOffsetNone);

    //axis
    const axisXCall = d3.axisBottom(this.x);
    const axisYCall = d3.axisLeft(this.y).ticks(n => {
      return formatBillion(n);
    });
    this.xAxisGroup.transition(1000 as any).call(axisXCall);
    this.yAxisGroup.transition(1000 as any).call(axisYCall);

    let layer = this.svg
      .selectAll(".alt")
      .data(this.stack(data as any))
      .enter()
      .append("g")
      .attr("class", d => "alt " + d.key)
      .attr("fill-opacity", 0.5);

    layer
      .append("path")
      .attr("class", "area")
      .attr("d", this.area)
      .style("fill", d => this.color(d.key));
    // Only label the layers left at the end (if one browser disappears)
    layer
      .append("text")
      .datum(d => d)
      .attr("transform", d => "translate(" + this.x(data[13].date) + "," + this.y(d[13][1]) + ")")
      .attr("x", -6)
      .attr("dy", ".35em")
      .style("text-anchor", "start")
      .text(d => d.key)
      .attr("fill-opacity", 1);
  };
}
