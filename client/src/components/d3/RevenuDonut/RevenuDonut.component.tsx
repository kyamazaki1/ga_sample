import * as React from "react";
import { D3Chart } from "./RevenuDonut.d3";
import { RevenueBySize } from "@components/d3/models";

function revenuDonut({ data }: { data: Array<RevenueBySize> }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data);
    }
  }, [data]);

  return (
    <React.Fragment>
      <div ref={d3Container}></div>
    </React.Fragment>
  );
}
export { revenuDonut as RevenuDonut };
