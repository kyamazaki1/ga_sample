import { Margin } from "./../utils";
import { RevenueBySize } from "@components/d3/models";
import * as d3 from "d3";

import { setPieScene } from "../utils";
import { Pie } from "d3";

const SVG_WIDTH = 380;
const SVG_HEIGHT = 300;

const RADIUS = Math.min(SVG_WIDTH, SVG_HEIGHT) / 2;

function dataKey(d) {
  return d.company_size;
}

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: Array<RevenueBySize> = [];
  label: d3.Selection<any, any, any, any>;

  color;
  arc: any;
  pie: Pie<any, any>;
  _current;

  hasLegend: boolean = false;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = setPieScene(el, { height: SVG_HEIGHT, width: SVG_WIDTH });
    this.color = d3.scaleOrdinal(d3.schemeAccent).domain(["small", "medium", "large"]);

    this.arc = d3
      .arc()
      .innerRadius(RADIUS - 80)
      .outerRadius(RADIUS - 20);

    this.pie = d3
      .pie()
      .sort((a: any, b: any) => (a.company_size > b.company_size ? -1 : 1))
      .value((d: any) => d.count);
    this.update(this.data);
  };

  arcTween = d => {
    var i = d3.interpolate(this._current, d);
    this._current = i(1);
    return t => {
      return this.arc(i(t));
    };
  };

  update = (data: Array<RevenueBySize>) => {
    if (data.length === 0) {
      return null;
    }
    this.data = data;

    this.makeLegend(data);
    let path = this.svg.selectAll("svg").data(this.pie(data), dataKey);

    path
      .attr("class", "update arc")
      .transition()
      .duration(750)
      .attrTween("d", this.arcTween);

    path
      .enter()
      .append("path")
      .attr("class", "enter arc")
      .attr("fill", (d: any) => this.color(d.data.company_size))
      .transition()
      .duration(750)
      .attrTween("d", this.arcTween);
  };

  makeLegend = (data: Array<RevenueBySize>) => {
    if (this.hasLegend) {
      return null;
    }

    const offsetY = SVG_HEIGHT / 2 - 10;
    const offsetX = 90;
    data.forEach((d: RevenueBySize, idx: number) => {
      this.svg
        .append("text")
        .attr("x", +offsetX + 20)
        .attr("y", -(offsetY - idx * 20))
        .attr("alignment-baseline", "middle")
        .text(d.company_size);

      this.svg
        .append("circle")
        .attr("cx", +offsetX + 90)
        .attr("cy", -(offsetY - idx * 20))
        .attr("r", 6)
        .style("fill", this.color(d.company_size));
    });

    this.hasLegend = true;
  };
}
