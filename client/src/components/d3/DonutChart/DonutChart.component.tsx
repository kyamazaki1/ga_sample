import * as React from "react";
import { D3Chart } from "./DonuChart.d3";
import { FruitParsed } from "@components/d3/models";

function donutChart({ data }: { data: Array<FruitParsed> }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);
  const form = React.useRef(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data, form.current);
    }
  }, [data]);

  return (
    <React.Fragment>
      <form ref={form} className="form"></form>
      <div style={{ padding: "50px" }} ref={d3Container}></div>
    </React.Fragment>
  );
}
export { donutChart as DonutChartComponent };
