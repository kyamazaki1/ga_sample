import { FruitParsed, WorldParsed } from "@components/d3/models";
import * as d3 from "d3";

import { applyAttr, setSvgScene, setPieScene } from "../utils";
import { Area, Stack, DefaultArcObject, Pie } from "d3";

const SVG_WIDTH = 600;
const SVG_HEIGHT = 400;

const RADIUS = Math.min(SVG_WIDTH, SVG_HEIGHT) / 2;
// Find the element in data0 that joins the lowest following element in data1.
function findFollowing(i, data0, data1, key) {
  var n = data1.length,
    m = data0.length;
  while (++i < n) {
    var k = key(data1[i]);
    for (var j = 0; j < m; ++j) {
      if (key(data0[j]) === k) return data0[j];
    }
  }
}

function key(d) {
  return d.data.region;
}
function type(d) {
  d.count = +d.count;
  return d;
}
function findNeighborArc(i, data0, data1, key) {
  var d;
  return (d = findPreceding(i, data0, data1, key))
    ? { startAngle: d.endAngle, endAngle: d.endAngle }
    : (d = findFollowing(i, data0, data1, key))
    ? { startAngle: d.startAngle, endAngle: d.startAngle }
    : null;
}
// Find the element in data0 that joins the highest preceding element in data1.
function findPreceding(i, data0, data1, key) {
  var m = data0.length;
  while (--i >= 0) {
    var k = key(data1[i]);
    for (var j = 0; j < m; ++j) {
      if (key(data0[j]) === k) return data0[j];
    }
  }
}

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: Array<WorldParsed> = [];
  label: d3.Selection<any, any, any, any>;

  color;
  arc: any;
  pie: Pie<any, any>;
  _current;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = setPieScene(el, { height: SVG_HEIGHT, width: SVG_WIDTH });
    this.color = d3.scaleOrdinal(d3.schemeCategory10);

    this.arc = d3
      .arc()
      .innerRadius(RADIUS - 80)
      .outerRadius(RADIUS - 20);

    this.pie = d3
      .pie()
      .sort(null)
      .value((d: any) => d.count);

    //axis
    /*   this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g"); */

    //tip

    this.update(this.data, []);
  };

  arcTween = d => {
    var i = d3.interpolate(this._current, d);
    this._current = i(1);
    return t => {
      return this.arc(i(t));
    };
  };

  change = region => {
    var path = this.svg.selectAll("path");
    var data0 = path.data(),
      data1 = this.pie(region.values);
    // JOIN elements with new data.
    path = path.data(data1, key);
    // EXIT old elements from the screen.
    path
      .exit()
      .datum(function(d, i) {
        return findNeighborArc(i, data1, data0, key) || d;
      })
      .transition()
      .duration(750)
      .attrTween("d", this.arcTween)
      .remove();

    // UPDATE elements still on the screen.
    path
      .transition()
      .duration(750)
      .attrTween("d", this.arcTween);
    // ENTER new elements in the array.
    path
      .enter()
      .append("path")
      .each((d, i) => {
        this._current = findNeighborArc(i, data0, data1, key) || d;
      })
      .attr("fill", (d: any) => {
        return this.color(d.data.region);
      })
      .transition()
      .duration(750)
      .attrTween("d", this.arcTween);
  };

  update = (data: Array<WorldParsed>, form: any) => {
    if (data.length === 0) {
      return null;
    }

    this.data = data;
    const regionsByFruit = d3
      .nest()
      .key((d: FruitParsed) => d.fruit)
      .entries(data)
      .reverse();

    const label = d3
      .select(form)
      .selectAll("label")
      .data(regionsByFruit)
      .enter()
      .append("label");
    label
      .append("input")
      .attr("type", "radio")
      .attr("name", "fruit")
      .attr("value", d => d.key)
      .on("change", this.change)
      .filter(function(d, i) {
        return !i;
      })
      .each(this.change)
      .property("checked", true);

    label.append("span").text(d => d.key);
  };
}
