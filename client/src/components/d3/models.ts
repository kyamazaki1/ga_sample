import { Map } from "d3";
export interface MapChart {
  type: "Topology";
  objects: {
    countries: {
      type: "GeometryCollection";
      bbox: [number, number, number, number];
      geometries: Array<{
        type: string;
        id: number;
        arcs: Array<Array<Array<number>>>;
      }>;
    };
    land: {
      type: "MultiPolygon";
      arcs: Array<Array<Array<number>>>;
    };
  };
  arcs: Array<Array<Array<number>>>;
  transform: {
    scale: [number, number];
    translate: [number, number];
  };
}
export type ChorolepthMap = [MapChart, Map<any>];

export interface Team {
  Team_CN: string;
  Team_EN: string;
  State: string;
}
export interface Fruit {
  region: string;
  fruit: string;
  count: string;
}
export interface FruitParsed {
  region: string;
  fruit: string;
  count: number;
}

export interface Population {
  age: string;
  population: string;
}
export interface PopulationParsed {
  age: string;
  population: number;
}

export interface Browser {
  date: string;
  "Google Chrome": string;
  "Internet Explorer": string;
  Firefox: string;
  Safari: string;

  "Microsoft Edge": string;
  Opera: string;
  Mozilla: string;
  "Other/Unknown": string;
}

export interface World {
  date: string;
  "Rest of the world": string;
  Mexico: string;
  Netherlands: string;
  Belgium: string;
  Canada: string;
  Ireland: string;
  Germany: string;
  "United Kingdom": string;
  Italy: string;
  Poland: string;
  "Dominican Republic": string;
}
export interface WorldParsed {
  date: Date;
  "Rest of the world": number;
  Mexico: number;
  Netherlands: number;
  Belgium: number;
  Canada: number;
  Ireland: number;
  Germany: number;
  "United Kingdom": number;
  Italy: number;
  Poland: number;
  "Dominican Republic": number;
}

export interface BrowserParsed {
  date: Date;
  "Google Chrome": number;
  "Internet Explorer": number;
  Firefox: number;
  Safari: number;
  "Microsoft Edge": number;
  Opera: number;
  Mozilla: number;
  "Other/Unknown": number;
}

export interface UnployementRaw {
  date: string;
  "Wholesale and Retail Trade": number;
  Manufacturing: number;
  "Leisure and hospitality": number;
  "Business services": number;
  Construction: number;
  "Education and Health": number;
  Government: number;
  Finance: number;
  "Self-employed": number;
  Other: number;
  "Transportation and Utilities": number;
  Information: number;
  Agriculture: number;
  "Mining and Extraction": number;
}

export interface AreaDataRaw {
  date: string;
  close: string;
}
export interface ParsedAreaDataRaw {
  date: Date;
  close: number;
}

export interface NodeGroup {
  id: string;
  group: number;
}
export interface NodeLink {
  source: string;
  target: string;
  value: number;
}

export interface NodeGraph {
  nodes: Array<NodeGroup>;
  links: Array<NodeLink>;
}

export interface CallRevenu {
  call_duration: number;
  call_revenue: number;
  category: string;
  company_size: string;
  date: string;
  team: string;
  units_sold: number;
}

export interface CallRevenuParsed {
  northeast: number;
  west: number;
  south: number;
  midwest: number;
  date: Date;
}
export interface CallRevenuStackChart {
  northeast: number;
  west: number;
  south: number;
  midwest: number;
  date: string;
}

export type groupRevenueKey = "call_revenue" | "call_duration" | "units_sold";
export interface RevenueBySize {
  count: number;
  company_size: string;
}
export interface RevenueCat {
  category: string;
  value: number;
}
