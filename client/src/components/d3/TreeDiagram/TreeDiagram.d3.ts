import * as d3 from "d3";

const SVG_WIDTH = 1000;
const SVG_HEIGHT = 2000;

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: Array<{ id: string; value: string }>;
  tree: any;
  stratify;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", SVG_WIDTH)
      .attr("height", SVG_HEIGHT);

    this.svg.append("g").attr("transform", "translate(40,0)");

    this.tree = d3.tree().size([SVG_HEIGHT, SVG_WIDTH - 20]);
    this.stratify = d3.stratify().parentId((d: any) => d.id.substring(0, d.id.lastIndexOf(".")));
  };

  update = (data: any) => {
    if (!data) {
      return null;
    }

    const root = this.stratify(data).sort(function(a, b) {
      return a.height - b.height || a.id.localeCompare(b.id);
    });
    // Add the links (given by calling tree(root), which also adds positional x/y coordinates) for the nodes
    const link = this.svg
      .selectAll(".link")
      .data(this.tree(root).links())
      .enter()
      .append("path")
      .attr("class", "link")
      .attr("stroke", "#555")
      .attr("stroke-opacity", "0.4")
      .attr("stroke-width", "1.5px")
      .attr(
        "d",
        d3
          .linkHorizontal()
          .x((d: any) => d.y)
          .y((d: any) => d.x)
      );
    // Add groups for each node in the hierarchy with circles and text labels
    const node = this.svg
      .selectAll(".node")
      .data(root.descendants())
      .enter()
      .append("g")
      .attr("fill", "steelblue")
      .attr("class", (d: any) => "node" + (d.children ? " node--internal" : " node--leaf"))
      .attr("transform", (d: any) => "translate(" + d.y + "," + d.x + ")");
    node.append("circle").attr("r", 2.5);
    node
      .append("text")
      .attr("dy", 3)
      .attr("x", (d: any) => (d.children ? -8 : 8))
      .style("text-anchor", (d: any) => (d.children ? "end" : "start"))
      .text((d: any) => d.id.substring(d.id.lastIndexOf(".") + 1));
  };
}
