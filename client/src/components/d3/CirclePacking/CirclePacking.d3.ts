import * as d3 from "d3";

const SVG_WIDTH = 970;
const SVG_HEIGHT = 570;
export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: Array<{ id: string; value: string }>;
  color;
  format;
  pack;
  stratify;
  node;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", SVG_WIDTH)
      .attr("height", SVG_HEIGHT);

    this.svg.append("g").attr("transform", "translate(40,0)");
    this.pack = d3
      .pack()
      .size([SVG_WIDTH - 2, SVG_HEIGHT - 2])
      .padding(3);
    this.format = d3.format(",d");

    this.color = d3.scaleSequential(d3.interpolateMagma).domain([-4, 4]);

    // Stratify converts the data into the correct format
    this.stratify = d3.stratify().parentId((d: any) => d.id.substring(0, d.id.lastIndexOf(".")));
  };

  update = (data: any) => {
    if (data.length === 0) {
      return null;
    }
    // Create the root node with d3.stratify()
    const root = this.stratify(data)
      .sum((d: any) => d.value)
      .sort(function(a, b) {
        return b.value - a.value;
      });
    // Adds an x, y, and r value to each node
    this.pack(root);

    // Add a group for all the descendents of the root node
    const node = this.svg
      .select("g")
      .selectAll("g")
      .data(root.descendants())
      .enter()
      .append("g")
      .attr("transform", (d: any) => "translate(" + d.x + "," + d.y + ")")
      .attr("class", (d: any) => "node" + (!d.children ? " node--leaf" : d.depth ? "" : " node--root"))
      .each(function(d: any) {
        d.node = this;
      })
      .on("mouseover", this.hovered(true))
      .on("mouseout", this.hovered(false));
    // Append a circle to each node. Color-coded by level of the hierarchy
    node
      .append("circle")
      .attr("id", (d: any) => "node-" + d.id)
      .attr("r", (d: any) => d.r)
      .style("fill", (d: any) => this.color(d.depth));
    // Add labels for only the leaf nodes

    // Add labels for only the leaf nodes
    const leaf = node.filter((d: any) => !d.children);
    leaf
      .append("clipPath")
      .attr("id", (d: any) => "clip-" + d.id)
      .append("use")
      .attr("xlink:href", (d: any) => "#node-" + d.id + "");
    leaf
      .append("text")
      .attr("clip-path", (d: any) => "url(#clip-" + d.id + ")")
      .selectAll("tspan")
      .data((d: any) => d.id.substring(d.id.lastIndexOf(".") + 1).split(/(?=[A-Z][^A-Z])/g))
      .enter()
      .append("tspan")
      .attr("x", 0)
      .attr("y", (d, i, nodes) => 13 + (i - nodes.length / 2 - 0.5) * 10)
      .text((d: any) => d);
    // Simple tooltip
    node.append("title").text((d: any) => d.id + "\n" + this.format(d.value));
  };

  // When we hover over a node, highlight all of the node's ancestors

  hovered(hover) {
    return function(d) {
      d3.selectAll(d.ancestors().map((d: any) => d.node)).attr("stroke", hover ? "#000" : "transparent");
    };
  }
}
