import * as React from "react";
import { D3Chart } from "./LineGraph.d3";
export interface Year {
  year: Date;
  value: number;
}
function lineGraph({ data }: { data: Array<Year> }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data);
    }
  }, [data]);

  return <div style={{ padding: "50px" }} ref={d3Container}></div>;
}
export { lineGraph as LineGraphComponent };
