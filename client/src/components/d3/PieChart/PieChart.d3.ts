import { PopulationParsed, WorldParsed } from "@components/d3/models";
import { axisLeft } from "d3-axis";
import * as d3 from "d3";
import d3Tip from "d3-tip";

import { ScaleBand, ScaleLinear, ScaleOrdinal } from "d3-scale";
import { height } from "@material-ui/system";
const men = "https://udemy-react-d3.firebaseio.com/data.json";

import { applyAttr, setSvgScene, setPieScene } from "../utils";
import { Area, Stack, DefaultArcObject, Pie } from "d3";

const SVG_WIDTH = 600;
const SVG_HEIGHT = 400;

const RADIUS = Math.min(SVG_WIDTH, SVG_HEIGHT) / 2;

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;
  data: Array<WorldParsed> = [];
  label: d3.Selection<any, any, any, any>;
  x: d3.ScaleTime<any, any>;
  y: d3.ScaleLinear<any, any>;
  layer;
  color;
  arc: any;
  pie: Pie<any, any>;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = setPieScene(el, { height: SVG_HEIGHT, width: SVG_WIDTH });
    this.color = d3.scaleOrdinal(d3.schemeCategory10);

    this.color = d3.scaleOrdinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

    this.arc = d3
      .arc()
      .innerRadius(0)
      .outerRadius(100);

    this.pie = d3
      .pie()
      .sort(null)
      .value((d: any) => d.population);

    //axis
    this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g");

    //tip

    this.update(this.data, []);
  };

  update = (data: Array<WorldParsed>, keys: Array<string>) => {
    if (data.length === 0) {
      return null;
    }
    this.data = data;

    let arc = this.svg
      .selectAll(".arc")
      .data(this.pie(data))
      .enter()
      .append("g")
      .attr("class", "arc");

    arc
      .append("path")
      .attr("d", this.arc)
      .style("fill", d => this.color(d.data.age));
  };
}
