import * as React from "react";
import { D3Chart } from "./PieChart.d3";
import { PopulationParsed } from "@components/d3/models";

function pieChart({ data }: { data: Array<PopulationParsed> }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data);
    }
  }, [data]);

  return <div style={{ padding: "50px" }} ref={d3Container}></div>;
}
export { pieChart as PieChartComponent };
