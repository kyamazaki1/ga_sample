import { ChorolepthMap } from "@components/d3/models";
import * as d3 from "d3";

const men = "https://udemy-react-d3.firebaseio.com/data.json";

import * as topojson from "topojson";
const SVG_WIDTH = 1000;
const SVG_HEIGHT = 600;

const margin = { top: 20, right: 20, bottom: 40, left: 50 };

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: ChorolepthMap;
  path;
  color;
  x;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", SVG_WIDTH)
      .attr("height", SVG_HEIGHT);
    this.x = d3
      .scaleLinear()
      .domain([1, 10])
      .rangeRound([600, 860]);

    this.color = d3
      .scaleThreshold()
      .domain(d3.range(2, 10))
      .range(d3.schemeBlues["9"]);

    this.svg
      .append("g")
      .attr("class", "key")
      .attr("transform", "translate(0,40)");

    this.path = d3.geoPath();

    this.update(this.data);
  };

  update = (data: ChorolepthMap) => {
    if (!data) {
      return null;
    }
    const [MapData, unemploymentData] = data;
    this.svg
      .selectAll("rect")
      .data(
        this.color.range().map(d => {
          d = this.color.invertExtent(d);
          if (d[0] == null) d[0] = this.x.domain()[0];
          if (d[1] == null) d[1] = this.x.domain()[1];
          return d;
        })
      )
      .enter()
      .append("rect")
      .attr("height", 8)
      .attr("x", d => this.x(d[0]))
      .attr("width", d => this.x(d[1]) - this.x(d[0]))
      .attr("fill", d => this.color(d[0]));
    this.svg
      .append("text")
      .attr("class", "caption")
      .attr("x", this.x.range()[0])
      .attr("y", -6)
      .attr("fill", "#000")
      .attr("text-anchor", "start")
      .attr("font-weight", "bold")

      .text("Unemployment rate");
    this.svg
      .call(
        d3
          .axisBottom(this.x)
          .tickSize(13)
          .tickFormat((x, i) => (i ? x : ((x + "%") as any)))
          .tickValues(this.color.domain())
      )
      .select(".domain")
      .remove();

    this.svg
      .append("g")
      .attr("fill", "none")
      .selectAll("path")
      .data((topojson.feature(MapData as any, (MapData as any).objects.counties) as any).features)
      .enter()
      .append("path")
      .attr("fill", (d: any) => {
        return this.color((d.rate = unemploymentData.get(d.id)));
      })
      .attr("d", this.path)
      .append("title")
      .text((d: any) => {
        return d.rate + "%";
      });
    this.svg
      .append("path")
      .datum(
        topojson.mesh(MapData as any, (MapData as any).objects.states, function(a, b) {
          return a !== b;
        })
      )
      .attr("fill", "none")
      .attr("stroke", "#fff")
      .attr("stroke-linejoin", "round")
      .attr("d", this.path);
  };
}
