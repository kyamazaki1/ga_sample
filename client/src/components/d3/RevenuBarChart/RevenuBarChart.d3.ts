import * as d3 from "d3";
import { ScaleBand, ScaleLinear } from "d3-scale";
import { RevenueCat } from "../models";

const margin = { top: 20, right: 20, bottom: 30, left: 30 };

const SVG_WIDTH = 300 - (margin.left + margin.right);
const SVG_HEIGHT = 250 - (margin.top + margin.bottom);

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  rects: SVGElement;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;

  title: string;
  data: Array<any>;
  label: d3.Selection<any, any, any, any>;
  color;
  y: ScaleLinear<any, any>;
  x: ScaleBand<any>;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el as any)
      .append("svg")
      .attr("width", SVG_WIDTH + margin.left + margin.right)
      .attr("height", SVG_HEIGHT + (margin.top + margin.bottom))
      .append("g")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);

    this.label = this.svg
      .append("text")
      .attr("x", SVG_WIDTH / 2)
      .attr("y", 0)
      .attr("text-anchor", "middle");

    this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g").attr("transform", `translate(0, 0)`);

    this.color = d3.scaleOrdinal(d3.schemeAccent).domain(["appliances", "electronics", "furniture", "materials"]);
  };

  update = (data: Array<RevenueCat>) => {
    this.label.text(this.title);
    const maxY = d3.max<RevenueCat, number>(data, (d: any) => d.value);
    //const minY = d3.min<RevenueCat, number>(data, (d: any) => d.value);
    this.y = d3
      .scaleLinear()
      .domain([0, maxY]) //the min means 0 here
      .range([SVG_HEIGHT, 0]);

    this.x = d3
      .scaleBand()
      .domain(data.map(r => r.category))
      .range([0, SVG_WIDTH])
      .padding(0.4);

    const xAxisCall = d3.axisBottom(this.x).tickSize(0);
    this.xAxisGroup
      .call(xAxisCall)
      .selectAll("text")
      .style("text-anchor", "start")
      .attr("x", -20)
      .attr("y", 10);

    // .attr("transform", "rotate(30)");

    const yAxisCall = d3
      .axisLeft(this.y)
      .tickSize(0)
      .ticks(4);
    this.yAxisGroup.call(yAxisCall);

    //DATA join
    let rects = this.svg.selectAll("rect").data(data, (d: any) => d.category); //use the second arg to specidfy key

    // EXIT old elements not present in new data.
    rects
      .exit()
      .attr("class", "exit")
      .transition(d3.transition().duration(1000))
      .attr("height", 0)
      .attr("y", SVG_HEIGHT)
      .style("fill-opacity", "0.1")
      .remove();

    //update
    rects
      .attr("class", "update")
      .transition(d3.transition().duration(1000))
      .attr("x", (d: any, i) => this.x(d.category))
      .attr("width", this.x.bandwidth)
      .attr("height", (d: any, i) => SVG_HEIGHT - this.y(d.value))
      .attr("y", (d: any) => this.y(d.value));

    //ENTER
    rects
      .enter()
      .append("rect")
      .attr("class", "enter")
      //  .merge(rects as any) //apyl same methods on update/enters
      .attr("x", (d: any, i) => this.x(d.category))
      .attr("width", this.x.bandwidth)
      .attr("height", (d: any, i) => SVG_HEIGHT - this.y(d.value))
      .attr("y", (d: any) => this.y(d.value))
      .attr("fill", (d: any) => this.color(d.category));
  };
}
