import * as React from "react";
import { D3Chart } from "./RevenuBarChart.d3";

interface RevenuBarChartProps {
  data: Array<any>;
  title: string;
}
function revenuBarChart({ data, title }: RevenuBarChartProps) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.title = title;
      useChart.update(data);
    }
  }, [data]);

  return <div style={{ padding: "50px" }} ref={d3Container}></div>;
}
export { revenuBarChart as RevenuBarChart };
