import * as d3 from "d3";
import { ScaleBand, ScaleLinear } from "d3-scale";
const men = "https://udemy-react-d3.firebaseio.com/tallest_men.json";
const women = "https://udemy-react-d3.firebaseio.com/tallest_women.json";

const margin = { top: 20, right: 20, bottom: 60, left: 50 };

const SVG_WIDTH = 450 - (margin.left + margin.right);
const SVG_HEIGHT = 300 - (margin.top + margin.bottom);

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  rects: SVGElement;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;
  menData;
  womenData;
  label: d3.Selection<any, any, any, any>;
  constructor(el: React.ReactInstance) {
    this.init(el);

    Promise.all([d3.json(men), d3.json(women)]).then(datasets => {
      const [menData, womenData] = datasets;
      this.menData = menData;
      this.womenData = womenData;
      this.update("men");
    });
  }

  init = el => {
    this.svg = d3
      .select(el as any)
      .append("svg")
      .attr("width", SVG_WIDTH + margin.left + margin.right)
      .attr("height", SVG_HEIGHT + (margin.top + margin.bottom))
      .append("g")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);

    this.label = this.svg
      .append("text")
      .attr("x", SVG_WIDTH / 2)
      .attr("y", 10)
      .attr("text-anchor", "middle");

    this.svg
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("x", -SVG_HEIGHT / 2)
      .attr("y", -40)
      .attr("text-anchor", "middle")
      .text("cms");

    this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g").attr("transform", `translate(0, 0)`);
  };

  update = gender => {
    const data = gender === "men" ? this.menData : this.womenData;
    this.label.text(`tallest ${gender}`);
    const maxY = d3.max<{ name: string; height: number }, number>(data, (d: any) => d.height);
    const minY = d3.min<{ name: string; height: number }, number>(data, (d: any) => d.height);
    const y: ScaleLinear<any, any> = d3
      .scaleLinear()
      .domain([minY, maxY]) //the min means 0 here
      .range([SVG_HEIGHT, 0]);

    const x: ScaleBand<any> = d3
      .scaleBand()
      .domain(data.map(r => r.name))
      .range([0, SVG_WIDTH])
      .padding(0.4);

    const xAxisCall = d3.axisBottom(x).tickSize(0);
    this.xAxisGroup
      .call(xAxisCall)
      .selectAll("text")
      .style("text-anchor", "end")
      .attr("x", `70`)
      .attr("transform", "rotate(30)");

    const yAxisCall = d3.axisLeft(y).tickSize(0);
    this.yAxisGroup.call(yAxisCall);

    //DATA join
    const rects = this.svg.selectAll("rect").data(data, (d: any) => d.name); //use the second arg to specidfy key
    //EXIT
    rects
      .exit()
      .transition()
      .duration(500)
      .attr("height", 0)
      .attr("y", SVG_HEIGHT)
      .remove();
    //update
    /* 
    rects
      .transition()
      .duration(500)
      .attr("x", (d: any, i) => x(d.name))
      .attr("y", (d: any) => y(d.height))
      .attr("width", x.bandwidth)
      .attr("height", (d: any, i) => SVG_HEIGHT - y(d.height)); */
    //ENTER
    rects
      .enter()
      .append("rect")
      .attr("fill", (d, i) => "grey")
      .attr("y", SVG_HEIGHT)
      .merge(rects as any) //apyl same methods on update/enters
      .attr("x", (d: any, i) => x(d.name))
      .attr("width", x.bandwidth)
      .transition()
      .duration(500)
      .attr("height", (d: any, i) => SVG_HEIGHT - y(d.height))
      .attr("y", (d: any) => y(d.height));
  };
}
