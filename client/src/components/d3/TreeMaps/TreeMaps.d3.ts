import * as d3 from "d3";

const SVG_WIDTH = 970;
const SVG_HEIGHT = 570;

function sumBySize(d) {
  return d.size;
}

// Return the number of descendants that the node has
function sumByCount(d) {
  return d.children ? 0 : 1;
}

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: Array<{ id: string; value: string }>;
  treemap: any;
  color;
  format;
  root;
  cell;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", SVG_WIDTH)
      .attr("height", SVG_HEIGHT);

    this.svg.append("g").attr("transform", "translate(40,0)");

    this.treemap = d3
      .treemap()
      .tile(d3.treemapResquarify)
      .size([SVG_WIDTH, SVG_HEIGHT])
      .round(true)
      .paddingInner(1);

    const fader = color => d3.interpolateRgb(color, "#fff")(0.2);
    this.color = d3.scaleOrdinal(d3.schemeCategory10.map(fader));
    this.format = d3.format(",d");

    this.update([], "sumBySize");
  };

  update = (data: any, sumTree: string) => {
    if (data.length === 0) {
      return null;
    }

    //  d3.selectAll("input").data([sumBySize, sumByCount], (d: any) => (d ? d.name : sumTree));
    // Give the treemap a new root, which uses a different summing function

    this.root = d3
      .hierarchy(data)
      .eachBefore(d => (d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name))
      .sum(sumTree === "sumBySize" ? sumBySize : sumByCount)
      .sort((a, b) => b.height - a.height || b.value - a.value);

    // Computes x0, x1, y0, and y1 for each node (where the rectangles should be)
    this.treemap(this.root);
    this.cell = this.svg
      .selectAll("g")
      .data(this.root.leaves())
      .enter()
      .append("g")
      .attr("transform", (d: any) => "translate(" + d.x0 + "," + d.y0 + ")");

    // Add rectanges for each of the boxes that were generated
    this.cell
      .append("rect")
      .attr("id", (d: any) => d.id)
      .attr("width", (d: any) => d.x1 - d.x0)
      .attr("height", (d: any) => d.y1 - d.y0)
      .attr("fill", (d: any) => this.color(d.parent.data.id));
    // Make sure that text labels don't overflow into adjacent boxes
    this.cell
      .append("clipPath")
      .attr("id", (d: any) => "clip-" + d.data.id)
      .append("use")
      .attr("xlink:href", (d: any) => "#" + d.data.id);
    // Add text labels - each word goes on its own line
    this.cell
      .append("text")
      .attr("clip-path", (d: any) => "url(#clip-" + d.data.id + ")")
      .selectAll("tspan")
      .data((d: any) => d.data.name.split(/(?=[A-Z][^A-Z])/g))
      .enter()
      .append("tspan")
      .attr("x", 4)
      .attr("y", function(d, i) {
        return 13 + i * 10;
      })
      .text((d: any) => d);
    // Simple way to make tooltips
    this.cell.append("title").text((d: any) => d.data.id + "\n" + this.format(d.value));
  };

  changed = sum => {
    if (!sum) {
      return null;
    }
    d3.selectAll("input").data([sumBySize, sumByCount], (d: any) => (d ? d.name : sum));
    // Give the treemap a new root, which uses a different summing function

    this.treemap(this.root.sum(sum === "sumBySize" ? sumBySize : sumByCount));

    // Add rectanges for each of the boxes that were generated
    this.cell
      .append("rect")
      .attr("id", (d: any) => d.id)
      .attr("width", (d: any) => d.x1 - d.x0)
      .attr("height", (d: any) => d.y1 - d.y0)
      .attr("fill", (d: any) => this.color(d.parent.data.id));
    // Make sure that text labels don't overflow into adjacent boxes
    this.cell
      .append("clipPath")
      .attr("id", (d: any) => "clip-" + d.data.id)
      .append("use")
      .attr("xlink:href", (d: any) => "#" + d.data.id);
    // Add text labels - each word goes on its own line
    this.cell
      .append("text")
      .attr("clip-path", (d: any) => "url(#clip-" + d.data.id + ")")
      .selectAll("tspan")
      .data((d: any) => d.data.name.split(/(?=[A-Z][^A-Z])/g))
      .enter()
      .append("tspan")
      .attr("x", 4)
      .attr("y", function(d, i) {
        return 13 + i * 10;
      })
      .text((d: any) => d);
    // Simple way to make tooltips
    this.cell.append("title").text((d: any) => d.data.id + "\n" + this.format(d.value));

    // Update the size and position of each of the rectangles
    this.cell
      .transition()
      .duration(750)
      .attr("transform", (d: any) => "translate(" + d.x0 + "," + d.y0 + ")")
      .select("rect")
      .attr("width", (d: any) => d.x1 - d.x0)
      .attr("width", (d: any) => d.y1 - d.y0);
  };
}
