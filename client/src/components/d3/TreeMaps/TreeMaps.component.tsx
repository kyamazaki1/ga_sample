import * as React from "react";
import { D3Chart } from "./TreeMaps.d3";

function treeMaps({ data, sumTree }: { data: Array<{ id: string; value: string }>; sumTree: string }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data);
    }
  }, [data]);
  React.useEffect(() => {
    if (useChart) {
      useChart.changed(sumTree);
    }
  }, [sumTree]);

  return (
    <React.Fragment>
      <div style={{ padding: "50px" }} ref={d3Container}></div>
    </React.Fragment>
  );
}
export { treeMaps as TreeMapsComponent };
