import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import * as PropTypes from "prop-types";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap"
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  })
);

interface SelectTreeProps {
  revenueChange: Function;
}
function selectCallRevenue({ revenueChange }: SelectTreeProps) {
  const classes = useStyles({});
  const [values, setValues] = React.useState<string>("call_revenue");

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: string }>) => {
    setValues(_ => event.target.value);
    revenueChange(event.target.value);
  };

  return (
    <form className={classes.root} autoComplete="off">
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="revenue_cat">group ny </InputLabel>
        <Select value={values} onChange={handleChange} inputProps={{ name: "group", id: "group" }}>
          <MenuItem value="call_revenue">Revenue</MenuItem>
          <MenuItem value="call_duration">Duration</MenuItem>
          <MenuItem value="units_sold">Units sold</MenuItem>
        </Select>
      </FormControl>
    </form>
  );
}
selectCallRevenue.defaultType = {
  revenueChange: PropTypes.func
};

export { selectCallRevenue as SelectCallRevenue };
