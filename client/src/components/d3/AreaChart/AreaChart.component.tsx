import * as React from "react";
import { D3Chart } from "./AreaChart.d3";
import { ParsedAreaDataRaw } from "@components/d3/models";

function areaChart({ data }: { data: Array<ParsedAreaDataRaw> }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.data = data;
      useChart.update(data);
    }
  }, [data]);

  return <div style={{ padding: "50px" }} ref={d3Container}></div>;
}
export { areaChart as AreaChartComponent };
