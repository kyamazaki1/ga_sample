import { ParsedAreaDataRaw } from "@components/d3/models";

import * as d3 from "d3";

import { applyAttr } from "../utils";
import { Area } from "d3";
const margin = { top: 20, right: 20, bottom: 40, left: 50 };

const SVG_WIDTH = 450 - (margin.left + margin.right);
const SVG_HEIGHT = 300 - (margin.top + margin.bottom);

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  circles: SVGElement;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;
  data: Array<ParsedAreaDataRaw> = [];
  label: d3.Selection<any, any, any, any>;
  x: d3.ScaleTime<any, any>;
  y: d3.ScaleLinear<any, any>;
  area: Area<ParsedAreaDataRaw>;

  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el as any)
      .append("svg")
      .attr("width", SVG_WIDTH + margin.left + margin.right)
      .attr("height", SVG_HEIGHT + (margin.top + margin.bottom))
      .append("g")
      .attr("fill", (d, i) => "grey")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);

    this.x = d3.scaleTime().range([0, SVG_WIDTH]);
    this.y = d3.scaleLinear().range([SVG_HEIGHT, 0]);

    this.area = d3
      .area<ParsedAreaDataRaw>()
      .x((d: ParsedAreaDataRaw) => this.x(d.date))
      .y0(this.y(0))
      .y1(d => this.y(d.close));

    //axis
    this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg
      .append("g")
      .append("text")
      .attr("fill", "#000")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Price ($)");

    applyAttr(this.svg.append("text"), {
      x: SVG_WIDTH / 2,
      y: SVG_HEIGHT + 30,
      "font-size": 15,
      "text-anchor": "middle"
    }).text("year");

    applyAttr(this.svg.append("text"), {
      x: -(SVG_HEIGHT / 2),
      y: -30,
      "font-size": 15,
      transform: "rotate(-90)",
      "text-anchor": "middle"
    }).text("close $");

    //tip

    this.update(this.data);
  };

  update = (data: Array<ParsedAreaDataRaw>) => {
    if (this.data.length === 0) {
      return null;
    }
    this.data = data;
    // X & Y
    const maxXDomain = d3.extent(data, (d: ParsedAreaDataRaw) => d.date);
    this.x.domain(maxXDomain);

    const maxYDomain = d3.max<ParsedAreaDataRaw, number>(data, (d: ParsedAreaDataRaw) => d.close);
    this.y.domain([0, maxYDomain]);
    //axis
    const axisXCall = d3.axisBottom(this.x);
    const axisYCall = d3.axisLeft(this.y);
    this.xAxisGroup.transition(1000 as any).call(axisXCall);
    this.yAxisGroup.transition(1000 as any).call(axisYCall);

    this.svg
      .append("path")
      .attr("fill", "steelblue")
      .attr("d", this.area(data));

    //tip
    /*     const tip = d3Tip()
      .attr("class", "d3-tip")
      .html((d: ParsedAreaDataRaw) => {
        return `<span style='color:red; cursor:pointer;'>${d.name}</span>`;
      });
    this.svg.call(tip); */

    //JOIN
    /*     const circles = this.svg.selectAll("circle").data(this.data, (d: ParsedAreaDataRaw) => d.name); //use the second arg to specidfy key
    //EXIT
    circles
      .exit()
      .transition(1000 as any)
      .attr("cy", this.y(0))
      .remove();
    //UPDATE

    circles.attr("fill", (d: ParsedAreaDataRaw) => (d.name === name ? "blue" : "grey"));

    //ENDTER (ADD)

    circles
      .enter()
      .append("circle")
      .attr("cy", this.y(0))
      .attr("r", 5)
      .on("click", (d: ParsedAreaDataRaw) => this.updateName(d.name))
      .on("mouseover", tip.show)
      .on("mouseleave", tip.hide)
      .merge(circles as any)
      .attr("cx", (d: ParsedAreaDataRaw) => this.x(Number(d.age)))
      .transition(1000 as any)
      .attr("cy", (d: ParsedAreaDataRaw) => this.y(Number(d.height))); */
  };
}
