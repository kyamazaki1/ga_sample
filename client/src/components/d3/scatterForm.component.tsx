import * as React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

interface ScatterFormProps {
  onAdd: Function;
}
interface State {
  name: string;
  age: string;
  height: string;
}

function scatterForm({ onAdd }): JSX.Element {
  const [values, setValues] = React.useState<State>({
    name: "",
    age: "",
    height: ""
  });

  const handleChange = (name: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [name]: event.target.value });
  };

  const add = () => {
    onAdd(values);
    setValues({
      name: "",
      age: "",
      height: ""
    });
  };

  return (
    <div>
      <form noValidate autoComplete="off">
        <TextField
          id="standard-name"
          label="Name"
          value={values.name}
          onChange={handleChange("name")}
          margin="normal"
        />
        <TextField id="standard-age" label="age" value={values.age} onChange={handleChange("age")} margin="normal" />
        <TextField
          id="standard-height"
          label="Height"
          value={values.height}
          onChange={handleChange("height")}
          margin="normal"
        />
      </form>
      <Button variant="contained" color="primary" onClick={add}>
        Add
      </Button>
    </div>
  );
}

export { scatterForm as ScatterForm };
