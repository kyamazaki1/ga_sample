import { axisLeft } from "d3-axis";
import * as d3 from "d3";
import d3Tip from "d3-tip";

import { ScaleBand, ScaleLinear } from "d3-scale";
import { height } from "@material-ui/system";
const men = "https://udemy-react-d3.firebaseio.com/data.json";

import { applyAttr } from "./../utils";
const margin = { top: 20, right: 20, bottom: 40, left: 50 };

const SVG_WIDTH = 450 - (margin.left + margin.right);
const SVG_HEIGHT = 300 - (margin.top + margin.bottom);
export interface Child {
  age: string;
  height: string;
  name: string;
}
export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  circles: SVGElement;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;
  data: Array<any> = [];
  label: d3.Selection<any, any, any, any>;
  x: d3.ScaleLinear<any, any>;
  y: d3.ScaleLinear<any, any>;
  updateName: Function;

  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el as any)
      .append("svg")
      .attr("width", SVG_WIDTH + margin.left + margin.right)
      .attr("height", SVG_HEIGHT + (margin.top + margin.bottom))
      .append("g")
      .attr("fill", (d, i) => "grey")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);

    this.x = d3.scaleLinear().range([0, SVG_WIDTH]);
    this.y = d3.scaleLinear().range([SVG_HEIGHT, 0]);

    //axis
    this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g");
    //label
    /*   this.svg
      .append("text")
      .attr("x", SVG_WIDTH / 2)
      .attr("y", SVG_HEIGHT + 30)
      .attr("font-size", 15)
      .attr("text-anchor", "middle")
      .text("age"); */

    applyAttr(this.svg.append("text").text("age"), {
      x: SVG_WIDTH / 2,
      y: SVG_HEIGHT + 30,
      "font-size": 15,
      "text-anchor": "middle"
    });

    /*     this.svg
      .append("text")
      .attr("x", -(SVG_HEIGHT / 2))
      .attr("y", -30)
      .attr("font-size", 15)
      .attr("transform", "rotate(-90)")
      .attr("text-anchor", "middle")
      .text("height in cm"); */

    applyAttr(this.svg.append("text"), {
      x: -(SVG_HEIGHT / 2),
      y: -30,
      "font-size": 15,
      transform: "rotate(-90)",
      "text-anchor": "middle"
    }).text("height in cms");

    //tip

    this.update(this.data, "");
  };

  update = (data: Array<Child>, name: string) => {
    if (this.data.length === 0) {
      return null;
    }

    this.data = data;
    // X & Y
    const maxXDomain = d3.max<Child, number>(data, (d: any) => Number(d.age));
    this.x.domain([0, maxXDomain]);

    const maxYDomain = d3.max<Child, number>(data, (d: any) => Number(d.height));
    this.y.domain([0, maxYDomain]);
    //axis
    const axisXCall = d3.axisBottom(this.x);
    const axisYCall = d3.axisLeft(this.y);
    this.xAxisGroup.transition(1000 as any).call(axisXCall);
    this.yAxisGroup.transition(1000 as any).call(axisYCall);

    //tip
    const tip = d3Tip()
      .attr("class", "d3-tip")
      .html((d: Child) => {
        return `<span style='color:red; cursor:pointer;'>${d.name}</span>`;
      });
    this.svg.call(tip);

    //JOIN
    const circles = this.svg.selectAll("circle").data(this.data, (d: Child) => d.name); //use the second arg to specidfy key
    //EXIT
    circles
      .exit()
      .transition(1000 as any)
      .attr("cy", this.y(0))
      .remove();
    //UPDATE

    circles.attr("fill", (d: Child) => (d.name === name ? "blue" : "grey"));
    //  .transition(1000 as any)
    //   .attr("cx", (d: Child) => this.x(Number(d.age)));
    // .attr("cy", (d: Child) => this.y(Number(d.height)));

    //ENDTER (ADD)

    circles
      .enter()
      .append("circle")
      .attr("cy", this.y(0))
      .attr("r", 5)
      .on("click", (d: Child) => this.updateName(d.name))
      .on("mouseover", tip.show)
      .on("mouseleave", tip.hide)
      .merge(circles as any)
      .attr("cx", (d: Child) => this.x(Number(d.age)))
      .transition(1000 as any)
      .attr("cy", (d: Child) => this.y(Number(d.height)));
  };
}
