import * as React from "react";
import { D3Chart } from "./ScatterPoint.d3";

function ScatterPoint({ data, highlight, activeName }: { data: Array<any>; highlight: Function; activeName: string }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.updateName = highlight;
    }
  }, [useChart]);

  React.useEffect(() => {
    if (useChart) {
      useChart.data = data;
      useChart.update(data, activeName);
    }
  }, [data, activeName]);

  return <div style={{ padding: "50px" }} ref={d3Container}></div>;
}
export { ScatterPoint as ScatterPointComponent };
