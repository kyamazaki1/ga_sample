import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import * as PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import { CallRevenuParsed } from "./models";
import { Subject, of } from "rxjs";
import { tap, map, debounceTime, distinctUntilChanged } from "rxjs/operators";
import * as d3 from "d3";
import { dragEnable } from "d3";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 300
    }
  })
);
const parseDmY = d3.timeParse("%d/%m/%Y");

interface DateSliderRevenueProps {
  dateChange: Function;
  data: Array<CallRevenuParsed>;
}

function valuetext(value: any) {
  if (value) {
    return `${value}`;
  }
  return "";
}
//https://stackoverflow.com/questions/57631094/using-react-useeffect-hook-with-rxjs-mergemap-operator
function dateSliderRevenue({ data, dateChange }: DateSliderRevenueProps) {
  const classes = useStyles({});
  const [value, setValue] = React.useState<any[]>([]);

  const [useData, setUseData] = React.useState<any[]>([]);

  const [value$] = React.useState(() => new Subject<any>());

  React.useEffect(() => {
    //const subscription = input$.subscribe(setInput);
    value$
      .pipe(
        debounceTime(500),
        map((i: any) => i),
        distinctUntilChanged((a, b) => a[0] === b[0] && a[1] === b[1]),

        tap((range: [Date, Date]) => {
          dateChange(range);
        })
      )
      .subscribe();

    return () => {
      value$.unsubscribe();
    };
  }, []);

  React.useEffect(() => {
    if (useData.length === 0) {
      const dates = data.map(d => d.date).sort((a, b) => (a > b ? 1 : -1));
      setUseData(dates);
      setValue([0, dates.length]);
    }
  }, [data]);

  const handleChange = (event: any, newValue: any | any[]) => {
    setValue(newValue as number[]);
    const [bef, aft] = value;
    const range = [useData[bef], useData[aft - 1]];
    value$.next(range);
  };

  return (
    <div className={classes.root}>
      <Typography id="range-slider" gutterBottom>
        Temperature range
      </Typography>
      <Slider
        max={useData.length}
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        getAriaValueText={v => valuetext(useData[v])}
      />
    </div>
  );
}
dateSliderRevenue.defaultType = {
  dateChange: PropTypes.func
};

export { dateSliderRevenue as DateSliderRevenue };
