import { MapChart } from "@components/d3/models";
import * as d3 from "d3";

import { ScaleBand, ScaleLinear, ScaleOrdinal } from "d3-scale";
import { height } from "@material-ui/system";
const men = "https://udemy-react-d3.firebaseio.com/data.json";

import { applyAttr, setSvgScene, setPieScene } from "../utils";
import * as topojson from "topojson";
const SVG_WIDTH = 600;
const SVG_HEIGHT = 400;

const margin = { top: 20, right: 20, bottom: 40, left: 50 };

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: MapChart;
  label: d3.Selection<any, any, any, any>;
  path;
  graticule;

  _current;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", SVG_WIDTH)
      .attr("height", SVG_HEIGHT);

    this.update(this.data);
  };

  update = (data: MapChart) => {
    if (!data) {
      return null;
    }

    const projection = d3
      .geoMercator()
      .scale(153)
      .translate([SVG_WIDTH / 2, SVG_HEIGHT / 2])
      .precision(0.1);
    this.path = d3.geoPath().projection(projection);
    this.graticule = d3.geoGraticule();

    this.svg
      .append("path")
      .datum(topojson.feature(data as any, data.objects.land))
      .attr("fill", "steelblue")
      .attr("d", this.path);
    this.svg
      .append("path")
      .datum(topojson.mesh(data as any, (data as any).objects.countries, (a, b) => a !== b))
      .attr("fill", "none")
      .attr("stroke", "#fff")
      .attr("stroke-width", ".5")

      .attr("d", this.path);
    this.svg
      .append("path")
      .datum(this.graticule)
      .attr("fill", "none")
      .attr("stroke", "#777")
      .attr("stroke-width", ".5")
      .attr("stroke-opacity", ".5")
      .attr("d", this.path);
  };
}
