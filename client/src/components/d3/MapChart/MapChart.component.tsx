import * as React from "react";
import { D3Chart } from "./MapChart.d3";
import { MapChart } from "@components/d3/models";

function mapChart({ data }: { data: MapChart }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data);
    }
  }, [data]);

  return (
    <React.Fragment>
      <div style={{ padding: "50px" }} ref={d3Container}></div>
    </React.Fragment>
  );
}
export { mapChart as MapChartComponent };
