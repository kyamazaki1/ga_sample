import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import * as PropTypes from "prop-types";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap"
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  })
);

interface SelectTreeProps {
  sumChange: Function;
}
function selectTree({ sumChange }: SelectTreeProps) {
  const classes = useStyles({});
  const [values, setValues] = React.useState<string>("sumBySize");

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: string }>) => {
    setValues(_ => event.target.value);
    sumChange(event.target.value);
  };

  return (
    <form className={classes.root} autoComplete="off">
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="age-simple">sum by </InputLabel>
        <Select value={values} onChange={handleChange} inputProps={{ name: "sum", id: "tree_sum" }}>
          <MenuItem value="sumBySize">sumBySize</MenuItem>
          <MenuItem value="sumByCount">sumByCount</MenuItem>
        </Select>
      </FormControl>
    </form>
  );
}

selectTree.defaultType = {
  sumChange: PropTypes.func
};

export { selectTree as SelectTree };
