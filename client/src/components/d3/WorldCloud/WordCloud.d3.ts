import { Team } from "@components/d3/models";
import { WorldParsed } from "@components/d3/models";
import * as d3 from "d3";

import { ScaleBand, ScaleLinear, ScaleOrdinal } from "d3-scale";
import { height } from "@material-ui/system";
const men = "https://udemy-react-d3.firebaseio.com/data.json";

import { applyAttr, setSvgScene, setPieScene } from "../utils";
import d3Could from "d3-cloud";
const SVG_WIDTH = 600;
const SVG_HEIGHT = 400;

const margin = { top: 20, right: 20, bottom: 40, left: 50 };

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: Array<WorldParsed> = [];
  label: d3.Selection<any, any, any, any>;
  categories;
  color;

  _current;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = setSvgScene(el, { height: SVG_HEIGHT, width: SVG_WIDTH }, margin);
    this.color = d3.scaleOrdinal(d3.schemeCategory10);

    this.update(this.data);
  };

  update = (data: Array<WorldParsed>) => {
    if (data.length === 0) {
      return null;
    }

    this.data = data;

    const categories = d3.keys(
      d3
        .nest()
        .key((d: Team) => d.State)
        .map(data)
    );
    const fontSize = d3
      .scalePow()
      .exponent(5)
      .domain([0, 1])
      .range([40, 80]);

    const layout = d3Could()
      .size([SVG_WIDTH, SVG_HEIGHT])
      .timeInterval(20)
      .words(data)
      /*      .rotate(function(d) {
        return 0;
      }) */
      .rotate(function() {
        return ~~(Math.random() * 2) * 90;
      })
      .fontSize((d, i) => fontSize(Math.random()))
      .fontWeight(["bold"])
      .text((d: Team) => d.Team_EN)
      .on("end", this.draw)
      .start();
  };

  draw = words => {
    var wordcloud = this.svg
      .append("g")
      .attr("class", "wordcloud")
      .attr("transform", "translate(" + SVG_WIDTH / 2 + "," + SVG_HEIGHT / 2 + ")");
    wordcloud
      .selectAll("text")
      .data(words)
      .enter()
      .append("text")
      .attr("class", "word")
      .style("fill", (d, i) => this.color(i))
      .style("font-size", (d: any) => d.size + "px")
      .style("font-family", (d: any) => d.font)
      .attr("text-anchor", "middle")
      .attr("transform", (d: any) => "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")")
      .text((d: any) => d.text);
  };
}
