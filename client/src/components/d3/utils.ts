import * as d3 from "d3";

export function applyAttr(
  svg: d3.Selection<any, unknown, null, undefined>,
  attrs: Object
): d3.Selection<any, unknown, null, undefined> {
  for (let prop in attrs) {
    svg.attr(prop, attrs[prop]);
  }

  return svg;
}
export interface Margin {
  top: number;
  left: number;
  bottom: number;
  right: number;
}
export interface Scene {
  height: number;
  width: number;
}
//set margin convetion

export function setSvgScene(
  el: HTMLElement,
  { height, width }: Scene,
  { top, left, bottom, right }: Margin
): d3.Selection<any, unknown, any, unknown> {
  return d3
    .select(el)
    .append("svg")
    .attr("width", width + left + right)
    .attr("height", height + (top + bottom))
    .append("g")
    .attr("fill", (d, i) => "grey")
    .attr("transform", `translate(${left}, ${top})`);
}

export function setPieScene(el: HTMLElement, { height, width }: Scene): d3.Selection<any, unknown, any, unknown> {
  return d3
    .select(el)
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
}

export function appendText(instance: d3.Selection<any, any, any, any>, txtValue: string, attrs: Object): void {
  applyAttr(instance.append("text").text(txtValue), attrs);
}
