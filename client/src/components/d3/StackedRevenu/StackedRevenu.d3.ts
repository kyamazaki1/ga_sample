import { CallRevenuParsed } from "@components/d3/models";
import { axisLeft } from "d3-axis";
import * as d3 from "d3";
import d3Tip from "d3-tip";

import { ScaleBand, ScaleLinear, ScaleOrdinal } from "d3-scale";
import { height } from "@material-ui/system";
const men = "https://udemy-react-d3.firebaseio.com/data.json";

import { applyAttr, setSvgScene } from "../utils";
import { Area, Stack } from "d3";
const margin = { top: 30, right: 50, bottom: 20, left: 70 };

const SVG_WIDTH = 900 - (margin.left + margin.right);
const SVG_HEIGHT = 500 - (margin.top + margin.bottom);

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;
  data: Array<CallRevenuParsed> = [];
  label: d3.Selection<any, any, any, any>;
  x: d3.ScaleTime<any, any>;
  y: d3.ScaleLinear<any, any>;
  stack: Stack<any, { [key: string]: number }, string>;
  area: Area<any>;
  z: ScaleOrdinal<any, any>;
  layer;
  color;
  axisXCall;
  axisYCall;
  teams;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = setSvgScene(el, { height: SVG_HEIGHT, width: SVG_WIDTH }, margin);
    this.color = d3.scaleOrdinal(d3.schemeCategory10);

    this.x = d3.scaleTime().range([0, SVG_WIDTH]);
    this.y = d3.scaleLinear().range([SVG_HEIGHT, 0]);
    this.z = d3.scaleOrdinal(this.color);

    this.stack = d3.stack();

    this.axisXCall = d3.axisBottom(this.x);
    this.axisYCall = d3.axisLeft(this.y).ticks(5, "s");

    this.area = d3
      .area()
      .x((d: [number, number]) => this.x((d as any).data.date))
      .y0((d: [number, number]) => this.y(d[0]))
      .y1((d: [number, number]) => this.y(d[1]));

    //axis
    this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g");

    //tip

    this.update(this.data, []);
  };

  update = (data: Array<CallRevenuParsed>, keys: Array<string>) => {
    if (data.length === 0) {
      return null;
    }

    this.color.domain(keys);

    this.data = data;
    // X & Y

    const maxXDomain = d3.extent(data, (d: CallRevenuParsed) => d.date);

    const maxCatVal = d3.max(data, function(d) {
      var vals = d3.keys(d).map(function(key: string) {
        return key !== "date" ? d[key] : 0;
      });
      return d3.sum(vals);
    });

    this.x.domain(maxXDomain);
    this.y.domain([0, maxCatVal]);

    this.stack.keys(keys);

    this.stack.order(d3.stackOrderNone);

    //axis
    this.axisXCall.scale(this.x);
    this.xAxisGroup.transition(d3.transition().duration(1000)).call(this.axisXCall);
    this.axisYCall.scale(this.y);
    this.yAxisGroup.transition(d3.transition().duration(1000)).call(this.axisYCall);

    //join data
    this.teams = this.svg.selectAll(".team").data(this.stack(data as any)); //<-- HERE classes are important to do the update.SELECT
    // Update the path for each team

    this.teams.select(".area").attr("d", this.area); //<-- HERE classes are important to do the update.SELECT

    this.teams
      .enter()
      .append("g")
      .attr("class", d => "team " + d.key) //<-- HERE classes are important to do the update.UPDATE
      .append("path")
      .attr("d", this.area) //right the path value HERE`
      .attr("class", "area") //<-- HERE classes are important to do the update.UPDATE
      .style("fill", d => this.color(d.key))
      .attr("fill-opacity", 0.5);

    // Only label the layers left at the end (if one browser disappears)
    /*     layer
      .append("text")
      .datum(d => d)
      .attr("transform", (d: any) => {
        return "translate(" + this.x(data[d.index].date) + "," + this.y(d[d.index][1]) + ")";
      })
      .attr("x", -6)
      .attr("dy", ".35em")
      .style("text-anchor", "start")
      .text(d => d.key)
      .attr("fill-opacity", 1); */
  };
}
