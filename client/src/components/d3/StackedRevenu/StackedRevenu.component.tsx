import * as React from "react";
import { D3Chart } from "./StackedRevenu.d3";
import { CallRevenuParsed } from "../models";

function stackedRevenu({ data, dataKeys }: { data: Array<CallRevenuParsed>; dataKeys: Array<string> }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      const toDate = data.map(d => ({ ...d, date: new Date(d.date) }));
      useChart.update(toDate, dataKeys);
    }
  }, [data]);
  return <div style={{ padding: "50px" }} ref={d3Container}></div>;
}
export { stackedRevenu as StackedRevenuComponent };
