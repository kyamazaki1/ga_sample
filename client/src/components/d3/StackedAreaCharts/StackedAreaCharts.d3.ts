import { StackedAreaChartsComponent } from "@src/components/d3/StackedAreaCharts/StackedAreaCharts.component";
import { BrowserParsed } from "@components/d3/models";
import { axisLeft } from "d3-axis";
import * as d3 from "d3";
import d3Tip from "d3-tip";

import { ScaleBand, ScaleLinear, ScaleOrdinal } from "d3-scale";
import { height } from "@material-ui/system";
const men = "https://udemy-react-d3.firebaseio.com/data.json";

import { applyAttr, setSvgScene } from "../utils";
import { Area, Stack } from "d3";
const margin = { top: 20, right: 20, bottom: 40, left: 50 };

const SVG_WIDTH = 600 - (margin.left + margin.right);
const SVG_HEIGHT = 400 - (margin.top + margin.bottom);

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  circles: SVGElement;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;
  data: Array<BrowserParsed> = [];
  label: d3.Selection<any, any, any, any>;
  x: d3.ScaleTime<any, any>;
  y: d3.ScaleLinear<any, any>;
  stack: Stack<any, { [key: string]: number }, string>;
  area: Area<any>;

  z: ScaleOrdinal<any, any>;
  layer;

  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = setSvgScene(el, { height: SVG_HEIGHT, width: SVG_WIDTH }, margin);

    this.x = d3.scaleTime().range([0, SVG_WIDTH]);
    this.y = d3.scaleLinear().range([SVG_HEIGHT, 0]);
    this.z = d3.scaleOrdinal(d3.schemeCategory10);

    this.stack = d3.stack();

    this.area = d3
      .area()
      .x((d: [number, number]) => this.x((d as any).data.date))
      .y0((d: [number, number]) => this.y(d[0]))
      .y1((d: [number, number]) => this.y(d[1]));

    //axis
    this.xAxisGroup = this.svg.append("g").attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g");

    /*     applyAttr(this.svg.append("text"), {
      x: SVG_WIDTH / 2,
      y: SVG_HEIGHT + 30,
      "font-size": 15,
      "text-anchor": "middle"
    }).text("year");

    applyAttr(this.svg.append("text"), {
      x: -(SVG_HEIGHT / 2),
      y: -30,
      "font-size": 15,
      transform: "rotate(-90)",
      "text-anchor": "middle "
    }).text("close $");*/

    //tip

    this.update(this.data, []);
  };

  update = (data: Array<BrowserParsed>, keys: Array<string>) => {
    if (data.length === 0) {
      return null;
    }

    this.data = data;
    // X & Y

    const maxXDomain = d3.extent(data, (d: BrowserParsed) => d.date);
    this.x.domain(maxXDomain);
    this.z.domain(keys);
    this.stack.keys(keys);
    //axis
    const axisXCall = d3.axisBottom(this.x);
    const axisYCall = d3.axisLeft(this.y).ticks(10, "%");
    this.xAxisGroup.transition(1000 as any).call(axisXCall);
    this.yAxisGroup.transition(1000 as any).call(axisYCall);

    let layer = this.svg
      .selectAll(".layer")
      .data(this.stack(data as any))
      .enter()
      .append("g");

    layer
      .append("path")
      .style("fill", d => this.z(d.key))
      .attr("d", this.area);
    // Only label the layers left at the end (if one browser disappears)
    layer
      .filter(d => d[d.length - 1][1] - d[d.length - 1][0] > 0.01)
      .append("text")
      .attr("x", SVG_WIDTH - 6)
      .attr("y", d => this.y((d[d.length - 1][0] + d[d.length - 1][1]) / 2))
      .attr("dy", ".35em")
      .style("font", "10px sans-serif")
      .style("text-anchor", "end")
      .text(d => d.key);
  };
}
