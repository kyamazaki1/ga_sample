import * as React from "react";
import { D3Chart } from "./StackedAreaCharts.d3";
import { ParsedAreaDataRaw, BrowserParsed } from "@components/d3/models";

function stackedAreaCharts({ data, dataKeys }: { data: Array<BrowserParsed>; dataKeys: Array<string> }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data, dataKeys);
    }
  }, [data]);

  return <div style={{ padding: "50px" }} ref={d3Container}></div>;
}
export { stackedAreaCharts as StackedAreaChartsComponent };
