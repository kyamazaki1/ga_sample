import * as d3 from "d3";

const SVG_WIDTH = 960;
const SVG_HEIGHT = 700;
const radius = Math.min(SVG_WIDTH, SVG_HEIGHT) / 2 - 10;

// Return the number of descendants that the node has

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: { name: string; children: Array<null> };
  color;
  format;
  partition;
  arc;
  x;
  y;
  root;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", SVG_WIDTH)
      .attr("height", SVG_HEIGHT)
      .append("g")
      .attr("transform", "translate(" + SVG_WIDTH / 2 + "," + SVG_HEIGHT / 2 + ")");

    this.color = d3.scaleOrdinal(d3.schemeSet1);
    this.x = d3.scaleLinear().range([0, 2 * Math.PI]);
    this.y = d3.scaleSqrt().range([0, radius]);
    this.partition = d3.partition();

    // These values will be provided by d3.partition()
    this.arc = d3
      .arc()
      .startAngle((d: any) => Math.max(0, Math.min(2 * Math.PI, this.x(d.x0))))
      .endAngle((d: any) => Math.max(0, Math.min(2 * Math.PI, this.x(d.x1))))
      .innerRadius((d: any) => Math.max(0, this.y(d.y0)))
      .outerRadius((d: any) => Math.max(0, this.y(d.y1)));

    this.format = d3.format(",d");
  };

  update = (data: any) => {
    if (data.length === 0) {
      return null;
    }
    this.root = d3.hierarchy(data).sum((d: any) => d.size);

    // Add an arc for each of the nodes in our hierarchy. partition(root) adds x0, x1, y0, and y1 values to each node.
    this.svg
      .selectAll("path")
      .data(this.partition(this.root).descendants())
      .enter()
      .append("path")
      .attr("d", this.arc)
      .style("fill", (d: any) => this.color((d.children ? d : d.parent).data.name))
      .on("click", this.click)
      .append("title")
      .text((d: any) => d.data.name + "\n" + this.format(d.value));
  };

  click = d => {
    const self = this;
    // Redraw the arcs when one of them is clicked to zoom in on a section;
    this.svg
      .transition()
      .duration(750)
      .tween("scales", () => {
        const xd = d3.interpolate(this.x.domain(), [d.x0, d.x1]);
        const yd = d3.interpolate(this.y.domain(), [d.y0, 1]);
        const yr = d3.interpolate(this.y.range(), [d.y0 ? 20 : 0, radius]);
        return t => {
          this.x.domain(xd(t));
          this.y.domain(yd(t)).range(yr(t));
        };
      })
      .selectAll("path")
      .attrTween("d", d => {
        return function() {
          //capture the modified d value inside function scope
          //do not use fat arrow => as it will leak modified outer d value
          let _d = d;
          return self.arc(_d);
        };
      });
  };
}
