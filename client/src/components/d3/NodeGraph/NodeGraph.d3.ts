import { NodeGraph } from "@components/d3/models";
import * as d3 from "d3";

const SVG_WIDTH = 1000;
const SVG_HEIGHT = 700;

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;

  data: NodeGraph;
  simulation;

  color;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", SVG_WIDTH)
      .attr("height", SVG_HEIGHT);
    this.color = d3.scaleOrdinal(d3.schemeCategory10);

    this.update(this.data);
  };

  update = (data: NodeGraph) => {
    if (!data) {
      return null;
    }
    this.simulation = d3
      .forceSimulation()
      .force("center", d3.forceCenter(SVG_WIDTH / 2, SVG_HEIGHT / 2))
      .force("charge", d3.forceManyBody().strength(-50))
      .force("collide", d3.forceCollide(10).strength(0.9))
      .force("link", d3.forceLink().id((d: any) => d.id));
    // Add lines for every link in the dataset
    const link = this.svg
      .append("g")
      .attr("class", "links")
      .attr("stroke", "#999")
      .attr("stroke-opacity", "0.6")
      .selectAll("line")
      .data(data.links)
      .enter()
      .append("line")
      .attr("stroke-width", d => Math.sqrt(d.value));
    // Add circles for every node in the dataset
    const node = this.svg
      .append("g")
      .attr("class", "nodes")
      .attr("stroke", "#fff")
      .attr("stroke-width", "1.5px")
      .selectAll("circle")
      .data(data.nodes)
      .enter()
      .append("circle")
      .attr("r", 5)
      .attr("fill", d => this.color(d.group))
      .call(
        d3
          .drag()
          .on("start", this.dragstarted)
          .on("drag", this.dragged)
          .on("end", this.dragended)
      );
    // Basic tooltips
    node.append("title").text(d => d.id);
    // Attach nodes to the simulation, add listener on the "tick" event
    this.simulation.nodes(data.nodes).on("tick", ticked);
    // Associate the lines with the "link" force
    this.simulation.force("link").links(data.links);
    // Dynamically update the position of the nodes/links as time passes
    function ticked() {
      link
        .attr("x1", (d: { target: any; source: any; value: number; index: number }) => d.source.x)
        .attr("y1", (d: { target: any; source: any; value: number; index: number }) => d.source.y)
        .attr("x2", (d: { target: any; source: any; value: number; index: number }) => d.target.x)
        .attr("y2", (d: { target: any; source: any; value: number; index: number }) => d.target.y);
      node
        .attr("cx", (d: { id: string; group: number; index: number; x: number; y: number }) => d.x)
        .attr("cy", (d: { id: string; group: number; index: number; x: number; y: number }) => d.y);
    }
  };

  // Change the value of alpha, so things move around when we drag a node
  dragstarted = d => {
    if (!d3.event.active) {
      this.simulation.alphaTarget(0.7).restart();
    }
    d.fx = d.x;
    d.fy = d.y;
  };
  // Fix the position of the node that we are looking at
  dragged = d => {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  };
  // Let the node do what it wants again once we've looked at it
  dragended = d => {
    if (!d3.event.active) {
      this.simulation.alphaTarget(0);
    }
    d.fx = null;
    d.fy = null;
  };
}
