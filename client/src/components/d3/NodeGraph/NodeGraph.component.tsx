import * as React from "react";
import { D3Chart } from "./NodeGraph.d3";
import { NodeGraph } from "@components/d3/models";

function nodeGraph({ data }: { data: NodeGraph }) {
  const d3Container = React.useRef(null);
  const [useChart, setUseChart] = React.useState(null);

  React.useEffect(() => {
    setUseChart(new D3Chart(d3Container.current));
  }, []);

  React.useEffect(() => {
    if (useChart) {
      useChart.update(data);
    }
  }, [data]);

  return (
    <React.Fragment>
      <div style={{ padding: "50px" }} ref={d3Container}></div>
    </React.Fragment>
  );
}
export { nodeGraph as NodeGraphComponent };
