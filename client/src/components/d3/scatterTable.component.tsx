import * as React from "react";
import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as PropTypes from "prop-types";
import { Child } from "./ScatterPoint/ScatterPoint.d3";

interface ScatterTableProps {
  data: Array<Child>;
  remove: Function;
  activeName: string;
  highlight: Function;
}
function scatterTable({ remove, data, activeName, highlight }: ScatterTableProps): JSX.Element {
  return (
    <div>
      {/*  <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        change graph
      </Button> */}
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="right">name</TableCell>
            <TableCell align="right">height</TableCell>
            <TableCell align="right">age</TableCell>
            <TableCell align="right">remove</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(row => (
            <TableRow key={row.name} selected={row.name === activeName}>
              <TableCell component="th" scope="row" onClick={() => highlight(row.name)}>
                {row.name}
              </TableCell>
              <TableCell align="right">{row.height}</TableCell>
              <TableCell align="right">{row.age}</TableCell>

              <TableCell align="right">
                <Button variant="contained" color="primary" onClick={() => remove(row.name)}>
                  remove
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
}

scatterTable.defaultType = {
  highlight: n => n,
  remove: n => n,
  data: []
};

export { scatterTable as ScatterTable };
