import * as React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem, { ListItemProps } from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import { AppLink } from "@src/components/AppLink/AppLink";
import { AppNavLink } from "@src/model/app";
export interface PageNavLink extends AppNavLink {
  Icon: any;
}

interface NavSubListProps {
  links: Array<PageNavLink>;
  listLabel: string;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    link: {
      display: "flex",
      alignItems: "flex-end"
    },
    link__icon: {
      justifyContent: "flex-end"
    }
  })
);

function navSubList(props: NavSubListProps): JSX.Element {
  const { links, listLabel } = props;
  const classes = useStyles(props);

  return (
    <List aria-label={listLabel}>
      {links.map((n: PageNavLink) => (
        <ListItem
          button
          key={n.name}
          className={classes.link}
          style={{
            justifyContent: n.Icon ? "space-around" : null
          }}
        >
          <AppLink path={n.path} name={n.name} />
          {n.Icon ? (
            <ListItemIcon className={classes.link__icon}>
              <n.Icon />
            </ListItemIcon>
          ) : null}
        </ListItem>
      ))}
    </List>
  );
}

export { navSubList as NavSubList };
