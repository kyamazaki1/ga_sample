import * as React from "react";
import {
  Link as RouterLink,
  LinkProps as RouterLinkProps
} from "react-router-dom";
import Link from "@material-ui/core/Link";
import { AppNavLink } from "@model/app";
import { Typography } from "@material-ui/core";
//https://material-ui.com/components/links/
// The usage of React.forwardRef will no longer be required for react-router-dom v6.
// see https://github.com/ReactTraining/react-router/issues/6056
/* const AdapterLink = React.forwardRef<HTMLAnchorElement, RouterLinkProps>((props, ref) => (
    <RouterLink innerRef={ref as any} {...props} />
  ));
  
  const CollisionLink = React.forwardRef<HTMLAnchorElement, Omit<RouterLinkProps, 'innerRef' | 'to'>>(
    (props, ref) => (
      <RouterLink innerRef={ref as any} to="/getting-started/installation/" {...props} />
    ),
  ); */
type TypoVariant =
  | "h1"
  | "h2"
  | "h3"
  | "h4"
  | "h5"
  | "h6"
  | "subtitle1"
  | "subtitle2"
  | "body1"
  | "body2"
  | "caption"
  | "button"
  | "overline"
  | "srOnly"
  | "inherit";
interface LinkProps extends AppNavLink {
  style: Object;
  variant: TypoVariant;
}

function appLink(props: LinkProps): JSX.Element {
  const { path, name, style, variant } = props;
  return (
    <Link component={RouterLink} to={path} style={style}>
      <Typography variant={variant}>{name}</Typography>
    </Link>
  );
}
appLink.defaultProps = {
  style: null,
  variant: "caption"
};
export { appLink as AppLink };
