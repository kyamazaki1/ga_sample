import * as React from "react";
import * as PropTypes from "prop-types";
import { useRef } from "react";
import { TextField } from "@material-ui/core";

interface LoginFormProps<T> {
  id: string; //"login-input" | "pwd-input" | "confirm-input";
  value: any;
  detail: { name: string; type: string; placeholder: string };
  handleUpdate: Function;
  name?: string; // "email" | "password";
}
function formInput<T>(props: LoginFormProps<T>): JSX.Element {
  const {
    detail: { name, type, placeholder },
    value
  } = props;
  const inputEl = useRef(null);

  return (
    <TextField
      fullWidth={true}
      id={props.id}
      type={type}
      ref={inputEl}
      value={value}
      inputProps={{ "data-testid": props.id, autoComplete: "on" }}
      onChange={(ev: React.ChangeEvent<HTMLInputElement>) => {
        props.handleUpdate({ name, value: ev.target.value });
        inputEl.current.focus();
      }}
      label={placeholder}
    />
  );
}
formInput.propTypes = {
  handleUpdate: PropTypes.func
};
export { formInput as FormInput };
