import * as React from "react";
import { useState, useEffect } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Fab from "@material-ui/core/Fab";
import ScoreIcon from "@material-ui/icons/Score";
import ImportExportIcon from "@material-ui/icons/ImportExport";
import {
  DRAWER_WIDTH,
  BTN_OFFSET
} from "@src/containers/ProtectedPage/ProtectedPage";
import { Tooltip, IconButton } from "@material-ui/core";
import { MoreNavBtn } from "./MoreNavBtn/MoreNavBtn.component";
import { ToggleCampainBtn } from "./ToggleCampainBtn/ToggleCampainBtn";

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    listWrap: {
      position: "relative",
      width: DRAWER_WIDTH,
      height: "100%",
      borderRight: "1px solid rgba(255, 255, 255, 0.12)",
      backgroundColor: theme.palette.background.paper
    },

    float_btn: {
      position: "absolute",
      width: 25,
      height: 25,
      minHeight: "inherit",
      top: "45%",
      left: DRAWER_WIDTH - BTN_OFFSET,
      zIndex: -1
    },
    nav__text: {
      fontSize: theme.typography.caption.fontSize
    },
    sort__txt: {
      fontSize: theme.typography.caption.fontSize,
      color: "rgba(255,255,255,.7)"
    },
    circle: {
      backgroundColor: "#197C48",
      width: 8,
      height: 8,
      content: "",
      position: "absolute",
      left: 35,
      top: 5,
      borderRadius: "40%"
    }
  });
});

interface SideListProps {
  slideState: boolean;
  toggleNav: Function;
}
//look if token is persited locally/state (should be)
//the validity of toke is handled by axios interceptorp

let sideList = (props: SideListProps): JSX.Element => {
  const { slideState, toggleNav } = props;
  const classes = useStyles({});

  const [useIsOpen, setUseIsOpen] = useState(slideState);
  function toggleDrawer(b: boolean) {
    setUseIsOpen(b);
  }
  useEffect(() => {
    toggleNav(useIsOpen);
  }, [useIsOpen]);
  useEffect(() => {
    setUseIsOpen(slideState);
  }, [slideState]);

  return (
    <div className={classes.listWrap} role="presentation">
      <Fab
        color="primary"
        aria-label="open menu"
        className={classes.float_btn}
        onClick={() => toggleDrawer(!useIsOpen)}
      >
        <ChevronLeftIcon />
      </Fab>
      <List dense={true}>
        <ListItem dense={true}>
          <ListItemText
            classes={{
              primary: classes.nav__text
            }}
            primary="All campaigns"
          />
          <MoreNavBtn />
          <ToggleCampainBtn />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem key="current_campain" dense={true}>
          <ListItemIcon>
            <ScoreIcon />
          </ListItemIcon>
          <ListItemText
            classes={{
              primary: classes.nav__text
            }}
            primary="Smart campain"
          />
        </ListItem>

        <ListItem key="sort_campain" dense={true}>
          <ListItemText
            classes={{
              primary: classes.sort__txt
            }}
            primary="Enabled, Paused and Removed"
          />
          <Tooltip title="Sort campaigns">
            <IconButton aria-label="Sort campaigns" size="small">
              <ImportExportIcon />
            </IconButton>
          </Tooltip>
        </ListItem>

        <ListItem key="list" dense={true}>
          <ListItemIcon>
            <ScoreIcon />
          </ListItemIcon>
          <span className={classes.circle}></span>

          <ListItemText
            classes={{
              primary: classes.nav__text
            }}
            primary="main campain"
          />
        </ListItem>
      </List>
    </div>
  );
};

export { sideList as SideList };
