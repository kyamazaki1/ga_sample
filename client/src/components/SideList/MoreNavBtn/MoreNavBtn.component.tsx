import * as React from "react";
import { makeStyles } from "@material-ui/styles";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { LabeledIconBtn } from "@src/components/LabeledIconBtn/LabeledIconBtn.componentt";
import { IconPopOver } from "@src/components/IconPopover/IconPopover.component";
import { AppNavLink } from "@src/model/app";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListSubheader from "@material-ui/core/ListSubheader";

import { AppLink } from "@src/components/AppLink/AppLink";
import { Divider } from "@material-ui/core";

const useStyles = makeStyles({});
interface AppNav {
  title: string;
  paths: Array<AppNavLink>;
}
const CAMPAIGN_PATHS: Array<AppNavLink> = [
  {
    name: "All",
    path: "/dashboard"
  },
  {
    name: "All enabled",
    path: "/"
  },
  {
    name: "All but removed",
    path: "/"
  }
];
const GRP_PATHS: Array<AppNavLink> = [
  {
    name: "All",
    path: "/dashboard"
  },
  {
    name: "All enabled",
    path: "/"
  },
  {
    name: "All but removed",
    path: "/"
  }
];

const APP_NAV: Array<AppNav> = [
  {
    title: "Campaign status",
    paths: CAMPAIGN_PATHS
  },
  {
    title: "Ad groups status",
    paths: GRP_PATHS
  }
];
interface MoreNavBtnProps {}

function moreNavBtn(props: MoreNavBtnProps): JSX.Element {
  const classes = useStyles(props);

  //TODO: abstract popover hooks
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <React.Fragment>
      <LabeledIconBtn label="" onAction={handleClick} size="small">
        <MoreVertIcon />
      </LabeledIconBtn>
      <IconPopOver
        eventTarget={anchorEl}
        resetAnchor={handleClose}
        origin={{
          vertical: "bottom",
          horizontal: "right"
        }}
        transform={{
          vertical: "top",
          horizontal: "center"
        }}
        idName="more-nav"
      >
        <List>
          {APP_NAV.map((p: AppNav) => (
            <div key={p.title}>
              <ListSubheader>{p.title}</ListSubheader>
              {p.paths.map(l => (
                <ListItem key={l.name}>
                  <AppLink path={l.path} name={l.name} />
                </ListItem>
              ))}
              <Divider />
            </div>
          ))}
          <ListItem>
            <AppLink path="/hide/draft" name="Hide drafts" />
          </ListItem>
          <Divider />

          <ListItem>
            <AppLink path="/hide/campaign" name="Hide campaign types" />
          </ListItem>
        </List>
      </IconPopOver>
    </React.Fragment>
  );
}

export { moreNavBtn as MoreNavBtn };
