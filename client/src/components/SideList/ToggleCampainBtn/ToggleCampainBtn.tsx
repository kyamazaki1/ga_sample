import * as React from "react";
import { makeStyles } from "@material-ui/styles";
import { LabeledIconBtn } from "@src/components/LabeledIconBtn/LabeledIconBtn.componentt";
import { IconPopOver } from "@src/components/IconPopover/IconPopover.component";
import { AppNavLink } from "@src/model/app";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ArrowDropDownCircleIcon from "@material-ui/icons/ArrowDropDownCircle";

import { AppLink } from "@src/components/AppLink/AppLink";

const useStyles = makeStyles({});

const CAMPAIGN_PATHS: Array<AppNavLink> = [
  {
    name: "All Campagins",
    path: "/dashboard"
  },
  {
    name: "All Campagins groups",
    path: "/"
  }
];

interface ToggleCampainBtnProps {}

function toggleCampainBtn(props: ToggleCampainBtnProps): JSX.Element {
  const classes = useStyles(props);

  //TODO: abstract popover hooks
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <React.Fragment>
      <LabeledIconBtn label="" onAction={handleClick} size="small">
        <ArrowDropDownCircleIcon />
      </LabeledIconBtn>
      <IconPopOver
        eventTarget={anchorEl}
        resetAnchor={handleClose}
        origin={{
          vertical: "bottom",
          horizontal: "right"
        }}
        transform={{
          vertical: "top",
          horizontal: "center"
        }}
        idName="toogle-campaign"
      >
        <List>
          {CAMPAIGN_PATHS.map(l => (
            <ListItem key={l.name}>
              <AppLink path={l.path} name={l.name} />
            </ListItem>
          ))}
        </List>
      </IconPopOver>
    </React.Fragment>
  );
}

export { toggleCampainBtn as ToggleCampainBtn };
