import * as React from "react";
import { cleanup, fireEvent, render } from "@testing-library/react";

import { ErrorBoundary } from "./ErrotBoundary";

class ErrButton extends React.Component {
  state = { renderBomb: false };
  handleBombClick = () => this.setState({ renderBomb: true });
  render() {
    return (
      <ErrorBoundary>
        {this.state.renderBomb ? <p>has error </p> : <button onClick={this.handleBombClick}>trigger error</button>}
      </ErrorBoundary>
    );
  }
}

jest.mock("./..//../mock/utils.mock", () => {
  return {
    reportError: jest.fn(() => Promise.resolve({ success: true }))
  };
});
describe("Container should math route", () => {
  beforeEach(() => {
    jest.spyOn(console, "error");
  });

  afterEach(() => {
    //console.error.mockRestore();
  });
  test("It should display error message on click", async () => {
    const { queryByText, getByText } = render(<ErrButton />);
    expect(queryByText(/has error/i)).toBeFalsy();
    fireEvent.click(getByText(/trigger error/i));
    expect(queryByText(/has error/i)).toBeTruthy();
  });
});
