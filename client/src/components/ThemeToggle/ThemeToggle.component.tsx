import { setAppThemeAction } from "@home/store";
import { FormControlLabel, FormGroup, Switch } from "@material-ui/core";
import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import { StoreState } from "@src/model";

type ThemeType = "light" | "dark";

function flipValue(themeT: ThemeType): ThemeType {
  return themeT === "light" ? "dark" : "light";
}

interface ThemeToggleProps {}

let themeToggle = React.memo((props: ThemeToggleProps) => {
  const useThemeToggle = useSelector((state: StoreState) => {
    return state.app.theme;
  });
  const dispatch = useDispatch();
  function handleThemeChange(e) {
    dispatch(setAppThemeAction(flipValue(useThemeToggle)));
  }

  return (
    <FormGroup data-testid="toggle-theme-btn">
      <FormControlLabel
        control={
          <Switch
            color="secondary"
            inputProps={{ "data-testid": "theme-switch" } as any}
            checked={useThemeToggle === "dark"}
            onChange={handleThemeChange}
            value={useThemeToggle}
          />
        }
        label={useThemeToggle}
        data-testid="theme-label"
      />
    </FormGroup>
  );
});

export { themeToggle as ThemeToggle };
