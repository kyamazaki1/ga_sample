import * as React from "react";
import {
  cleanup,
  waitForElement,
  fireEvent,
  wait,
  getByText
} from "@testing-library/react";
import { ThemeToggle } from "./ThemeToggle.component";
import { renderWithRedux } from "@src/mock/utils.mock";
import { MOCK_APP_STATE, MOCK_APP_LIGHT_STATE } from "@src/fixtures";

describe("toggle theme button", () => {
  const dispatch = jest.fn();
  afterEach(cleanup);
  /*   test("renders with dark mode default", async () => {
    const { getByLabelText, getByTestId } = renderWithRedux(
      <ThemeToggle app={MOCK_APP_STATE} setAppThemeAction={dispatch} />
    );
    await waitForElement(() => getByLabelText(/dark/i));
    expect((getByTestId("theme-switch") as any).checked).toBe(true);
  });

  test("set light theme on switch", async () => {
    const {
      getByTestId,
      store,
      rerenderWithRedux,
      getByLabelText
    } = renderWithRedux(
      <ThemeToggle app={MOCK_APP_STATE} setAppThemeAction={dispatch} />
    );
    const switchTheme = getByTestId("theme-switch");

    fireEvent.click(switchTheme);
    // pass store next state
    rerenderWithRedux(<ThemeToggle />, MOCK_APP_LIGHT_STATE);
    await waitForElement(() => getByLabelText(/light/i));
  }); */
});
