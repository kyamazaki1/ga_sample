import * as React from "react";
import { Route } from "react-router-dom";

const appContainer = ({ component: Component, ...rest }) => {
  return <Route {...rest} render={matchProps => <Component {...matchProps} />} />;
};

export { appContainer as AppContainer };
