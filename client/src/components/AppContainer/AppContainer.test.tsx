import * as React from "react";
import { cleanup } from "@testing-library/react";
import App from "@dashboard/containers/Dashboard.container";
import { testIdGetter } from "@mock/utils.mock";
import { renderWithReduxRouter } from "@src/mock/utils.mock";

describe("Container should math route", () => {
  afterEach(cleanup);

  test("It should display the main page by default", async () => {
    const {
      container,
      history: { navigate },
      getByTestId
    } = renderWithReduxRouter(<App />, {
      route: "/something-that-does-not-match"
    });
    await navigate("/login");
    const { appTitle } = testIdGetter(["appTitle", "app-title"], getByTestId);
    expect(appTitle.textContent).toBe("App Page");
  });
});
