import * as React from "react";
import { Button, withStyles, Theme } from "@material-ui/core";
import { FormState } from "@src/model";
import { STYLES } from "@src/containers/PageTheme/PageTheme.container";

interface FormSubmitBtnProps {
  state: FormState;
  isInvalid: boolean;
}

interface BtnState {
  msg: string;
  props: Object;
}
interface MapBtnState {
  [prop: string]: BtnState;
}
const mapBtnState = {
  invalid: {
    msg: "submit",
    btnProps: {
      style: {
        color: "white"
      }
    }
  },
  valid: {
    msg: "submit",
    btnProps: {
      style: {
        color: "white"
      }
    }
  },

  pending: {
    msg: "loding",
    btnProps: {}
  },
  success: {
    msg: "success",
    btnProps: {
      style: {
        backgroundColor: "green",
        color: "white"
      }
    }
  },
  error: {
    msg: "error",
    btnProps: {
      style: {
        backgroundColor: "red",
        color: "white"
      }
    }
  }
};

const PillButton = withStyles((theme: Theme) => ({
  root: {
    borderRadius: "25px",
    padding: "7px 0"
  }
}))(Button);

const formSubmitBtn = React.memo((props: FormSubmitBtnProps) => {
  const { state, isInvalid } = props;

  const currentState = mapBtnState.hasOwnProperty(state)
    ? mapBtnState[state]
    : mapBtnState.error;
  return (
    <React.Fragment>
      <PillButton
        variant="contained"
        color="inherit"
        fullWidth={true}
        disabled={isInvalid}
        type="submit"
        id="login-submit"
        data-testid="login-submit"
        {...currentState.btnProps}
      >
        {currentState.msg}
      </PillButton>
    </React.Fragment>
  );
});

export { formSubmitBtn as FormSubmitBtn };
