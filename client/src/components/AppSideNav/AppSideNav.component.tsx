import * as React from "react";
import { useEffect, useState } from "react";
import { makeStyles, Theme } from "@material-ui/core";
import Button from "@material-ui/core/Button";

import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";

import Fab from "@material-ui/core/Fab";
import MenuIcon from "@material-ui/icons/Menu";

const DRAWER_WIDTH: number = 230;
const TOPBAR_HEIGHT: number = 70;
const useStyles = makeStyles((theme: Theme) => {
  return {
    menuBtn: {
      height: TOPBAR_HEIGHT
    },
    permanentBar: {
      position: "absolute",
      height: "100vh",
      top: TOPBAR_HEIGHT,
      backgroundColor: `${theme.palette.background.paper}`
    },
    listWrap: {
      position: "relative",
      width: DRAWER_WIDTH,
      backgroundColor: `${theme.palette.background.paper}`,
      height: "100%"
    },

    float_btn: {
      position: "absolute",
      width: 25,
      height: 25,
      minHeight: "inherit",
      top: "45%",
      left: DRAWER_WIDTH - 5,
      zIndex: -10
    },
    nav: {
      top: TOPBAR_HEIGHT,
      backgroundColor: "transparent",
      width: DRAWER_WIDTH + 30
    }
  };
});

interface AppSideNav {}
const toggleDrawer = (action: Function, ...args: Array<unknown>) => (
  event: React.KeyboardEvent | React.MouseEvent
) => {
  if (
    event &&
    event.type === "keydown" &&
    ((event as React.KeyboardEvent).key === "Tab" ||
      (event as React.KeyboardEvent).key === "Shift")
  ) {
    return;
  }
  action.call(null, ...args);
};

const appSideNav = React.memo(
  (props: AppSideNav): JSX.Element => {
    const classes = useStyles(props);

    const [useIsOpen, setUseIsOpen] = useState(false);
    const [usePeakNavWidth, setUsePeakNavWidth] = useState(0);

    function isHovered(appWidth: 0 | 30): void {
      setUsePeakNavWidth(appWidth);
    }
    const sideList = () => (
      <React.Fragment>
        <div
          className={classes.listWrap}
          role="presentation"
          onClick={toggleDrawer(setUseIsOpen, false)}
          onKeyDown={toggleDrawer(setUseIsOpen, false)}
        >
          <Fab
            color="primary"
            aria-label="open menu"
            className={classes.float_btn}
            onClick={toggleDrawer(setUseIsOpen, false)}
          >
            <ChevronLeftIcon />
          </Fab>
          <List>
            {["Inbox"].map((text, index) => (
              <ListItem button key={text}>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
          <Divider />
          <List>
            {["All mail", "Trash", "Spam"].map((text, index) => (
              <ListItem button key={text}>
                <ListItemIcon>
                  {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
      </React.Fragment>
    );
    return (
      <div>
        <Button
          onClick={toggleDrawer(setUseIsOpen as any, true)}
          onMouseEnter={() => isHovered(30)}
          onMouseLeave={() => isHovered(0)}
          className={classes.menuBtn}
        >
          <MenuIcon />
        </Button>
        <div
          onMouseEnter={() => isHovered(30)}
          onMouseLeave={() => isHovered(0)}
          className={classes.permanentBar}
          style={{ width: usePeakNavWidth }}
        >
          <Fab
            color="primary"
            aria-label="open menu"
            className={classes.float_btn}
            style={{ left: usePeakNavWidth - 5 }}
            onClick={toggleDrawer(setUseIsOpen, true)}
          >
            <ChevronRightIcon />
          </Fab>
        </div>

        <Drawer
          onClick={toggleDrawer(setUseIsOpen, false)}
          classes={{
            paper: classes.nav
          }}
          variant="persistent"
          anchor="left"
          open={useIsOpen}
          elevation={0}
        >
          {sideList()}
        </Drawer>
      </div>
    );
  }
);

export { appSideNav as AppSideNav };
