import * as React from "react";
import { makeStyles } from "@material-ui/styles";
import SearchIcon from "@material-ui/icons/Search";
import { LabeledIconBtn } from "@src/components/LabeledIconBtn/LabeledIconBtn.componentt";
import TextField from "@material-ui/core/TextField";
import Popover, { PopoverOrigin } from "@material-ui/core/Popover";

const useStyles = makeStyles({
  root: { width: "80%" }
});

interface IconPopOverProps {
  children: React.ReactNode;
  origin: PopoverOrigin;
  transform: PopoverOrigin;
  eventTarget: HTMLButtonElement;
  resetAnchor: Function;
  isFullWith: boolean;
  idName: string;
}

function iconPopOver(props: IconPopOverProps): JSX.Element {
  const {
    children,
    origin,
    transform,
    eventTarget,
    resetAnchor,
    isFullWith,
    idName
  } = props;
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  React.useEffect(() => {
    setAnchorEl(eventTarget);
  }, [eventTarget]);
  const classes = useStyles(props);
  const open = Boolean(anchorEl);
  const id = open ? idName : undefined;

  const handleClose = () => {
    resetAnchor(null);
  };
  return (
    <Popover
      id={id}
      open={open}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={origin}
      transformOrigin={transform}
      classes={{
        paper: isFullWith ? classes.root : null
      }}
    >
      {children}
    </Popover>
  );
}
iconPopOver.defaultProps = {
  origin: {
    vertical: "bottom",
    horizontal: "center"
  },
  transform: {
    vertical: "top",
    horizontal: "left"
  },
  isFullWith: false
};
export { iconPopOver as IconPopOver };
