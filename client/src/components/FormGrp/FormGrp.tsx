import * as React from "react";
import { FormGroupWrapper, FormHint } from "@src/Ui/form";
import { FormInput } from "../FormInput/FormInput.component";
import * as PropTypes from "prop-types";
// stati value of inputs/hint .Could'nt come up with better name
interface FormGrpSpec {
  id: string;
  detail: {
    name: string;
    type: string;
    placeholder: string;
  };
  type: string;
  hintTestId: string;
}
interface FormGrpProps<T> {
  isValid: boolean;
  spec: FormGrpSpec;
  hint: string;
  value: T;
  handleUpdate: Function;
}
function formGrp<T>(props: FormGrpProps<T>): JSX.Element {
  const { hint, isValid, spec, value } = props;
  return (
    <FormGroupWrapper isValid={isValid}>
      <FormInput<T>
        id={spec.id}
        detail={spec.detail}
        value={value as any}
        handleUpdate={props.handleUpdate}
        name={spec.type}
      />
      <FormHint data-testid={spec.hintTestId}>{hint}</FormHint>
    </FormGroupWrapper>
  );
}

formGrp.propTypes = {
  handleUpdate: PropTypes.func
};

export { formGrp as FormGrp };
