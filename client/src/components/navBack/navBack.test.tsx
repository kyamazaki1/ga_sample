import * as React from "react";

import { cleanup, render, fireEvent } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router } from "react-router";
import { Navback } from "./nav-back.component";
function renderWithRouter(ui, { route = "/", history = createMemoryHistory({ initialEntries: [route] }) } = {}) {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    history
  };
}
// history={{ goBack: fn }}
describe("Navback", () => {
  const fn = jest.fn();
  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);
  test("It should pop one history", () => {
    const { getByText, container } = renderWithRouter(
      <Navback
        {...{
          history: {
            goBack: fn
          }
        }}
      />
    );
    fireEvent.click(getByText(/back/i));

    expect(fn).toHaveBeenCalled();
  });
});
