import * as React from "react";
interface NavBackProps {
  history?: any;
}
function navBack(props: NavBackProps): JSX.Element {
  return <button onClick={props.history.goBack}>back</button>;
}

export { navBack as Navback };
