import * as React from "react";
import { useState, useEffect } from "react";
import { makeStyles, Theme } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import RefreshIcon from "@material-ui/icons/Refresh";
import HelpIcon from "@material-ui/icons/Help";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import MenuIcon from "@material-ui/icons/Menu";

import { ThemeToggle } from "@components/ThemeToggle/ThemeToggle.component";
import { AppBarLogo } from "@components/AppTopBar/AppBarLogo/AppBarLogo.component";
import { UserResponse } from "@model/user";
import { AppLink } from "@components/AppLink/AppLink";
import { AppBarAvatar } from "./AppBarAvatar/AppBarAvatar";
import { AppSearchBtn } from "./AppSearchBtn/AppSearchBtn.component";
import { AppReportBtn } from "./AppReportBtn/AppReportBtn.component";
import { AppToolsBtn } from "./AppToolsBtn/AppToolsBtn.component";
import { AppBarNav } from "./AppBarNav/AppBarNav.component";
const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
    padding: 0
  },
  heading: {
    display: "inline-flex",
    alignItems: "center"
  },
  avatar: {
    margin: 10
  },
  logo: {},
  nav: {
    width: "30%",
    display: "flex",
    justifyContent: "space-evenly"
  },
  actions: {
    display: "inline-flex",
    alignItems: "center",
    justifyContent: "flex-end"
  },
  divider: {
    height: 40
  },
  menuBtn: {}
}));

interface AppTopBar {
  user: UserResponse;
  toggleNav: Function;
  slideState: boolean;
}

const PATH = [
  {
    name: "All Campaigns",
    path: "/"
  },
  {
    name: "Smart Campaign",
    path: "/"
  }
];

const APP_NAV = {
  path: PATH,
  tooltip: "All Campaigns > smart Campaign"
};
const appTopBar = React.memo(
  (props: AppTopBar): JSX.Element => {
    const { user, toggleNav, slideState } = props;

    const classes = useStyles(props);

    const [useIsOpen, setUseIsOpen] = useState(slideState);

    function toggleDrawer(b: boolean) {
      setUseIsOpen(b);
    }
    useEffect(() => {
      toggleNav(useIsOpen);
    }, [useIsOpen]);
    useEffect(() => {
      setUseIsOpen(slideState);
    }, [slideState]);

    return (
      <AppBar data-testid="app-bar" color="inherit" position="sticky">
        <Toolbar className={classes.toolbar}>
          <div className={classes.heading}>
            <Button
              onClick={() => toggleDrawer(!useIsOpen)}
              className={classes.menuBtn}
            >
              <MenuIcon />
            </Button>

            <AppBarLogo />
            <Divider orientation="vertical" className={classes.divider} />

            <AppBarNav appNav={APP_NAV} />
          </div>

          <div className={classes.actions}>
            <AppSearchBtn />
            <AppReportBtn />
            <AppToolsBtn />
            <Divider orientation="vertical" className={classes.divider} />
            <Tooltip title="refresh">
              <IconButton aria-label="refresh">
                <RefreshIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="help">
              <IconButton aria-label="help">
                <HelpIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="notifications">
              <IconButton aria-label="notifications">
                <NotificationsIcon />
              </IconButton>
            </Tooltip>
            <AppBarAvatar user={user} />
            <ThemeToggle />
          </div>
        </Toolbar>
      </AppBar>
    );
  }
);

export { appTopBar as AppTopBar };
