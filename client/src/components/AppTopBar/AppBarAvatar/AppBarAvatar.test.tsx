import * as React from "react";
import { cleanup, render, fireEvent } from "@testing-library/react";
import { MOCK_APP_USER } from "@src/fixtures";
import { AppBarAvatar } from "./AppBarAvatar";
import { renderWithReduxRouter } from "@src/mock/utils.mock";
import Login from "@login/containers/Login.container";
describe("App top bar component,", () => {
  afterEach(cleanup);
  it("should not show menus", () => {
    const { getByTestId, getByRole } = render(<AppBarAvatar user={MOCK_APP_USER} />);
    const avatar = getByTestId("avatar");
    const itemsWrap = getByRole("document");
    expect(avatar).toBeDefined();
    expect(itemsWrap.style.opacity).toBe("0");
  });
  it("should show menus", () => {
    const { getByTestId, getByRole } = render(<AppBarAvatar user={MOCK_APP_USER} />);
    const avatarBtn = getByTestId("avatar");
    const itemsWrap = getByRole("document");
    fireEvent.click(avatarBtn);
    expect(itemsWrap.style.opacity).toBe("1");
  });
  //MuiPaper-root
  it("should redirect to sigin page", async () => {
    const { getByTestId } = render(<AppBarAvatar user={MOCK_APP_USER} />);
    const logout = getByTestId("logout-btn");
    fireEvent.click(logout);
    const route = "/login";
    const { getByLabelText } = renderWithReduxRouter(<Login />, { route });
    const loginform = getByLabelText(/login/i);
    expect(loginform).toBeDefined();
  });
});
