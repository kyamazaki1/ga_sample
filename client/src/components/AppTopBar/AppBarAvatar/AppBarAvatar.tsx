import * as React from "react";
import { makeStyles, Avatar, Button, Menu, MenuItem } from "@material-ui/core";
import * as AvatarImg from "@src/assets/img/fake_avatar.png";
import Tooltip from "@material-ui/core/Tooltip";

import { UserResponse } from "@model/user";
import { logout } from "@store/store.utils";

const useStyles = makeStyles({
  avatar: {
    margin: 10
  }
});

interface AppBarAvatar {
  user: UserResponse;
}

function appBarAvatar(props: AppBarAvatar): JSX.Element {
  const {
    user: { username }
  } = props;
  const classes = useStyles({});
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  function handleClick(event: React.MouseEvent<HTMLButtonElement>) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function handleLogOut() {
    logout();
    handleClose();
  }
  const u = username ? username : "";
  return (
    <div>
      <Tooltip title={u}>
        <Button
          data-testid="avatar-btn"
          type="button"
          aria-controls="user-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <Avatar
            data-testid="avatar"
            alt={username}
            src={AvatarImg}
            className={classes.avatar}
          />
        </Button>
      </Tooltip>

      <Menu
        id="user-menu"
        data-testid="user-menu-root"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem
          className="menu__item"
          data-testid="profile-btn"
          onClick={handleClose}
        >
          Profile
        </MenuItem>
        <MenuItem
          className="menu__item"
          data-testid="account-btn"
          onClick={handleClose}
        >
          My account
        </MenuItem>
        <MenuItem
          className="menu__item"
          data-testid="logout-btn"
          onClick={handleLogOut}
        >
          Logout
        </MenuItem>
      </Menu>
    </div>
  );
}
appBarAvatar.defaultProps = {
  user: {
    username: ""
  }
};
export { appBarAvatar as AppBarAvatar };
