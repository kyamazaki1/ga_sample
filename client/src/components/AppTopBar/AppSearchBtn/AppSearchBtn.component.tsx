import * as React from "react";
import { makeStyles } from "@material-ui/styles";
import SearchIcon from "@material-ui/icons/Search";
import { LabeledIconBtn } from "@src/components/LabeledIconBtn/LabeledIconBtn.componentt";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import { IconPopOver } from "@src/components/IconPopover/IconPopover.component";

const useStyles = makeStyles({
  form: { width: 200, padding: 10 }
});

interface AppSearchBtnProps {}

function appSearchBtn(props: AppSearchBtnProps): JSX.Element {
  const classes = useStyles(props);
  //TODO: abstract popover hooks
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const inputChng = (ev: any) => {
    console.log(ev.target);
  };
  return (
    <React.Fragment>
      <LabeledIconBtn label="search" onAction={handleClick}>
        <SearchIcon />
      </LabeledIconBtn>
      <IconPopOver
        eventTarget={anchorEl}
        resetAnchor={handleClose}
        origin={{
          vertical: "center",
          horizontal: "right"
        }}
        transform={{
          vertical: "center",
          horizontal: "right"
        }}
        idName="search-input"
      >
        <form className={classes.form} noValidate autoComplete="off">
          <TextField
            id="search"
            label=""
            onChange={inputChng}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              )
            }}
          />
        </form>
      </IconPopOver>
    </React.Fragment>
  );
}

export { appSearchBtn as AppSearchBtn };
