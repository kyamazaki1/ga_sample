import * as React from "react";
import { useState, useEffect } from "react";
import { makeStyles, Theme } from "@material-ui/core";
import Tooltip from "@material-ui/core/Tooltip";

import { AppLink } from "@components/AppLink/AppLink";
import { AppNavLink } from "@src/model/app";

const useStyles = makeStyles((theme: Theme) => ({
  navs: {
    padding: `0 0 5px 20px`
  },
  tooltip: {
    fontSize: theme.typography.caption.fontSize,
    padding: 15
  },
  nas__sep: {
    fontSize: 10
  }
}));

interface AppBarNavProps {
  appNav: {
    path: Array<AppNavLink>;
    tooltip: string;
  };
}

const PATH = [
  {
    name: "All Campaigns",
    path: "/"
  },
  {
    name: "Smart Campaign",
    path: "/"
  }
];

const APP_NAV = {
  path: PATH,
  tooltip: "All Campaigns > smart Campaign"
};
const appBarNav = React.memo(
  (props: AppBarNavProps): JSX.Element => {
    const {
      appNav: { path, tooltip }
    } = props;

    const classes = useStyles(props);

    function setNav(predicate: boolean): Object {
      if (predicate) {
        return {
          fontSize: 13,
          fontWeight: 200
        };
      }
      return {
        fontSize: 16
      };
    }
    return (
      <Tooltip
        title={tooltip}
        classes={{
          tooltip: classes.tooltip
        }}
      >
        <nav>
          {path.map((n: AppNavLink, idx: number) => (
            <div className={classes.navs} key={n.name}>
              <AppLink name={n.name} path={n.path} style={setNav(idx === 0)} />
              <span
                style={{ opacity: idx == 0 ? 1 : 0 }}
                className={classes.nas__sep}
              >
                &nbsp; > &nbsp;
              </span>
            </div>
          ))}
        </nav>
      </Tooltip>
    );
  }
);

export { appBarNav as AppBarNav };
