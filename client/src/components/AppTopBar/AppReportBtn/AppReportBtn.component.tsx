import * as React from "react";
import { makeStyles } from "@material-ui/styles";
import AssessmentIcon from "@material-ui/icons/Assessment";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import EventNoteIcon from "@material-ui/icons/EventNote";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import LibraryAddIcon from "@material-ui/icons/LibraryAdd";
import HourglassFullIcon from "@material-ui/icons/HourglassFull";
import SettingsApplicationsIcon from "@material-ui/icons/SettingsApplications";
import PaymentIcon from "@material-ui/icons/Payment";
import { LabeledIconBtn } from "@src/components/LabeledIconBtn/LabeledIconBtn.componentt";

import { IconPopOver } from "@src/components/IconPopover/IconPopover.component";
import { AppNavLink } from "@src/model/app";
import { AppLink } from "@src/components/AppLink/AppLink";
const useStyles = makeStyles({
  list__wrap: {
    display: "flex",
    height: "100%",
    justifyContent: "space-between"
  },
  list: {
    width: "100%",
    borderLeft: "1px solid  rgba(255, 255, 255, 0.12)"
  },
  list__icon: {
    minWidth: 35
  }
});

interface AppReportBtnProps {}

const PATHS: Array<AppNavLink> = [
  {
    name: "dashboard",
    path: "/overiew"
  },
  {
    name: "pages",
    path: "/"
  },
  {
    name: "more",
    path: "/"
  },
  {
    name: "pikach",
    path: "/"
  },
  {
    name: "generate",
    path: "/"
  }
];

function appReportBtn(props: AppReportBtnProps): JSX.Element {
  const classes: any = useStyles(props);

  //TODO: abstract popover hooks
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <React.Fragment>
      <LabeledIconBtn label="report" onAction={handleClick}>
        <AssessmentIcon />
      </LabeledIconBtn>
      <IconPopOver
        eventTarget={anchorEl}
        resetAnchor={handleClose}
        idName="report"
        isFullWith
      >
        <div className={classes.list__wrap}>
          <List className={classes.list}>
            <ListItem>
              <ListItemIcon classes={{ root: classes.list__icon }}>
                <EventNoteIcon />
              </ListItemIcon>
              <ListItemText primary="Planning" />
            </ListItem>

            {PATHS.map((p: AppNavLink, idx: number) => (
              <ListItem key={`${p.name}__first`}>
                <AppLink
                  key={`${p.name}__first_link`}
                  name={p.name}
                  path={p.path}
                />
              </ListItem>
            ))}
          </List>
          <List className={classes.list}>
            <ListItem>
              <ListItemIcon classes={{ root: classes.list__icon }}>
                <LibraryBooksIcon />
              </ListItemIcon>
              <ListItemText primary="Shared library" />
            </ListItem>

            {PATHS.map((p: AppNavLink) => (
              <ListItem key={`${p.name}__second`}>
                <AppLink
                  key={`${p.name}__second_link`}
                  name={p.name}
                  path={p.path}
                />
              </ListItem>
            ))}
          </List>
          <List className={classes.list}>
            <ListItem>
              <ListItemIcon classes={{ root: classes.list__icon }}>
                <LibraryAddIcon />
              </ListItemIcon>
              <ListItemText primary="Bulked actions" />
            </ListItem>
            {PATHS.map((p: AppNavLink) => (
              <ListItem key={`${p.name}__third`}>
                <AppLink
                  key={`${p.name}__third_link`}
                  name={p.name}
                  path={p.path}
                />
              </ListItem>
            ))}
          </List>
          <List className={classes.list}>
            <ListItem>
              <ListItemIcon classes={{ root: classes.list__icon }}>
                <HourglassFullIcon />
              </ListItemIcon>
              <ListItemText primary="Measurement" />
            </ListItem>
            {PATHS.map((p: AppNavLink) => (
              <ListItem key={`${p.name}__fourth`}>
                <AppLink
                  key={`${p.name}__fourth_link`}
                  name={p.name}
                  path={p.path}
                />
              </ListItem>
            ))}
          </List>
          <List className={classes.list}>
            <ListItem>
              <ListItemIcon classes={{ root: classes.list__icon }}>
                <SettingsApplicationsIcon />
              </ListItemIcon>
              <ListItemText primary="Setup" />
            </ListItem>
            {PATHS.map((p: AppNavLink) => (
              <ListItem key={`${p.name}__fifth`}>
                <AppLink
                  key={`${p.name}__fifth_link`}
                  name={p.name}
                  path={p.path}
                />
              </ListItem>
            ))}
          </List>
          <List className={classes.list}>
            <ListItem>
              <ListItemIcon classes={{ root: classes.list__icon }}>
                <PaymentIcon />
              </ListItemIcon>
              <ListItemText primary="Billing" />
            </ListItem>
            {PATHS.map((p: AppNavLink) => (
              <ListItem key={`${p.name}__sixth`}>
                <AppLink
                  key={`${p.name}__sixth_link`}
                  name={p.name}
                  path={p.path}
                />
              </ListItem>
            ))}
          </List>
        </div>
      </IconPopOver>
    </React.Fragment>
  );
}

export { appReportBtn as AppReportBtn };
