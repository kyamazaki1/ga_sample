import * as React from "react";
import * as BlueLogo from "@src/assets/img/logo_blue.png";
import * as WhiteLogo from "@src/assets/img/logo_white.png";
import styled from "styled-components";
import { ResponsiveImg } from "@src/Ui/img";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  root: (props: any) => ({
    display: "inline-flex",
    "justify-content": "center",
    "align-items": "center",
    height: "50px",
    ...props.root
  }),
  app__title: (props: any) => ({
    fontSize: "1.8rem",
    padding: "0 1.5rem",
    ...props.app__title
  }),
  logo: {
    padding: "0 10px"
  }
});

export const LogoImgWrap = styled.div`
  display: inline-flex;
  justify-content: center;
  align-items: center;
`;
interface AppBarLogoProps {
  root?: Object;
  app__title?: Object;
  altLogo?: boolean;
}

function appBarLogo(props: AppBarLogoProps): JSX.Element {
  const classes: any = useStyles(props);

  return (
    <div className={classes.root}>
      <ResponsiveImg
        className={classes.logo}
        src={props.altLogo ? WhiteLogo : BlueLogo}
        maxHeight={3}
      />
    </div>
  );
}
appBarLogo.defaultProps = {
  altLogo: false
};

export { appBarLogo as AppBarLogo };
