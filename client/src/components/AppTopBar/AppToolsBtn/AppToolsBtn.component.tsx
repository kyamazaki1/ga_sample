import * as React from "react";
import { makeStyles } from "@material-ui/styles";
import BuildIcon from "@material-ui/icons/Build";
import { LabeledIconBtn } from "@src/components/LabeledIconBtn/LabeledIconBtn.componentt";
import { IconPopOver } from "@src/components/IconPopover/IconPopover.component";
import { AppNavLink } from "@src/model/app";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import { AppLink } from "@src/components/AppLink/AppLink";

const useStyles = makeStyles({});
const PATHS: Array<AppNavLink> = [
  {
    name: "dashboard",
    path: "/dashboard"
  },
  {
    name: "pages",
    path: "/"
  },
  {
    name: "generate",
    path: "/"
  }
];
interface AppToolsBtnProps {}

function appToolsBtn(props: AppToolsBtnProps): JSX.Element {
  const classes = useStyles(props);

  //TODO: abstract popover hooks
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <React.Fragment>
      <LabeledIconBtn label="tools & settings" onAction={handleClick}>
        <BuildIcon />
      </LabeledIconBtn>
      <IconPopOver
        eventTarget={anchorEl}
        resetAnchor={handleClose}
        origin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        transform={{
          vertical: "top",
          horizontal: "right"
        }}
        idName="tools"
      >
        <List>
          {PATHS.map((p: AppNavLink) => (
            <ListItem key={p.name}>
              <AppLink key={p.name} name={p.name} path={p.path} />
            </ListItem>
          ))}
        </List>
      </IconPopOver>
    </React.Fragment>
  );
}

export { appToolsBtn as AppToolsBtn };
