import * as React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import { NavSubList } from "../NavSubList/NavSubList.component";
import { NavItems } from "@src/containers/ProtectedPage/ProtectedPage";
interface NavListProps {
  navItems: NavItems;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nav: {
      width: "100%",
      borderRight: "1px solid rgba(255, 255, 255, 0.12)",
      height: "100vh",
      marginLeft: -5
    },
    link: {
      display: "flex",
      alignItems: "flex-end"
    },
    link__icon: {
      justifyContent: "flex-end"
    }
  })
);

function navList(props: NavListProps): JSX.Element {
  const { navItems } = props;
  const classes = useStyles(props);

  return (
    <nav className={classes.nav}>
      <NavSubList
        links={navItems.overview.list}
        listLabel={navItems.overview.label}
      />
      <Divider variant="middle" />
      <NavSubList
        links={navItems.campaigns.list}
        listLabel={navItems.campaigns.label}
      />

      <Divider variant="middle" />
      <NavSubList
        links={navItems.settings.list}
        listLabel={navItems.settings.label}
      />

      <Divider variant="middle" />
      <NavSubList
        links={navItems.history.list}
        listLabel={navItems.history.label}
      />
    </nav>
  );
}
export { navList as NavList };
