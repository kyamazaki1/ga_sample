import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";
import { CampaignsGraph } from "../components/CampaignqGaph/CampaignGraph.component";
import { DateFilter } from "../components/DateFilter/DateFilter.component";
import { useDispatch, useSelector } from "react-redux";
import { trySample } from "@src/PageDashboard/store";
import { StoreState } from "@src/model";
import { CampaignsData } from "../components/CampaignsData/CampaignsData.component";

interface CampaignsProps {}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      display: "flex",
      width: "50%",
      flexWrap: "wrap",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      flex: 1,
      margin: "0 auto"
    },
    icon: {
      color: "#FBBC04",
      fontSize: theme.spacing(10)
    },
    title: {
      margin: "30px 0"
    },
    sub_title: {},
    root: {
      overflow: "auto"
    }
  })
);

function Campaigns(props: CampaignsProps): JSX.Element {
  const {} = props;
  const classes = useStyles(props);
  const dashboard = useSelector((state: StoreState) => {
    return state.dashboard;
  });
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(trySample());
  }, []);

  return (
    <section className={classes.root}>
      <DateFilter />
      <CampaignsGraph />
      <CampaignsData />
    </section>
  );
}
export default Campaigns;
