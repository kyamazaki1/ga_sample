import * as d3 from "d3";
import d3Tip from "d3-tip";
import {
  Scene,
  Margin,
  applyAttr,
  setSvgScene
} from "@src/components/d3/utils";

const margin = { top: 20, right: 150, bottom: 40, left: 40 };

const SVG_WIDTH = 900 - (margin.left - margin.right);
const SVG_HEIGHT = 200 - (margin.top - margin.bottom);

//

export class D3Chart {
  svg: d3.Selection<any, any, any, any>;
  circles: SVGElement;
  xAxisGroup: d3.Selection<any, any, any, any>;
  yAxisGroup: d3.Selection<any, any, any, any>;
  label: d3.Selection<any, any, any, any>;
  x: d3.ScaleTime<any, any>;
  y: d3.ScaleLinear<any, any>;
  focus;
  data;
  constructor(el: React.ReactInstance) {
    this.init(el);
  }

  init = el => {
    //margin convetion
    this.svg = setSvgScene(
      el,
      { height: SVG_HEIGHT, width: SVG_WIDTH },
      margin
    );
    // set range
    //init range. domain in update once have data
    this.x = d3.scaleTime().range([0, SVG_WIDTH]);
    this.y = d3.scaleLinear().range([SVG_HEIGHT, 0]);

    //axis
    this.xAxisGroup = this.svg
      .append("g")
      .attr("transform", `translate(0, ${SVG_HEIGHT})`);
    this.yAxisGroup = this.svg.append("g");

    //label

    this.focus = this.svg
      .append("g")
      .attr("class", "focus")
      .style("display", "none");

    this.focus
      .append("line")
      .attr("class", "x-hover-line hover-line")
      .attr("y1", 0)
      .attr("y2", SVG_HEIGHT);

    this.focus
      .append("line")
      .attr("class", "y-hover-line hover-line")
      .attr("x1", 0)
      .attr("x2", SVG_WIDTH);

    this.focus.append("circle").attr("r", 7.5);

    this.focus
      .append("text")
      .attr("x", 15)
      .attr("dy", ".31em");

    this.svg
      .append("rect")
      .attr("width", SVG_WIDTH)
      .attr("height", SVG_HEIGHT)
      .attr("fill", "transparent")
      .on("mouseover", () => {
        this.focus.style("display", null);
      })
      .on("mouseout", () => {
        this.focus.style("display", "none");
      })
      .on("mousemove", this.mousemove);
  };

  update = (data: Array<any>, name: string) => {
    if (data.length === 0) {
      return null;
    }
    this.data = data;
    //domain
    //populate with value
    this.x.domain(d3.extent(data, d => d.date));
    this.y.domain([
      d3.min(data, d => d.value) / 1.005,
      d3.max(data, d => d.value) * 1.005
    ]);

    //axis
    //populate with value

    const axisXCall = d3.axisBottom(this.x).ticks(2);
    this.xAxisGroup.call(axisXCall);

    //Line
    const line = d3
      .line()
      .x((d: any) => this.x(d.date))
      .y((d: any) => this.y(d.value));

    //draw
    this.svg
      .append("path")
      .attr("fill", "none")
      .attr("stroke", "grey")
      .attr("stroke-with", "3px")
      .attr("d", line(data as any));
  };

  mousemove = () => {
    if (!this.data) {
      return;
    }
    const g = this.svg["_groups"][0][0]; //selection
    const bisectDate = d3.bisector((d: any) => d.date).left;
    const x0: any = this.x.invert(d3.mouse(g)[0]);
    const i = bisectDate(this.data, x0, 1);
    const d0 = this.data[i - 1];
    const d1 = this.data[i];
    const d = x0 - d0.date > d1.date - x0 ? d1 : d0;
    this.focus.attr(
      "transform",
      "translate(" + this.x(d.date) + "," + this.y(d.value) + ")"
    );
    this.focus.select("text").text(`date : ${d.date.toISOString().slice(0, 10)}
      value : ${d.value}
      `);
    this.focus.select(".x-hover-line").attr("y2", SVG_HEIGHT - this.y(d.value));
    this.focus.select(".y-hover-line").attr("x2", -this.x(d.date));
  };

  //tooltips code
}
