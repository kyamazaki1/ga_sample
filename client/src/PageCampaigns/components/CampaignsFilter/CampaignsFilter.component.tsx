import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Popover from "@material-ui/core/Popover";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import * as React from "react";
import { CampaignsSearchInput } from "./CampaignSearchInput/CampaignSearchInput.component";
import { CampaignsFiltersList } from "./CampainFiltersList/CampainFiltersList.component";
import { CampaignsFiltersResults } from "./CampaignFilterResults/CampaignFilterResults.component";

const MENUS = [
  {
    menuValue: "performance",
    menuLabel: "performance",
    list: [
      {
        viewValue: "clicks",
        value: "clicks"
      },
      {
        viewValue: "cost",
        value: "cost"
      },
      {
        viewValue: "impr",
        value: "impr"
      },
      {
        viewValue: "ctr",
        value: "ctr"
      },
      {
        viewValue: "interactions",
        value: "interactions"
      },
      {
        viewValue: "interaction vpc",
        value: "interaction_vpc"
      },
      {
        viewValue: "avg cpc",
        value: "avg_cpc"
      }
    ]
  },
  {
    menuValue: "conversions",
    menuLabel: "conversions",
    list: [
      {
        viewValue: "conv value",
        value: "conv_value"
      },
      {
        viewValue: "conv value /cost",
        value: "conv_value_cost"
      },
      {
        viewValue: "conv value /click",
        value: "conv_value_click"
      },
      {
        viewValue: "value conv",
        value: "value_conv"
      },
      {
        viewValue: "Conversions (by conv time)",
        value: "conv_time"
      },
      {
        viewValue: "Conversions value (by conv time)",
        value: "conv_val_time"
      },
      {
        viewValue: " value / conv (by conv time)",
        value: "val_time"
      },
      {
        viewValue: "all conv",
        value: "conv_all"
      }
    ]
  },
  {
    menuValue: "attribution",
    menuLabel: "attribution",
    list: [
      {
        viewValue: "conversion (current model)",
        value: "curr_conv"
      },
      {
        viewValue: "cost conv (current model)",
        value: "curr_cost_conv"
      },
      {
        viewValue: "rate conv (current model)",
        value: "curr_rate_conv"
      },
      {
        viewValue: "value conv (current model)",
        value: "curr_value_conv"
      },
      {
        viewValue: "value/ conv (current model)",
        value: "curr_conv_value"
      }
    ]
  },
  {
    menuValue: "competitive_metrics",
    menuLabel: "competitive metrics",
    list: [
      {
        viewValue: "search impr share",
        value: "search_impr_share"
      },
      {
        viewValue: "search top IS",
        value: "search_top_is"
      },
      {
        viewValue: "search abs top IS",
        value: "search_abs_top_is"
      },
      {
        viewValue: "search lost is",
        value: "search_lost_is"
      }
    ]
  },
  {
    menuValue: "call_details",
    menuLabel: "call details",
    list: [
      {
        viewValue: "phone calls",
        value: "phone_calls"
      },
      {
        viewValue: "phone impr",
        value: "phone_impr"
      },
      {
        viewValue: "PTR",
        value: "ptr"
      }
    ]
  }
];

function flattenMenus(
  list: Array<{
    menuValue: string;
    menuLabel: string;
    list: Array<{
      viewValue: string;
      value: string;
    }>;
  }>
): Array<{
  viewValue: string;
  value: string;
}> {
  return list.reduce((acc, curr) => [...acc, ...curr.list], []);
}
interface CampaignsFilterProps {}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    front: {
      width: 90
    },
    item: {
      padding: 0
    },
    btn: {
      borderBottom: "1px dotted grey"
    },
    label: {
      fontSize: 12,
      textTransform: "lowercase"
    }
  })
);

function campaignsFilter(props: CampaignsFilterProps): JSX.Element {
  const {} = props;
  const classes = useStyles(props);
  const [useFilterValue, setUseFilterValue] = React.useState<string>("clicks");
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const [useIsQuering, setUseIsQuering] = React.useState<boolean>(false);

  const [useQueryResults, setUseQueryResults] = React.useState<
    Array<{ value: string; viewValue: string }>
  >([]);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  function handleOnFiler(dispatch: {
    type: "FILTER_CAMPAIGN";
    payload: { viewValue: string; value: string };
  }) {
    const { payload } = dispatch;
    setUseFilterValue(payload.viewValue);
    handleClose();
    setUseIsQuering(false);
  }

  const open = Boolean(anchorEl);
  const id = open ? "applyFiler" : undefined;

  function handleOnQuery(b: boolean) {
    setUseIsQuering(b);
  }

  function handleOnQueryResult(l) {
    setUseQueryResults(l);
  }
  function renderList(isQ, defaultList = MENUS) {
    return isQ ? (
      <CampaignsFiltersResults
        results={useQueryResults}
        onApplyFilter={handleOnFiler}
      />
    ) : (
      defaultList.map((m: any) => (
        <CampaignsFiltersList
          key={m.menuValue}
          menuValue={m.menuValue}
          menuLabel={m.menuLabel}
          list={m.list}
          onApplyFilter={handleOnFiler}
        />
      ))
    );
  }

  return (
    <div>
      <Button
        aria-describedby={id}
        color="primary"
        onClick={handleClick}
        size="small"
        classes={{
          root: classes.btn,
          label: classes.label
        }}
      >
        {useFilterValue}
        <ArrowDropDownIcon />
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
      >
        <List aria-label="campaign filters ">
          <ListItem>
            <CampaignsSearchInput
              onQuery={handleOnQuery}
              list={flattenMenus(MENUS)}
              onQueryResult={handleOnQueryResult}
            />
          </ListItem>
          {renderList(useIsQuering)}
        </List>
      </Popover>
    </div>
  );
}

export { campaignsFilter as CampaignsFilter };
