import { Typography } from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";

interface CampaignsFiltersResultsProps {
  results: Array<{
    value: string;
    viewValue: string;
  }>;
  onApplyFilter: Function;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    item: {
      padding: 0
    },
    root__btn: {
      width: "100%",
      display: "flex",
      alignItems: "start",
      justifyContent: "start"
    }
  })
);

function campaignsFiltersResults(
  props: CampaignsFiltersResultsProps
): JSX.Element {
  const { results, onApplyFilter } = props;
  const classes = useStyles(props);
  function renderL(qLen) {
    return qLen.length === 0 ? (
      <ListItem>
        <Typography variant="body1">no results</Typography>
      </ListItem>
    ) : (
      qLen.map(r => (
        <ListItem
          button
          key={`${r.value}__resutls`}
          onClick={() => onApplyFilter({ type: "FILTER_CAMPAIGN", payload: r })}
        >
          <Typography variant="body1">{r.viewValue} </Typography>
        </ListItem>
      ))
    );
  }

  return renderL(results);
}

export { campaignsFiltersResults as CampaignsFiltersResults };
