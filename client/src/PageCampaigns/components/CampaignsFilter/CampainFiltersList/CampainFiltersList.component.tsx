import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import * as React from "react";

interface CampaignsFiltersListProps {
  menuValue: string;
  menuLabel: string;
  list: Array<{
    value: string;
    viewValue: string;
  }>;
  onApplyFilter: Function;
  innerRef?: any;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    item: {
      padding: 0
    },
    root__btn: {
      width: "100%",
      display: "flex",
      alignItems: "start",
      justifyContent: "start"
    }
  })
);

function CampaignsFiltersList(props: any): JSX.Element {
  const { menuValue, list, onApplyFilter, menuLabel } = props;
  const classes = useStyles(props);

  return (
    <ListItem>
      <Tooltip
        placement="right-start"
        interactive
        title={
          <List aria-label={`${menuLabel} sub cat`}>
            {list.map(l => (
              <ListItem button key={l.value}>
                <ListItemText
                  primary={l.viewValue}
                  color="primary"
                  onClick={() =>
                    onApplyFilter({ type: "FILTER_CAMPAIGN", payload: l })
                  }
                />
              </ListItem>
            ))}
          </List>
        }
      >
        <Button
          className={classes.root__btn}
          endIcon={<ArrowRightIcon />}
          disableRipple
        >
          {menuLabel}
        </Button>
      </Tooltip>
    </ListItem>
  );
}

export { CampaignsFiltersList };
