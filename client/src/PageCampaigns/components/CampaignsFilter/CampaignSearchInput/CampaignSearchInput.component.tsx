import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import * as React from "react";

interface CampaignsSearchInputProps {
  onQuery: Function;
  list: Array<{ value: string; viewValue: string }>;
  onQueryResult: Function;
}
const useStyles = makeStyles((theme: Theme) => createStyles({}));

function campaignsSearchInput(props: CampaignsSearchInputProps): JSX.Element {
  const { onQuery, list, onQueryResult } = props;
  const classes = useStyles(props);

  const [useQ, setUseQ] = React.useState("");

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const q = event.target.value;
    setUseQ(q);
    onQuery(q.length > 0);
    onQueryResult(list.filter(l => l.value.includes(q)));
  };

  return (
    <FormControl>
      <Input
        value={useQ}
        onChange={handleInputChange}
        id="input-with-icon-adornment"
        startAdornment={
          <InputAdornment position="end">
            <SearchIcon />
          </InputAdornment>
        }
      />
    </FormControl>
  );
}

export { campaignsSearchInput as CampaignsSearchInput };
