import * as React from "react";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { CampaignsTable } from "./CampaignsTable/CampaignsTable.component";

interface CampaignsProps {}
const useStyles = makeStyles((theme: Theme) => createStyles({}));

function campaignsData(props: CampaignsProps): JSX.Element {
  const {} = props;
  const classes = useStyles(props);

  return <CampaignsTable />;
}

export { campaignsData as CampaignsData };
