import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";
import { useState, useEffect } from "react";
import * as d3 from "d3";
import { LineGraphComponent } from "../LineGraph/LineGraph.component";
import { CampaignsFilter } from "../CampaignsFilter/CampaignsFilter.component";
import TuneIcon from "@material-ui/icons/Tune";
import AllOutIcon from "@material-ui/icons/AllOut";
import IconButton from "@material-ui/core/IconButton";

const LIST = [
  {
    filter_label: "clicks",
    filter_value: "0",
    type: "first_data_filter"
  },
  {
    filter_label: "impressions",
    filter_value: "0",
    type: "second_data_filter"
  },
  {
    filter_label: "conversions",
    filter_value: "$0.00",
    type: "third_data_filter"
  },
  {
    filter_label: "cost",
    filter_value: "$0.00",
    type: "fourth_data_filter"
  }
];
const FILTERS = [
  {
    value: "clicks",
    viewValue: "Click"
  },
  {
    value: "impressions",
    viewValue: "Impressions"
  },
  {
    value: "ctr",
    viewValue: "CTR"
  },
  {
    value: "average_ctc",
    viewValue: "Avg. ctc"
  },
  {
    value: "cost",
    viewValue: "Cost"
  },
  {
    value: "conversions",
    viewValue: "Conversion"
  }
];
const parseYTime = d3.timeParse("%Y-%m-%dT%H:%M:%SZ");

const LINE_DATA = [
  { date: "2019-12-01T00:00:00Z", value: 771900 },
  { date: "2019-12-02T00:00:00Z", value: 771500 },
  { date: "2019-12-03T00:00:00Z", value: 770500 },
  { date: "2019-12-04T00:00:00Z", value: 770400 },
  { date: "2019-12-05T00:00:00Z", value: 771000 },
  { date: "2019-12-06T00:00:00Z", value: 772400 },
  { date: "2019-12-07T00:00:00Z", value: 774100 },
  { date: "2019-12-08T00:00:00Z", value: 776700 },
  { date: "2019-12-09T00:00:00Z", value: 777100 },
  { date: "2019-12-10T00:00:00Z", value: 779200 },
  { date: "2019-12-11T00:00:00Z", value: 782300 },
  { date: "2019-12-12T00:00:00Z", value: 754409 }
];

interface CampaignsProps {}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btns: {
      display: "flex",
      justifyContent: "flex-end",
      listStyle: "none"
    },
    btn: {
      padding: "0 10px"
    }
  })
);

function campaignsGraph(props: CampaignsProps): JSX.Element {
  const {} = props;
  const classes = useStyles(props);
  const [useData, setUseData] = useState([]);
  useEffect(() => setUseData(LINE_DATA), []);

  return (
    <React.Fragment>
      <ul className={classes.btns}>
        <li className={classes.btn}>
          <CampaignsFilter />
        </li>
        <li className={classes.btn}>
          <CampaignsFilter />
        </li>
        <li className={classes.btn}>
          <IconButton aria-label="expand">
            <AllOutIcon />
          </IconButton>
        </li>
        <li className={classes.btn}>
          <IconButton aria-label="expand">
            <TuneIcon />
          </IconButton>
        </li>
      </ul>

      <LineGraphComponent
        data={useData.map((data: any): any => ({
          ...data,
          date: parseYTime(data.date)
        }))}
      />
    </React.Fragment>
  );
}

export { campaignsGraph as CampaignsGraph };
