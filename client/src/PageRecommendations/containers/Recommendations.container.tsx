import { trySample } from "@dashboard/store";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";
import { ViewTypeBar } from "../components/ViewTypeBar/ViewTypeBar.component";
import EmojiObjectsIcon from "@material-ui/icons/EmojiObjects";
import Typography from "@material-ui/core/Typography";

interface RecommendationsProps {}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      display: "flex",
      width: "50%",
      flexWrap: "wrap",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      flex: 1,
      margin: "0 auto"
    },
    icon: {
      color: "#FBBC04",
      fontSize: theme.spacing(10)
    },
    title: {
      margin: "30px 0"
    },
    sub_title: {}
  })
);

let Recommendations = function(props: RecommendationsProps): JSX.Element {
  const classes = useStyles(props);

  return (
    <React.Fragment>
      <ViewTypeBar />

      <section className={classes.content}>
        <EmojiObjectsIcon className={classes.icon} />
        <Typography variant="h2" color="primary" className={classes.title}>
          you do not have any recommendations
        </Typography>
        <Typography
          variant="body1"
          color="primary"
          className={classes.sub_title}
        >
          Recommendations look at your account's performance history, your
          campaign settings and trends across Google to automatically generate
          suggestions that could improve your performance.
        </Typography>
      </section>
    </React.Fragment>
  );
};

export default Recommendations;
