import Paper from "@material-ui/core/Paper";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import DashboardIcon from "@material-ui/icons/Dashboard";
import TableChartIcon from "@material-ui/icons/TableChart";
import * as React from "react";
import { LabeledIconBtn } from "@src/components/LabeledIconBtn/LabeledIconBtn.componentt";

interface ViewTypeBarProps {}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {},
    page__bar: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      padding: `4px ${theme.spacing(3)}px`
    }
  })
);

function viewTypeBar(props: ViewTypeBarProps): JSX.Element {
  const classes = useStyles(props);

  return (
    <Paper className={classes.page__bar}>
      <Typography variant="h1" className={classes.title} color="primary">
        Recommandations
      </Typography>

      <div>
        <LabeledIconBtn label="Table" onAction={null}>
          <TableChartIcon />
        </LabeledIconBtn>
        <LabeledIconBtn label="Card" onAction={null}>
          <DashboardIcon />
        </LabeledIconBtn>
      </div>
    </Paper>
  );
}

export { viewTypeBar as ViewTypeBar };
