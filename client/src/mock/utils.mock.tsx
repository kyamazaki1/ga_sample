import * as React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { AppReducer } from "@home/store";
import { createStore } from "redux";
import { MOCK_APP_USER, MOCKED_LOGGED_USER, MOCK_STORE_STATE } from "@src/fixtures";
import { Route, Router } from "react-router";
import { createMemorySource, LocationProvider, createHistory } from "@reach/router";
import ProtectedPage from "@src/containers/ProtectedPage/ProtectedPage";
import LoginContainer from "@login/containers/Login.container";
import { createBrowserHistory } from "history";
import { StoreState } from "@src/model/index";

//https://gist.github.com/darekzak/0c56bd9f1ad6e876fd21837feee79c50
/**
 * mock connect app to redux && abality of rerender props by passing next store state
 * usage
 *  const { getByTestId, store, rerenderWithRedux, getByLabelText } = renderWithRedux(
 *    <ThemeToggle app={MOCK_APP_STATE} setAppThemeAction={dispatch} />
 *     );
 *     fireEvent.click(switchTheme);
 *
 *    // pass store next state
 * rerenderWithRedux(<ThemeToggle />, MOCK_APP_LIGHT_STATE);
 *
 * @export
 * @param {JSX.Element} ui
 * @param {*} [{ initialState = MOCK_APP_STATE, store = createStore(AppReducer, initialState) }={}]
 * @param {*} [renderFn=render]
 * @param {string} [action={ type: "MOCK", payload: null }]
 * @returns
 */
export function renderWithRedux(
  ui: JSX.Element,
  { initialState = MOCK_STORE_STATE, store = createStore(AppReducer, initialState) } = {},
  renderFn = render,
  action = { type: "MOCK", payload: null }
) {
  const obj: any = {
    ...renderFn(<Provider store={store}>{ui}</Provider>),
    store
  };
  obj.rerenderWithRedux = (el: JSX.Element, nextState: StoreState) => {
    if (nextState) {
      store.replaceReducer(() => nextState as any);
      store.dispatch(action);
      store.replaceReducer(AppReducer);
    }
    return renderWithRedux(el, { store }, obj.rerender);
  };
  return obj;
}

// this is a handy function that I would utilize for any component
// that relies on the router being in context

export function renderWithReduxRouter(ui, { route = "/", history = createHistory(createMemorySource(route)) } = {}) {
  return {
    ...renderWithRedux(<LocationProvider history={history}>{ui}</LocationProvider>),
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    history
  };
}
/**
 * trigger a route nav with path && user as rge for authentification
 * usage
 * go to protected page renderWithTopReduxRouter(AppContainer, "/");
 * test auth of unath page  -> renderWithTopReduxRouter(AppContainer, "/", INIT_APP_USER);
 * @export
 * @param {*} ui
 * @param {*} path
 * @param {*} [user=MOCK_APP_USER]
 * @returns
 */
export function renderWithTopReduxRouter(ui, path, user = MOCK_APP_USER) {
  return {
    ...renderWithRedux(
      <Router history={createBrowserHistory(createMemorySource(path) as any)}>
        <ProtectedPage path="/" user={user} component={ui} />
        <Route path="/login/" component={LoginContainer} />
        <Route component={LoginContainer} />
      </Router>,
      { initialState: MOCKED_LOGGED_USER }
    ),
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    history
  };
}

export function getTestIdWithReduxRouter(component: JSX.Element, route: string, id: string): HTMLElement {
  const { getByTestId } = renderWithReduxRouter(component, { route });
  return getByTestId(id);
}

type testKeys = [string, string];
//react testing library
export function testIdGetter<T extends HTMLElement>(
  keys: testKeys,
  fn: Function
): {
  [prop: string]: T;
} {
  const [objKey, testId] = keys;
  return {
    [objKey]: fn(testId)
  };
}

export function reportError() {
  // in a real app this would report an error
  // but this isn't a real app and is only here to be mocked.
  return Promise.resolve({ success: true });
}
