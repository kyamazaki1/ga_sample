import * as React from "react";
import Login from "./Login.container";
import { render, fireEvent, cleanup } from "@testing-library/react";
import { LoginForm } from "@login/components/LoginForm/LoginForm.component";
import { testIdGetter } from "@mock/utils.mock";
//import { API } from "@shared/const";
import { renderWithRedux } from "@mock/utils.mock";

describe("Access to app", () => {
  afterEach(cleanup);
  test("It should display form upon opening", () => {
    const { getByTestId, store } = renderWithRedux(<Login />);
    const form = getByTestId("login-form");
    expect(form).toBeDefined();
  });
  test("It should prevent submit", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const handleFormVal = jest.fn(() => "invalid");
    const { getByTestId, getByText } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
        formTouched={false}
        formValidation={handleFormVal}
      />
    );
    const MOCK_INPUT: string = "";
    const MOCK_PWD: string = "re";

    const { loginInput } = testIdGetter<HTMLInputElement>(["loginInput", "login-input"], getByTestId);
    fireEvent.change(loginInput, { target: { value: MOCK_INPUT } });
    expect(loginInput.value).toBe(MOCK_INPUT);

    const { pwdInput } = testIdGetter<HTMLInputElement>(["pwdInput", "pwd-input"], getByTestId);
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);

    const { loginSubmit } = testIdGetter<HTMLButtonElement>(["loginSubmit", "login-submit"], getByTestId);
    expect(loginSubmit.disabled).toEqual(true);

    loginSubmit.click();

    //act
    expect(handleTryAuth).toHaveBeenCalledTimes(0);
  });
});
