import * as React from "react";
import { useState } from "react";
//style
//components
import { LoginForm } from "@login/components/LoginForm/LoginForm.component";
//hooks
import { useTryAuth, useValidationError } from "@login/hooks/useTryLogin";
//Model
import { AuthPayload, UseAuthPayload, FormState } from "@model/index";
import { Grid, Theme, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as bg from "@src/assets/img/bg.png";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flex: 1
  },
  images: {
    backgroundColor: theme.palette.primary.main,
    height: "100%",
    width: "100%",
    background: `url(${bg}) no-repeat center`
  },
  form: {
    textAlign: "center",
    justifyContent: "center",
    display: "flex",
    flex: 1,
    flexDirection: "column",
    padding: `0 ${theme.spacing(2)}px`
  }
}));

interface LoginProps {}

function Login(props: LoginProps): JSX.Element {
  const classes = useStyles(props);
  //user try auth map
  const [tryAuthState, setTryAuthState]: UseAuthPayload = useState<AuthPayload>(
    null
  );
  const [tryAuth] = useTryAuth(tryAuthState);
  //reset validation error  signin type
  const [formTouched, setFormTouched] = useValidationError();

  const handleResetValidationError: () => void = () => setFormTouched(false);
  const handleTryAuth: (f: AuthPayload) => void = form => {
    setTryAuthState(form), setFormTouched(true);
  };

  const [useFormVal, setFormVal]: [FormState, Function] = useState<FormState>(
    tryAuth.state
  );

  //btn state from form vali and api response
  const handleFormValidation = (valid: FormState) => {
    setFormVal(valid);
  };
  React.useEffect(() => {
    setFormVal(tryAuth.state);
  }, [tryAuth.state]);
  const [useSunburst, setUseSunburst] = React.useState<any>(null);

  return (
    <Grid container className={classes.root}>
      <Grid item md={8} sm={6}>
        <div className={classes.images}></div>
      </Grid>
      <Grid item md={4} sm={6} xs={12} className={classes.form}>
        <Typography variant="h1"> Dashboard Poc</Typography>

        <LoginForm
          id="login-form"
          loginType="signin"
          data-testid="login-form"
          tryAuth={handleTryAuth}
          resetValidationError={handleResetValidationError}
          formTouched={formTouched}
          validState={tryAuth.isSuccess ? true : false}
          btnState={useFormVal}
          formValidation={handleFormValidation}
        />
      </Grid>
    </Grid>
  );
}

export default Login;
