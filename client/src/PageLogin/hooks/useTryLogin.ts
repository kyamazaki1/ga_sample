import { FormState } from "@src/model";
import { AjaxResponse } from "rxjs/ajax";
import { useState, useEffect } from "react";
import { UseBoolean } from "@model/index";
import { tap, catchError, map, filter } from "rxjs/operators";
import { UserResponse } from "@model/user";
import { setAppUserAction } from "@home/store/";
import { store } from "../../index";
import { of } from "rxjs";
import { history } from "@src/route-history";
import { PROTECTED_DEFAULT_PATH } from "@src/routes";
import { tryUserAuth } from "@store/store.utils";
interface AuthPayload {
  email: string;
  password: string;
}
export interface AuthState {
  isSuccess: boolean;
  state: FormState;
}
/**
 *
 *
 * @export
 * @param {AuthPayload} payload
 * @returns {[AuthState]}
 */
export function useTryAuth(payload: AuthPayload): [AuthState] {
  const [tryAuth, setTryAuth] = useState({ isSuccess: null, state: "invalid" });
  useEffect(() => {
    if (!!payload) {
      setTryAuth({ isSuccess: false, state: "pending" });
      tryUserAuth(payload)
        .pipe(
          catchError((err: AjaxResponse) => {
            setTryAuth({ isSuccess: false, state: "error" });
            return of(null);
          }),
          filter(re => !!re),
          map((data: UserResponse) => {
            setTryAuth({ isSuccess: true, state: "success" });
            return store.dispatch(setAppUserAction(data));
          }),
          tap(_ => {
            history.push(PROTECTED_DEFAULT_PATH);
          })
        )
        .subscribe();
    }
  }, [payload]);
  return [tryAuth as AuthState];
}
export function useValidationError(touchState: boolean = false): UseBoolean {
  const [formTouched, setFormTouched]: UseBoolean = useState<boolean>(
    touchState
  );
  useEffect(() => {
    setFormTouched(touchState);
  }, [touchState]);
  return [formTouched, setFormTouched];
}
