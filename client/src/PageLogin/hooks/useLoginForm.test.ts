import { validate } from "@shared/utils/form";
import {
  MOCK_CONFIRM_INPUT_STATE,
  BASE_FORM_STATE,
  MOCK_INPUT_REF_STATE,
  MOCK_LOGIN_INPUT_STATE,
  MOCK_PWD_INPUT_STATE
} from "../../fixtures/index";
import { renderHook } from "@testing-library/react-hooks";
import { useLoginForm } from "./useLoginForm";
import { act } from "@testing-library/react";

test("should return signup form state on is signup", () => {
  const initialValue = false;

  const expected = {
    ...BASE_FORM_STATE,
    validate,
    isFormValid: false,
    confirmInput: MOCK_CONFIRM_INPUT_STATE,
    inputsRef: {
      ...BASE_FORM_STATE.inputsRef,
      confirmInput: {
        ...MOCK_INPUT_REF_STATE,
        state: MOCK_CONFIRM_INPUT_STATE
      }
    }
  };
  const { result, rerender } = renderHook(() => useLoginForm(initialValue));
  //https://github.com/facebook/jest/issues/5998
  //parse object to workaround error "Compared values have no visual difference but may have different properties."
  expect(JSON.parse(JSON.stringify(result.current))).toMatchObject(JSON.parse(JSON.stringify(expected)));
});

test("should return signin form state on is not signup", () => {
  const expected = {
    ...BASE_FORM_STATE,
    loginInput: {
      ...MOCK_LOGIN_INPUT_STATE,
      valid: true
    },
    pwdInput: {
      ...MOCK_PWD_INPUT_STATE,
      valid: true
    },

    confirmInput: {
      ...MOCK_CONFIRM_INPUT_STATE,
      valid: true
    },
    inputsRef: {
      ...BASE_FORM_STATE.inputsRef,
      loginInput: {
        ...MOCK_INPUT_REF_STATE,
        validations: ["required", "isEmail"],
        state: {
          ...MOCK_LOGIN_INPUT_STATE,
          valid: true
        }
      },
      pwdInput: {
        ...MOCK_INPUT_REF_STATE,
        validations: ["required", "minLength"],
        state: {
          ...MOCK_PWD_INPUT_STATE,
          valid: true
        }
      },
      confirmInput: {
        ...MOCK_INPUT_REF_STATE,
        state: {
          ...MOCK_CONFIRM_INPUT_STATE,
          valid: true
        }
      }
    }
  };
  const { result, rerender } = renderHook(() => useLoginForm(false));

  let updated;

  act(() => {
    updated = renderHook(() => useLoginForm(true));
    // console.log(updated.result.current.loginInput);
    updated.result.current.loginInput.valid = true;
    updated.result.current.pwdInput.valid = true;
    updated.result.current.confirmInput.valid = true;
  });
  rerender();
  //TODO if login input && pw inputs are valid
  //going to signup ->form invalid
  //going to signin ->form valid

  //https://github.com/facebook/jest/issues/5998
  //parse object to workaround error "Compared values have no visual difference but may have different properties."
  expect(JSON.parse(JSON.stringify(result.current))).toMatchObject(JSON.parse(JSON.stringify(expected)));
});
