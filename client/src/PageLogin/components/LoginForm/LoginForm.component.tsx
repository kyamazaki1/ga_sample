import * as React from "react";
import { VerticalForm, FormHint } from "@ui/form";
import { FormUpdatePayload, FormState } from "@model/index";
import { useLoginForm } from "@login/hooks/useLoginForm";
import { Columns } from "@ui/layout";
import * as PropTypes from "prop-types";
import { uiValidation, displayErrorHint, formsCtrlsToPayload } from "@shared/utils/form";
import { FormSubmitBtn } from "../../../components/FormSubmitBtn/FormSubmitBtn";
import { UseCbOnEffect } from "@src/hooks/useCbOnEffect";
import { FormGrp } from "../../../components/FormGrp/FormGrp";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    height: "300px",
    minWidth: "300px",
    padding: "10px 30px",
    display: "flex",
    flexDirection: "column"
  },

  btn_wrap: {
    width: "70%",
    margin: "0 auto"
  }
}));

interface LoginFormProps {
  loginType: string;
  tryAuth: Function;
  resetValidationError: Function;
  validState: boolean;
  id: string;
  formTouched: boolean;
  btnState: FormState;
  formValidation: Function;
}

function loginForm(props: LoginFormProps): JSX.Element {
  const classes = useStyles(props);

  const isSignup: boolean = props.loginType === "signup";
  const { loginInput, pwdInput, isFormValid, inputsRef, updateFormState } = useLoginForm(isSignup);
  const onHandleUpdate = (f: FormUpdatePayload): void => {
    updateFormState(f, inputsRef);
    props.resetValidationError();
  };

  UseCbOnEffect<boolean>(isFormValid, props.formValidation, isFormValid ? "valid" : "invalid");
  return (
    <div className={classes.root}>
      <VerticalForm
        data-testid="login-form"
        name="sigin"
        onSubmit={event => {
          event.preventDefault();
          const payload = formsCtrlsToPayload(inputsRef);
          props.tryAuth(payload);
        }}
      >
        <section>
          <FormGrp<string>
            spec={{
              id: "login-input",
              detail: { name: "loginInput", type: "email", placeholder: "login" },
              type: "email",
              hintTestId: "input-error"
            }}
            handleUpdate={onHandleUpdate}
            isValid={uiValidation(inputsRef.loginInput)}
            hint={displayErrorHint(inputsRef.loginInput)}
            value={loginInput.value}
          />
          <FormGrp<string>
            spec={{
              id: "pwd-input",
              detail: { name: "pwdInput", type: "password", placeholder: "password" },
              type: "password",
              hintTestId: "pwd-error"
            }}
            handleUpdate={onHandleUpdate}
            isValid={uiValidation(inputsRef.pwdInput)}
            hint={displayErrorHint(inputsRef.pwdInput)}
            value={pwdInput.value}
          />
        </section>

        <div className={classes.btn_wrap}>
          <FormSubmitBtn state={props.btnState} isInvalid={!isFormValid} />
          <FormHint
            id="form-hint"
            data-testid="message"
            className={props.formTouched && !props.validState ? "show" : "hide"}
          >
            {props.validState ? null : "login failed"}
          </FormHint>
        </div>
      </VerticalForm>
    </div>
  );
}
loginForm.propTypes = {
  tryAuth: PropTypes.func,
  resetValidationError: PropTypes.func,
  formValidation: PropTypes.func
};

loginForm.defaultProps = {
  id: "login-form",
  formTouched: false,
  btnState: "initial"
};

export { loginForm as LoginForm };
