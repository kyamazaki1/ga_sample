import * as React from "react";
import { render, fireEvent, cleanup, waitForElement } from "@testing-library/react";
import { LoginForm } from "@login/components/LoginForm/LoginForm.component";
import { testIdGetter } from "@mock/utils.mock";
import { ERROR_MSG } from "@shared/utils/form";
import { renderWithReduxRouter } from "@src/mock/utils.mock";
import App from "@home/containers/App.container";

describe("Fill form's input", () => {
  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);

  test("It should display login error", async () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const handleFormVal = jest.fn();

    const { getByTestId } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
        formTouched={false}
        btnState="invalid"
        formValidation={handleFormVal}
      />
    );

    const { loginInput } = await waitForElement(() =>
      testIdGetter<HTMLInputElement>(["loginInput", "login-input"], getByTestId)
    );

    fireEvent.change(loginInput, { target: { value: "test" } });
    fireEvent.change(loginInput, { target: { value: null } });

    const { inputError } = testIdGetter<HTMLInputElement>(["inputError", "input-error"], getByTestId);
    expect(inputError.textContent).toBe(ERROR_MSG.isEmail);

    const { loginSubmit } = testIdGetter<HTMLInputElement>(["loginSubmit", "login-submit"], getByTestId);
    expect(loginSubmit.disabled).toBe(true);
  });

  test("It should display password error", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const handleFormVal = jest.fn();

    const { getByTestId, rerender } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
        formTouched={false}
        btnState="invalid"
        formValidation={handleFormVal}
      />
    );
    const MOCK_PWD: string = "re";
    const { pwdInput } = testIdGetter<HTMLInputElement>(["pwdInput", "pwd-input"], getByTestId);
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);
    const { pwdError } = testIdGetter<HTMLInputElement>(["pwdError", "pwd-error"], getByTestId);
    expect(pwdError.textContent).toBe(ERROR_MSG.minLength);
    const { loginSubmit } = testIdGetter<HTMLInputElement>(["loginSubmit", "login-submit"], getByTestId);
    expect(loginSubmit.disabled).toBe(true);
  });
  test("It should match input", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const handleFormVal = jest.fn();

    const { getByTestId } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
        formTouched={false}
        btnState="valid"
        formValidation={handleFormVal}
      />
    );
    const LOGIN_INPUT: string = "toto";
    const { loginInput } = testIdGetter<HTMLInputElement>(["loginInput", "login-input"], getByTestId);
    fireEvent.change(loginInput, { target: { value: LOGIN_INPUT } });
    expect(loginInput.value).toBe(LOGIN_INPUT);

    const MOCK_PWD: string = "rerere";
    const { pwdInput } = testIdGetter<HTMLInputElement>(["pwdInput", "pwd-input"], getByTestId);
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);

    const { loginSubmit } = testIdGetter<HTMLInputElement>(["loginSubmit", "login-submit"], getByTestId);
    expect(loginSubmit.disabled).toBe(true);
  });
  test("It should disable submit button on invalid inputs", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const handleFormVal = jest.fn();

    const { getByTestId } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
        formTouched={false}
        btnState="valid"
        formValidation={handleFormVal}
      />
    );
    const LOGIN_INPUT: string = "mock";
    const { loginInput } = testIdGetter<HTMLInputElement>(["loginInput", "login-input"], getByTestId);
    fireEvent.change(loginInput, { target: { value: LOGIN_INPUT } });
    expect(loginInput.value).toBe(LOGIN_INPUT);

    const MOCK_PWD: string = "ree";
    const { pwdInput } = testIdGetter<HTMLInputElement>(["pwdInput", "pwd-input"], getByTestId);
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);
    const { loginSubmit } = testIdGetter<HTMLInputElement>(["loginSubmit", "login-submit"], getByTestId);
    expect(loginSubmit.disabled).toBe(true);
  });
  test("It display error hint on invalid credentials", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const handleFormVal = jest.fn();

    const { getByTestId, rerender, getByText } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
        formTouched={false}
        btnState="valid"
        formValidation={handleFormVal}
      />
    );
    const LOGIN_INPUT: string = "mock@mock.fr";
    const { loginInput } = testIdGetter<HTMLInputElement>(["loginInput", "login-input"], getByTestId);
    fireEvent.change(loginInput, { target: { value: LOGIN_INPUT } });
    expect(loginInput.value).toBe(LOGIN_INPUT);

    const MOCK_PWD: string = "rererere";
    const { pwdInput } = testIdGetter<HTMLInputElement>(["pwdInput", "pwd-input"], getByTestId);
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);
    const { loginSubmit } = testIdGetter<HTMLInputElement>(["loginSubmit", "login-submit"], getByTestId);

    fireEvent.click(loginSubmit);

    rerender(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
        formTouched={true}
        btnState="invalid"
        formValidation={handleFormVal}
      />
    );

    const hintError = getByText(/login failed/);

    expect(hintError).toBeTruthy();
  });
  test("It display error hint on invalid credentials", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const handleFormVal = jest.fn();

    const { getByTestId, rerender } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
        formTouched={false}
        btnState="valid"
        formValidation={handleFormVal}
      />
    );
    const LOGIN_INPUT: string = "mock@mock.fr";
    const { loginInput } = testIdGetter<HTMLInputElement>(["loginInput", "login-input"], getByTestId);
    fireEvent.change(loginInput, { target: { value: LOGIN_INPUT } });
    expect(loginInput.value).toBe(LOGIN_INPUT);

    const MOCK_PWD: string = "rererere";
    const { pwdInput } = testIdGetter<HTMLInputElement>(["pwdInput", "pwd-input"], getByTestId);
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);
    const { loginSubmit } = testIdGetter<HTMLInputElement>(["loginSubmit", "login-submit"], getByTestId);

    fireEvent.click(loginSubmit);
    const route = "/";
    const { getByText } = renderWithReduxRouter(<App />, { route });

    expect(getByText(/app Page/i)).toBeTruthy();
  });
});
