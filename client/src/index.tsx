import * as React from "react";
import * as ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
//css library
import "./styles.css";
//style
//navigation
import { Route, Switch, Router } from "react-router-dom";
import { history } from "./route-history";
//routes
import {
  LoginContainer,
  DashboardContainer,
  RecommendationsContainer,
  CampaignsContainer
} from "./routes";
//redux observable
import configureStore from "@store/config.store";
import ProtectedPage from "./containers/ProtectedPage/ProtectedPage";
import { Provider } from "react-redux";

//style

import { ThemeProvider, withTheme } from "@material-ui/styles";
import { PersistGate } from "redux-persist/lib/integration/react";
import { safeGet } from "@shared/utils";
import { UserResponse } from "@model/user";
import { useLatestValue } from "@hooks/useLatestValue";
import { Loading } from "./components/Loading/Loading.component";
import { APP_THEME } from "./containers/PageTheme/PageTheme.container";

import { createGlobalStyle } from "styled-components";
import { Redirect } from "react-router";

/** constants  */
export const GlobalStyle = createGlobalStyle`
html{
  height:100vh;
  overflow-y: hidden;
}
#root, body{
  height:inherit;
}
#root{
  display:flex;
  flex-direction:column;
}`;

export const { store, persistor } = configureStore();

function AppWrap(): JSX.Element {
  const user: UserResponse = safeGet(store.getState(), "app", "user");
  const [useUser] = useLatestValue(user, null);

  return (
    <Provider store={store}>
      <ThemeProvider theme={APP_THEME}>
        <PersistGate loading={null} persistor={persistor}>
          <Router history={history}>
            <React.Suspense fallback={<Loading />}>
              <GlobalStyle />
              <Switch>
                <ProtectedPage
                  path="/overview"
                  exact
                  user={useUser}
                  component={DashboardContainer}
                />
                <ProtectedPage
                  path="/recommendations"
                  exact
                  user={useUser}
                  component={RecommendationsContainer}
                />

                <ProtectedPage
                  path="/campaigns"
                  exact
                  user={useUser}
                  component={CampaignsContainer}
                />
                <Route path="/login" component={LoginContainer} />

                <Redirect to="/overview" />
              </Switch>
            </React.Suspense>
          </Router>
        </PersistGate>
      </ThemeProvider>
    </Provider>
  );
}

ReactDOM.render(
  <AppWrap />,
  (document.getElementById("root") as HTMLElement) ||
    document.createElement("div")
);
registerServiceWorker();
