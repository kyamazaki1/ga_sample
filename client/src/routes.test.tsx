import * as React from "react";
import Login from "@login/containers/Login.container";
import { renderWithRedux, renderWithTopReduxRouter } from "./mock/utils.mock";
import { cleanup, wait } from "@testing-library/react";
import { DashboardContainer, LoginContainer } from "@src/routes";
import { INIT_APP_USER } from "@src/fixtures";

describe("Lodable components", () => {
  afterEach(cleanup);
  it("renders login logo", async () => {
    const { getByTestId } = renderWithRedux(<Login />);
    const form = getByTestId("login-form");
    expect(form).toBeDefined();
  });

  test("It should renders login page", () => {
    const { getAllByTestId } = renderWithTopReduxRouter(
      LoginContainer,
      "/login",
      INIT_APP_USER
    );
    expect(getAllByTestId("login-input")).toBeTruthy();
  });

  test("It should renders App page", async () => {
    const { queryAllByText } = renderWithTopReduxRouter(
      DashboardContainer,
      "/dashboard"
    );
    expect(queryAllByText(/internal tools/i)).toBeTruthy();
  });
});
