import * as React from "react";
import { Suspense, lazy } from "react";

//PATH

export const PUBLIC_PATH: string = "/login/";
export const PROTECTED_DEFAULT_PATH: string = "/overview/";

export const LoginContainer = lazy(() =>
  import("@login/containers/Login.container")
);
export const DashboardContainer = lazy(() =>
  import("@dashboard/containers/Dashboard.container")
);

export const RecommendationsContainer = lazy(() =>
  import("@src/PageRecommendations/containers/Recommendations.container")
);

export const CampaignsContainer = lazy(() =>
  import("@src/PageCampaigns/containers/Campaigns.container")
);
