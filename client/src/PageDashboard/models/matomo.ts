//https://developer.matomo.org/api-reference/reporting-api
export type RANGE_TYPE = "day" | "week" | "month" | "year" | "range";
export type DATE = "today" | "yesterday" | string; //RANGE_VALUE= '2011-01-01,2011-02-15'
type API_METHOD = "API.get";

export interface API_BASE {
  method: API_METHOD;
}

export interface API_DATE extends API_BASE {
  method: "API.get";
  period: RANGE_TYPE;
  date: string;
}
