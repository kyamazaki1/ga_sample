import { DispatchAction } from "@model/index";
import { mergeMap, map, switchMap, tap, concatMap } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { Observable, concat } from "rxjs";
import { StateObservable, ofType } from "redux-observable";
import { reqDispatchAction, autoRefreshGet } from "@src/store/store.utils";
import { dashboardUrl } from "../shared/utils";
import { Action } from "redux";
import { StoreState } from "@src/model";
import { TRY_SAMPLE } from "./dashboard.actionTypes";
import { setSample } from "./dashboard.actions";
/* 
export const revenueEpic$ = (
  action$: Observable<Action>,
  state$: StateObservable<StoreState>,
  client = ajax
): Observable<any> =>
  action$.pipe(
    ofType(TRY_REVENUE),
    mergeMap((action: DispatchAction<string>) => {
      return concat(
        autoRefreshGet(state$, dashboardUrl(action.payload), setRevenue, client),
        autoRefreshGet(state$, dashboardUrl(action.payload), setRevenueRange, client)
      );
    })
  ); */

export const fetchSample$ = (
  action$: Observable<Action>,
  state$: StateObservable<StoreState>,
  client = ajax
): Observable<any> =>
  action$.pipe(
    ofType(TRY_SAMPLE),
    mergeMap((action: DispatchAction<string>) => {
      return autoRefreshGet(state$, dashboardUrl("/all/"), setSample, client);
    })
  );
