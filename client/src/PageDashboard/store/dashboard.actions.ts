import { TRY_SAMPLE, SET_SAMPLE } from "./dashboard.actionTypes";
//

export const trySample = () => ({
  type: TRY_SAMPLE,
  payload: null
});
export const setSample = (payload: Array<any>) => ({
  type: SET_SAMPLE,
  payload
});
