import { SET_SAMPLE } from "./dashboard.actionTypes";
import { DispatchAction } from "./../../model/store";
import produce from "immer";
export const initialAppState = {
  sample: []
};

function dashboardReducer(
  state = initialAppState,
  action: DispatchAction<any>
): any {
  return produce(state, draftState => {
    const { payload } = action;
    switch (action.type) {
      case SET_SAMPLE: {
        draftState.sample = payload.data;
        return;
      }
    }
  });
}

export { dashboardReducer as DashboardReducer };
