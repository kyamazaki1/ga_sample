import { argsToString } from "./utils";

describe("sync store utils", () => {
  afterEach(() => jest.clearAllMocks());
  test("stringify object key value into url args format", () => {
    const MOCK_ARG = {
      key1: "mock1",
      key2: "mock2"
    };
    const expected = "&key1=mock1&key2=mock2";
    expect(argsToString(MOCK_ARG)).toBe(expected);
  });
});
