export const API_ROOT: string = process.env.REACT_APP_API_URI;

export const DASHBOARD_ROOT: string = `${API_ROOT}sample`;
