import { DASHBOARD_ROOT } from "./const";

export function argsToString(obj: Object): string {
  return Object.keys(obj).reduce(
    (acc: string, curr: string) => acc.concat(`&${curr}=${obj[curr]}`),
    ""
  );
}
//`${API_ROOT}data-viz/`
export function dashboardUrl(
  args: string,
  api: string = DASHBOARD_ROOT
): string {
  return `${api}${args}`;
}
