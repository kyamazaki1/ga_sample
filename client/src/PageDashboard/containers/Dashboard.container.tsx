import { DateFilter } from "@dashboard/components/DateFilter/DateFilter.component";
import { ListFilter } from "@dashboard/components/ListFilter/ListFilter.component";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";
import { DataContent } from "../components/DataContent/DataContent.component";
import { useDispatch, useSelector } from "react-redux";
import { trySample } from "@src/PageDashboard/store";
import { StoreState } from "@src/model";

interface DashboardContainerProps {
  dashboard?: any;
  trySample?: Function;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "grid",
      gridTemplateColumns: "145px auto"
    },
    main: {
      height: "100vh",
      overflow: "auto"
    }
  })
);

let Dashboard = function(props: DashboardContainerProps): JSX.Element {
  const classes = useStyles(props);

  const dashboard = useSelector((state: StoreState) => {
    return state.dashboard;
  });
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(trySample());
  }, []);

  return (
    <React.Fragment>
      <DateFilter />
      <ListFilter />
      <DataContent />
    </React.Fragment>
  );
};

export default Dashboard;
