import * as React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";

import Fab from "@material-ui/core/Fab";
import Typography from "@material-ui/core/Typography";
import Popover from "@material-ui/core/Popover";

import { FilterPopover } from "../FilterPopover/FilterPopover.component";
import { FilterAction } from "../ListFilter/ListFilter.component";
import Button from "@material-ui/core/Button";
const FILTER_ACTIONS = [
  {
    name: "All",
    action: null
  },
  {
    name: "All enabled",
    action: null
  },
  {
    name: "All but removed",
    action: null
  }
];

interface FilterBtnProps {
  type: "campaing" | "ad_group";
  title: string;
  currentFilter: {
    type: string;
    value: string;
  };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: "0 10px"
    }
  })
);

function filterBtn(props: FilterAction): JSX.Element {
  const classes = useStyles(props);
  // TYPE in action dispatch ?
  const { type, currentFilter, title } = props;
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? type : undefined;

  //date range in v4 release 01/2020
  //https://github.com/mui-org/material-ui-pickers/issues/364
  return (
    <div className={classes.root}>
      <Fab
        variant="extended"
        size="small"
        color="secondary"
        aria-describedby={id}
        onClick={handleClick}
      >
        <Typography variant="caption" color="primary">
          {type} {currentFilter.type}: <span> {currentFilter.value}</span>
        </Typography>
      </Fab>

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
      >
        <FilterPopover
          title={title}
          actions={FILTER_ACTIONS}
          onClose={handleClose}
        />
      </Popover>
    </div>
  );
}

export { filterBtn as FilterBtn };
