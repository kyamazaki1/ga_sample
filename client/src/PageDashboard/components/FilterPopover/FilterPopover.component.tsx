import * as React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import IconButton from "@material-ui/core/IconButton";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

interface PopoverAction {
  name: string;
  action: any;
}
interface FilterPopoverProps {
  title: string;
  actions: Array<PopoverAction>;
  onClose: Function;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card__title: {
      fontSize: 15
    },
    card__header: {
      backgroundColor: theme.palette.secondary.main,
      padding: theme.spacing(1)
    }
  })
);

function filterPopover(props: FilterPopoverProps): JSX.Element {
  const classes = useStyles(props);
  const { title, actions, onClose } = props;
  //date range in v4 release 01/2020
  //https://github.com/mui-org/material-ui-pickers/issues/364
  return (
    <Card>
      <CardHeader
        action={
          <IconButton aria-label="settings" onClick={() => onClose()}>
            <HighlightOffIcon />
          </IconButton>
        }
        title={title}
        classes={{
          root: classes.card__header,
          title: classes.card__title
        }}
      />

      <CardContent>
        <List dense aria-label="secondary mailbox folders">
          {actions.map(a => (
            <ListItem button dense key={a.name}>
              <ListItemText primary={a.name} color="primary" />
            </ListItem>
          ))}
        </List>
      </CardContent>
    </Card>
  );
}

export { filterPopover as FilterPopover };
