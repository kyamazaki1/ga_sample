import * as React from "react";
import { useState } from "react";

import IconButton from "@material-ui/core/IconButton";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import FilterListIcon from "@material-ui/icons/FilterList";
import { AddFilterBtn } from "../AddFilterBtn/AddFilterBtn.component";
import { FilterBtn } from "../FilterBtn/FilterBtn.component";

export interface FilterAction {
  type: "campaign" | "ad_group";
  title: string;
  currentFilter: {
    type: string;
    value: string;
  };
}
const FILTERS: Array<FilterAction> = [
  {
    type: "campaign",
    currentFilter: {
      type: "status",
      value: "all"
    },
    title: "Campaign status"
  },
  {
    type: "ad_group",
    currentFilter: {
      type: "status",
      value: "all"
    },
    title: "Ad group status"
  }
];

interface ListFilterProps {
  filters: Array<FilterAction>;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {},
    filters: {
      borderBottom: `1px solid ${theme.palette.divider}`
    }
  })
);

function listFilter(props: ListFilterProps): JSX.Element {
  const { filters } = props;
  const classes = useStyles(props);
  const [useOpenFilter, setUseOpenFilter] = useState<string>(null);

  function openFiler(e) {
    e.preventDefault();
    setUseOpenFilter("initial");
  }
  function onClosePopover() {
    setUseOpenFilter(null);
  }
  //date range in v4 release 01/2020
  //https://github.com/mui-org/material-ui-pickers/issues/364
  return (
    <Toolbar className={classes.filters}>
      <IconButton aria-label="delete" color="primary" onClick={openFiler}>
        <FilterListIcon />
      </IconButton>
      {filters.map(f => (
        <FilterBtn
          key={f.type}
          type={f.type as any}
          currentFilter={f.currentFilter}
          title={f.title}
        />
      ))}
      <AddFilterBtn openFilter={useOpenFilter} closePopover={onClosePopover} />
    </Toolbar>
  );
}
listFilter.defaultProps = {
  filters: FILTERS
};
export { listFilter as ListFilter };
