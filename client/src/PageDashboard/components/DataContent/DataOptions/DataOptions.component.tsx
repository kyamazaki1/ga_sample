import IconButton from "@material-ui/core/IconButton";
import Popover from "@material-ui/core/Popover";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";

import * as React from "react";

const OPTIONS = [
  {
    value: "hourly"
  },
  {
    value: "daily"
  },
  {
    value: "weekly"
  },
  {
    value: "monthly"
  },
  {
    value: "quaterly"
  }
];

interface DataOptionsProps {}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    option: {
      position: "absolute",
      right: 10,
      top: 10
    },
    list: {
      minWidth: 300
    }
  })
);

function dataOptions(props: DataOptionsProps): JSX.Element {
  const classes = useStyles(props);

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const [selectedIndex, setSelectedIndex] = React.useState(1);

  const handleListItemClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    index: number
  ) => {
    setSelectedIndex(index);
  };
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <React.Fragment>
      <IconButton
        onClick={handleClick}
        className={classes.option}
        color="primary"
        aria-label="show more card options"
      >
        <MoreVertIcon />
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
      >
        <List
          subheader={<ListSubheader>Card options</ListSubheader>}
          aria-label="data options"
          className={classes.list}
          dense
        >
          {OPTIONS.map(o => (
            <ListItem
              dense
              button
              key={o.value}
              selected={selectedIndex === 0}
              onClick={event => handleListItemClick(event, 0)}
            >
              <ListItemText primary={o.value} />
            </ListItem>
          ))}
        </List>
      </Popover>
    </React.Fragment>
  );
}

export { dataOptions as DataOptions };
