import FormControl from "@material-ui/core/FormControl";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";
import { Typography } from "@material-ui/core";

interface DataSelectProps {
  filter_value: string;
  filter_label: string;
  filters: Array<{
    value: string;
    viewValue: string;
  }>;
  type: string;
  className: string;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    select: {
      position: "relative",
      zIndex: 3,
      width: "100%"
    },

    list__item: {
      borderBottom: `1px solid #5C5C5C`,
      borderRight: `1px solid #5C5C5C`
    },
    item__content: {
      minWidth: 90
    },
    list__txt: {
      paddingTop: 10
    },
    item_0: {
      backgroundColor: "#2666D2"
    },
    item_1: {
      backgroundColor: "#C53929"
    },
    item_2: {
      backgroundColor: "#F29900"
    },
    item_3: {
      backgroundColor: "#188038"
    },
    icon: {
      opacity: 0
    }
  })
);

function dataSelect(props: DataSelectProps): JSX.Element {
  const { filters, filter_value, filter_label, type, className } = props;
  const classes = useStyles(props);

  const [useLabel, setUseLabel] = React.useState("");
  const [useIsClicked, setUseIsClicked] = React.useState(false);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    event.stopPropagation();
    event.preventDefault();

    setUseLabel(event.target.value as any);
  };
  function click() {
    event.stopPropagation();
    event.preventDefault();
    setUseIsClicked(!useIsClicked);
  }
  return (
    <ListItem
      button
      onClick={click}
      disableRipple
      className={`${classes.list__item} ${
        !useIsClicked ? classes[className] : null
      }`}
      key={type}
    >
      <div>
        <FormControl className={classes.select} disabled={useIsClicked}>
          <Select
            labelId="demo-simple-select-label"
            id={type}
            value={useLabel || filter_label}
            onChange={e => handleChange(e)}
            classes={{
              icon: useIsClicked ? classes.icon : null
            }}
            disableUnderline={useIsClicked}
          >
            {filters.map(b => (
              <MenuItem key={b.value} value={b.value}>
                {b.viewValue}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <ListItemText primary={filter_value} className={classes.list__txt} />
      </div>
    </ListItem>
  );
}

dataSelect.defaultProps = {
  btn_selects: [
    {
      value: null,
      viewValue: null
    }
  ],
  btn_value: null
};
export { dataSelect as DataSelect };
