import List from "@material-ui/core/List";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";
import { DataSelect } from "../DataSelect/DataSelect.component";

interface DataListProps {
  list: Array<{
    filter_label: string;
    filter_value: string;
    type: string;
  }>;

  filters: Array<{
    value: string;
    viewValue: string;
  }>;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      display: "inline-flex",
      justifyContent: "space-around",
      width: "66%",
      padding: 0
    }
  })
);

function dataList(props: DataListProps): JSX.Element {
  const { list, filters } = props;
  const classes = useStyles(props);

  return (
    <List
      dense
      aria-label="apply metrics filtering"
      classes={{
        root: classes.list
      }}
    >
      {list.map((l, idx) => (
        <DataSelect
          filters={filters}
          filter_value={l.filter_value}
          filter_label={l.filter_label}
          type={l.type}
          key={l.type}
          className={`item_${idx}`}
        ></DataSelect>
      ))}
    </List>
  );
}

dataList.defaultProps = {
  list: [
    {
      filter_label: null,
      filter_value: null,
      type: null
    }
  ],

  filters: [
    {
      value: null,
      viewValue: null
    }
  ]
};
export { dataList as DataList };
