import * as React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";

import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";

import Typography from "@material-ui/core/Typography";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";

interface DateFilterProps {}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {},
    page__bar: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      padding: `4px ${theme.spacing(3)}px`
    }
  })
);

function dateFilter(props: DateFilterProps): JSX.Element {
  const classes = useStyles(props);

  // The first commit of Material-UI
  const [selectedDate, setSelectedDate] = React.useState<Date | null>(
    new Date("2014-08-18T21:11:54")
  );

  const handleDateChange = (date: Date | null) => {
    setSelectedDate(date);
  };
  //date range in v4 release 01/2020
  //https://github.com/mui-org/material-ui-pickers/issues/364
  return (
    <Paper className={classes.page__bar}>
      <Typography variant="h1" className={classes.title} color="primary">
        Overview
      </Typography>

      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Date picker inline"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            "aria-label": "change date"
          }}
        />
      </MuiPickersUtilsProvider>
    </Paper>
  );
}

export { dateFilter as DateFilter };
