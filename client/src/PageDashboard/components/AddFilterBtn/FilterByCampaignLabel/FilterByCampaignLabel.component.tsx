import * as React from "react";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { AdGroupInput } from "../FilterByPopover/AdGroupInput/AdGroupInput.component";
import { AdGroupCell } from "../FilterByPopover/AdGroupCell/AddGroupCell.component";
import { AdGroupCheckbox } from "../FilterByPopover/AdGroupCheckbox/AdGroupCheckbox.component";
import { AddGroupCellRow } from "../FilterByPopover/AdGroupCellRow/AddGroupCellRow.component";

const SELECTIONS = [
  {
    hasBorder: true,
    data: [{ value: "campaing 1" }]
  },
  {
    hasBorder: false,
    data: [{ value: "ad group 1" }, { value: "ad group  2" }]
  }
];
const RESULTS = [
  {
    hasBorder: false,
    data: [{ value: "no results" }]
  }
];
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    col: {
      width: "100%",
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row: {
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row__borderless: {}
  })
);

interface FilterByCampaignLabelProps {}
function filterByCampaignLabel(props: FilterByCampaignLabelProps): JSX.Element {
  const classes = useStyles(props);

  return (
    <React.Fragment>
      <main className={classes.col}>
        <div className={classes.row}>
          <AdGroupInput />
        </div>
        <div className={classes.row__borderless}>
          <AdGroupCell rows={RESULTS} name="campaign_label"></AdGroupCell>
        </div>
      </main>
    </React.Fragment>
  );
}

export { filterByCampaignLabel as FilterByCampaignLabel };
