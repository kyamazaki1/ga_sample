import * as React from "react";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { AdGroupCell } from "../FilterByPopover/AdGroupCell/AddGroupCell.component";
import { AdGroupCheckbox } from "../FilterByPopover/AdGroupCheckbox/AdGroupCheckbox.component";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    col: {
      width: "100%"
    },
    row: {
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row__borderless: {}
  })
);

interface FilterByDeviceTypeProps {}
function filterByDeviceType(props: FilterByDeviceTypeProps): JSX.Element {
  const classes = useStyles(props);

  return (
    <React.Fragment>
      <main className={classes.col}>
        <div className={classes.row__borderless}>
          <Typography variant="caption">Matches any: </Typography>
          <AdGroupCell
            rows={[]}
            name="Smart"
            header={
              <List className={classes.row} dense>
                <ListItem dense>
                  <AdGroupCheckbox label="Computers" />
                </ListItem>
                <ListItem dense>
                  <AdGroupCheckbox label="Mobile any" />
                </ListItem>
                <ListItem dense>
                  <AdGroupCheckbox label="Tablets" />
                </ListItem>
              </List>
            }
          ></AdGroupCell>
        </div>
      </main>
    </React.Fragment>
  );
}

export { filterByDeviceType as FilterByDeviceType };
