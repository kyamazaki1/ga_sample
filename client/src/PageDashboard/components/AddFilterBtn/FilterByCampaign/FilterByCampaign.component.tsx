import * as React from "react";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { AdGroupInput } from "../FilterByPopover/AdGroupInput/AdGroupInput.component";
import { AdGroupCell } from "../FilterByPopover/AdGroupCell/AddGroupCell.component";
import { AdGroupCheckbox } from "../FilterByPopover/AdGroupCheckbox/AdGroupCheckbox.component";
import { AddGroupCellRow } from "../FilterByPopover/AdGroupCellRow/AddGroupCellRow.component";

const RESULTS = [
  {
    hasBorder: false,
    data: [{ value: "no results" }]
  }
];
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    col: {
      width: "100%",
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row: {
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row__borderless: {},
    cells: {
      display: "grid",
      gridTemplateColumns: "50% 50%"
    },
    card_content: {
      display: "grid",
      gridTemplateColumns: "67% 33%"
    }
  })
);

interface FilterByCampaignProps {}
function filterByCampaign(props: FilterByCampaignProps): JSX.Element {
  const classes = useStyles(props);

  return (
    <section className={classes.card_content}>
      <main className={classes.col}>
        <div className={classes.row}>
          <AdGroupInput />
        </div>
        <div className={classes.row__borderless}>
          <AdGroupCell
            rows={RESULTS}
            name="campaign"
            header={
              <List className={classes.row} dense>
                <ListItem dense>
                  <AdGroupCheckbox label="no label" />
                </ListItem>
              </List>
            }
          ></AdGroupCell>
        </div>
      </main>

      <aside className={classes.col}>
        <AddGroupCellRow hasBorder data={[{ value: "none selected" }]} />
        <AddGroupCellRow data={[{ value: "" }]} />
      </aside>
    </section>
  );
}

export { filterByCampaign as FilterByCampaign };
