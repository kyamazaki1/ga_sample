import * as React from "react";
import { useState, useEffect } from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Popover from "@material-ui/core/Popover";
import { AddFilterCard } from "./AddFilterCard/AddFilterCard.component";
import { FilterByPopover } from "./FilterByPopover/FlterByPopover.component";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: "0 10px"
    }
  })
);

const mapFilterProps = {
  ad_group: {
    title: "Ad group",
    size: 700
  },
  campaign: {
    title: "Ad Campaign",
    size: 700
  },
  campaign_label: {
    title: "Campaign label"
  },
  campaign_type: {
    title: "Campaign type"
  },
  device_type: {
    title: "Device type"
  }
};

function setPopOverProps(type: string, mapProps = mapFilterProps) {
  return { ...mapProps[type], ...{ type } };
}

interface AddFilterBtnProps {
  openFilter: string;
  closePopover: Function;
}
function addFilterBtn(props: AddFilterBtnProps): JSX.Element {
  const classes = useStyles(props);
  // TYPE in action dispatch ?
  const { openFilter, closePopover } = props;
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const [usePopoverContent, setUsePopoverContent] = React.useState<string>(
    "initial"
  );

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  let btn: HTMLButtonElement = null;

  useEffect(() => {
    setAnchorEl(openFilter ? btn : null);
  }, [openFilter]);

  const handleClose = () => {
    closePopover(null); //reset IconButton state
    setUsePopoverContent("initial"); //reset popover content
    setAnchorEl(null); //remove popover
  };

  const open = Boolean(anchorEl);
  const id = open ? "add_filter" : undefined;

  function handleOnApplyFilter(e: { type: string }) {
    setUsePopoverContent(e.type);
  }

  function renderContent(type: string): JSX.Element {
    const isFilterType = mapFilterProps.hasOwnProperty(type);

    const comProps = setPopOverProps(type);
    return isFilterType ? (
      <FilterByPopover {...comProps} closeFilter={handleClose} />
    ) : (
      <AddFilterCard
        onApplyFilter={handleOnApplyFilter}
        closeFilter={handleClose}
      />
    );
  }

  return (
    <div className={classes.root}>
      <Button buttonRef={(ref: any) => (btn = ref)} onClick={handleClick}>
        Add filter
      </Button>

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
      >
        {renderContent(usePopoverContent)}
      </Popover>
    </div>
  );
}

export { addFilterBtn as AddFilterBtn };
