import * as React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import TextField from "@material-ui/core/TextField";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card__header: {
      padding: `0 10px`
    }
  })
);
const FILTERS = [
  {
    type: "ad_group"
  },
  {
    type: "campaign"
  },
  {
    type: "campaign_label"
  },
  {
    type: "campaign_type"
  },
  {
    type: "device_type"
  }
];

interface AddFilterCardProps {
  filters: Array<{ [prop: string]: string }>;
  onApplyFilter: Function;
  closeFilter: Function;
}
function addFilterCard(props: AddFilterCardProps): JSX.Element {
  const classes = useStyles(props);
  const { filters, onApplyFilter } = props;

  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const handleListItemClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    index: number
  ) => {
    // setSelectedIndex(index);
    onApplyFilter(filters[index]);
  };

  return (
    <Card>
      <CardHeader
        action={
          <form noValidate autoComplete="off">
            <TextField
              id="searchFilter"
              label="search"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                )
              }}
            />
          </form>
        }
        title=""
        classes={{
          root: classes.card__header
        }}
      />

      <CardContent>
        <List dense aria-label="secondary mailbox folders">
          {filters.map((a, idx) => (
            <ListItem
              button
              dense
              key={a.type}
              selected={selectedIndex === idx}
              onClick={event => handleListItemClick(event, idx)}
            >
              <ListItemText primary={a.type} color="primary" />
            </ListItem>
          ))}
        </List>
      </CardContent>
    </Card>
  );
}
addFilterCard.defaultProps = {
  filters: FILTERS
};

export { addFilterCard as AddFilterCard };
