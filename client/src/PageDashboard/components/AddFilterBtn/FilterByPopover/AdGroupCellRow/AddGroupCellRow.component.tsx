import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    row: {
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row__borderless: {},

    list__item: {}
  })
);

interface AddGroupCellRowProps {
  hasBorder?: boolean;
  data: Array<{ value: string }>;
}
function addGroupCellRow(props: AddGroupCellRowProps): JSX.Element {
  const classes = useStyles(props);
  const { hasBorder, data } = props;
  const applyClass = {
    true: "row",
    false: "row__borderless"
  };
  return (
    <List className={classes[applyClass[hasBorder.toString()]]} dense>
      {data.map(d => (
        <ListItem key={d.value} className={classes.list__item} dense>
          <ListItemText primary={d.value} />
        </ListItem>
      ))}
    </List>
  );
}
addGroupCellRow.defaultProps = {
  hasBorder: false
};
export { addGroupCellRow as AddGroupCellRow };
