import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import IconButton from "@material-ui/core/IconButton";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import * as React from "react";
import { FilterByAdGroup } from "../FilterByAdGroup/FilterByAdGroup.component";
import { FilterByCampaign } from "../FilterByCampaign/FilterByCampaign.component";
import { FilterByCampaignLabel } from "../FilterByCampaignLabel/FilterByCampaignLabel.component";
import { FilterByCampaignType } from "../FilterByCampaignType/FilterByCampaignType.component";
import { FilterByDeviceType } from "../FilterByDeviceType/FilterByDeviceType.component";

interface FilterByPopoverTheme extends Theme {
  size: number;
}
const useStyles = makeStyles((theme: FilterByPopoverTheme) =>
  createStyles({
    root: {
      minWidth: (theme: FilterByPopoverTheme) => theme.size
    },
    card__title: {
      fontSize: 15
    },
    card__header: {
      backgroundColor: theme.palette.secondary.main,
      padding: theme.spacing(1)
    },
    card_content: {
      padding: 0
    },
    col: {
      width: "100%",
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row: {
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row__borderless: {},
    cells: {
      display: "grid",
      gridTemplateColumns: "50% 50%"
    },

    actions: {
      display: "flex",
      justifyContent: "flex-end",
      padding: 10
    }
  })
);

interface FilterByPopoverProps {
  title: string;
  closeFilter: Function;
  type: string;
  size: number;
}
function filterByPopover(props: FilterByPopoverProps): JSX.Element {
  const { closeFilter, title, type, size } = props;
  //merge theme and custom props
  const classes = useStyles({ size } as FilterByPopoverTheme);
  //TODO use filterbypopover to handle popover contentd
  // define in
  function renderCardContent(type: string) {
    switch (type) {
      case "ad_group": {
        return <FilterByAdGroup />;
      }

      case "campaign": {
        return <FilterByCampaign />;
      }

      case "campaign_label": {
        return <FilterByCampaignLabel />;
      }

      case "campaign_type": {
        return <FilterByCampaignType />;
      }

      case "device_type": {
        return <FilterByDeviceType />;
      }
      default:
        return null;
    }
  }

  return (
    <Card className={classes.root}>
      <CardHeader
        action={
          <IconButton
            aria-label="ad group"
            onClick={() => {
              closeFilter();
            }}
          >
            wkkkkkkkk
            <CloseIcon />
          </IconButton>
        }
        title={title}
        classes={{
          root: classes.card__header,
          title: classes.card__title
        }}
      />

      <CardContent className={classes.card_content}>
        {renderCardContent(type)}
      </CardContent>

      <CardActions classes={{ root: classes.actions }}>
        <Button size="small">Apply</Button>
      </CardActions>
    </Card>
  );
}

filterByPopover.defaultProps = {
  size: 300
};
export { filterByPopover as FilterByPopover };
