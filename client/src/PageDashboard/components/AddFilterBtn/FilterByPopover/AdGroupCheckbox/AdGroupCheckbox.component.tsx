import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    checkox: {
      padding: 2
    },
    label: {
      fontSize: "0.85rem",
      paddingLeft: 5
    }
  })
);

interface AdGroupCheckboxProps {
  label: string;
}
function adGroupCheckbox(props: AdGroupCheckboxProps): JSX.Element {
  const classes = useStyles(props);
  const { label } = props;
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedF: true
  });

  const handleChange = (name: string) => (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setState({ ...state, [name]: event.target.checked });
  };
  return (
    <FormControlLabel
      classes={{ label: classes.label }}
      control={
        <Checkbox
          checked={state.checkedA}
          onChange={handleChange("checkedA")}
          value="checkedA"
          classes={{ root: classes.checkox }}
          inputProps={{
            "aria-label": "ad groups selected"
          }}
        />
      }
      label={label}
    />
  );
}

export { adGroupCheckbox as AdGroupCheckbox };
