import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import * as React from "react";
import { AddGroupCellRow } from "../AdGroupCellRow/AddGroupCellRow.component";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cell: {
      borderRight: `1px solid ${theme.palette.grey[400]}`
    }
  })
);

interface AdGroupCellPops {
  rows: Array<{
    hasBorder: boolean;
    data: Array<{ value: string }>;
  }>;
  name: string;
  header?: any;
}
function adGroupCell(props: AdGroupCellPops): JSX.Element {
  const classes = useStyles(props);
  const { rows, name, header } = props;
  return (
    <div className={classes.cell}>
      {header}
      {rows.map((c, idx) => (
        <AddGroupCellRow
          key={`${name}__${idx}`}
          hasBorder={c.hasBorder}
          data={c.data}
        />
      ))}
    </div>
  );
}
adGroupCell.defaultProps = {
  header: null
};
export { adGroupCell as AdGroupCell };
