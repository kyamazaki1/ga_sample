import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import InputAdornment from "@material-ui/core/InputAdornment";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cell: {
      borderRight: `1px solid ${theme.palette.grey[400]}`
    },
    input: {
      width: "100%",
      height: 52
    }
  })
);

interface AdGroupInputProps {}
function adGroupInput(props: AdGroupInputProps): JSX.Element {
  const classes = useStyles(props);

  return (
    <form noValidate autoComplete="off" className={classes.cell}>
      <InputBase
        classes={{
          root: classes.input
        }}
        id="searchFilter"
        endAdornment={
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        }
      />
    </form>
  );
}

export { adGroupInput as AdGroupInput };
