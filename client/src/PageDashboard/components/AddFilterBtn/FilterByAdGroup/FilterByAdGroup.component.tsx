import * as React from "react";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { AdGroupInput } from "../FilterByPopover/AdGroupInput/AdGroupInput.component";
import { AdGroupCell } from "../FilterByPopover/AdGroupCell/AddGroupCell.component";
import { AdGroupCheckbox } from "../FilterByPopover/AdGroupCheckbox/AdGroupCheckbox.component";
import { AddGroupCellRow } from "../FilterByPopover/AdGroupCellRow/AddGroupCellRow.component";

const SELECTIONS = [
  {
    hasBorder: true,
    data: [{ value: "campaing 1" }]
  },
  {
    hasBorder: false,
    data: [{ value: "ad group 1" }, { value: "ad group  2" }]
  }
];
const RESULTS = [
  {
    hasBorder: false,
    data: [{ value: "no results" }]
  }
];
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    col: {
      width: "100%",
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row: {
      borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    row__borderless: {},
    cells: {
      display: "grid",
      gridTemplateColumns: "50% 50%"
    },
    card_content: {
      display: "grid",
      gridTemplateColumns: "67% 33%"
    }
  })
);

interface AddFilterCardProps {}
function filterByAdGroup(props: AddFilterCardProps): JSX.Element {
  const classes = useStyles(props);

  return (
    <section className={classes.card_content}>
      <main className={classes.col}>
        <div className={classes.row}>
          <AdGroupInput />
        </div>
        <div className={classes.row__borderless}>
          <div className={classes.cells}>
            <AdGroupCell rows={SELECTIONS} name="selection" />
            <AdGroupCell
              rows={RESULTS}
              name="results"
              header={
                <List className={classes.row} dense>
                  <ListItem dense>
                    <AdGroupCheckbox label="no ads group" />
                  </ListItem>
                </List>
              }
            ></AdGroupCell>
          </div>
        </div>
      </main>

      <aside className={classes.col}>
        <AddGroupCellRow hasBorder data={[{ value: "none selected" }]} />
        <AddGroupCellRow data={[{ value: "" }]} />
      </aside>
    </section>
  );
}

export { filterByAdGroup as FilterByAdGroup };
