import { AppState } from "@home/store";
import { StateObservableGeneric } from "@src/store/store.utils";
import { StoreState } from "@src/model";
export const INIT_APP_USER = {
  access_token_type: null,
  access_token: null,
  user_id: null,
  username: null,
  is_admin: false,
  business_id: null,
  expires: null
};
export const MOCK_APP_USER = {
  access_token_type: "mock_type",
  access_token: "mock_token",
  user_id: 0,
  username: "mockName",
  is_admin: false,
  business_id: 0,
  expires: new Date().toUTCString()
};
//token expries in 3 mins
export const MOCK_APP_USER_TOKEN_TO_REFRESH = {
  ...MOCK_APP_USER,
  expires: new Date(new Date().getTime() + 1000 * 240).toUTCString()
};

//token expries in 55 mins

export const MOCK_APP_USER_FRESH_TOKEN = {
  ...MOCK_APP_USER,
  expires: new Date(new Date().getTime() + 1000 * 2100).toUTCString()
};

export const UPDATED_APP_USER = {
  ...MOCK_APP_USER,
  username: "update"
};

export const UPDATED_TOKEN_USER = {
  ...MOCK_APP_USER,
  access_token: "update"
};

export const MOCK_APP_STATE: AppState = {
  theme: "dark",
  userDetail: null,
  business: null,
  user: MOCK_APP_USER
};
export const MOCK_LIGHT_APP_STATE: AppState = {
  theme: "light",
  userDetail: null,
  business: null,
  user: MOCK_APP_USER
};

export const MOCK_APP_STATE_FRESH: AppState = {
  theme: "dark",
  userDetail: null,
  business: null,
  user: MOCK_APP_USER_FRESH_TOKEN
};

export const MOCK_APP_STATE_TOKEN_TO_REFRESH: AppState = {
  theme: "dark",
  userDetail: null,
  business: null,
  user: MOCK_APP_USER_TOKEN_TO_REFRESH
};

export const MOCK_INIT_APP_STATE = {
  business: null,
  theme: "dark",
  user: {
    access_token: "mock_token",
    access_token_type: "mock_type",
    business_id: 0,
    expires: new Date().toUTCString(),
    is_admin: false,
    user_id: 0,
    username: "mockName"
  },
  userDetail: null
};

export const MOCK_STORE_STATE = {
  app: MOCK_APP_STATE
};
export const MOCK_STORE_STATE_FRESH = {
  app: MOCK_APP_STATE_FRESH
};

export const MOCK_STORE_STATE_TOKEN_TO_REFRESH = {
  app: MOCK_APP_STATE_TOKEN_TO_REFRESH
};

export const OBS_MOCK_APP_STATE: StateObservableGeneric<StoreState> = {
  value: MOCK_STORE_STATE
};
export const MOCK_AUTH: string = "Authorisation : Bearer Mock";
export const MOCK_ENDPOINT: string = "mock_endpoint";

export const BASE_INPUT_STATE = {
  valid: false,
  value: "",
  touched: false,
  validationType: [],
  errorDisplay: null
};
export const BASE_MOCK_FORM_CONTROL = {
  loginInput: BASE_INPUT_STATE,
  pwdInput: BASE_INPUT_STATE
};

export const MOCK_FORM_CTRL_STATE = {
  state: BASE_INPUT_STATE,
  setter: () => null,
  validations: ["required"]
};

export const MOCK_LOGIN_INPUT_STATE = {
  ...BASE_INPUT_STATE,
  validationType: ["required", "isEmail"]
};

export const MOCK_PWD_INPUT_STATE = {
  ...BASE_INPUT_STATE,
  validationType: ["required", "minLength"]
};

export const MOCK_CONFIRM_INPUT_STATE = {
  ...BASE_INPUT_STATE,
  validationType: ["required", "minLength"]
};

export const MOCK_INPUT_REF_STATE = {
  state: null,
  setter: function dispatchAction() {}.bind(this),
  validations: []
};

export const BASE_FORM_STATE = {
  loginInput: MOCK_LOGIN_INPUT_STATE,
  pwdInput: MOCK_PWD_INPUT_STATE,
  isFormValid: false,
  inputsRef: {
    loginInput: {
      ...MOCK_INPUT_REF_STATE,
      state: MOCK_LOGIN_INPUT_STATE,
      validations: ["required", "isEmail"]
    },
    pwdInput: {
      ...MOCK_INPUT_REF_STATE,
      state: MOCK_PWD_INPUT_STATE,
      validations: ["required", "minLength"]
    }
  },
  updateFormState: function updateFormState() {}
};

export const MOCK_APP_LIGHT_STATE = {
  app: {
    ...MOCK_APP_STATE,
    theme: "light"
  }
};
export const MOCKED_LOGGED_USER = {
  app: {
    ...MOCK_APP_STATE,
    user: MOCK_APP_USER
  }
};
