import styled from "styled-components";

export const Columns = styled.div.attrs({ className: "columns" })``;

export const VerticalColumns = styled(Columns)`
  flex: 1;
  height: 100vh;
  display: flex;
  flex-direction: column;
`;

export const ListWraper = styled.div`
  height: 100%;
  overflow: hidden;
`;

export const SpaceBetween = styled.div`
  display: flex;
  justify-content: space-between;
`;
