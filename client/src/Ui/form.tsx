import * as React from "react";
import styled from "styled-components";
import { makeStyles, Paper, withStyles } from "@material-ui/core";

export const VerticalForm = styled.form.attrs({})`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  flex: 1;
`;
export const FormGroupWrapper = styled.div.attrs({})<{ isValid: boolean }>`
  margin: 10px 0;
`;

export const FormHint = styled.p`
  color: red;
  font-size: 0.7rem;
  padding-top: 0.2rem;
  margin-bottom: 0;
  min-height: 20px;
  width: 100%;
  white-space: nowrap;
`;
