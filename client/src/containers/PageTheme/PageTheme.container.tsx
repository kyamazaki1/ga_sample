import { Container, withStyles } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import * as React from "react";
import { useSelector } from "react-redux";
import { StoreState } from "@src/model";

const StyledContainer = withStyles(({ palette }) => {
  return {
    root: {
      backgroundColor: palette.background.paper,
      padding: 0,
      display: "flex",
      flex: 1,
      flexDirection: "column",
      maxWidth: "100%"
    }
  };
})(Container);

export const STYLES = {
  primary: {
    light: "#757ce8",
    main: "#777777",
    dark: "#002884",
    contrastText: "#fff"
  },
  secondary: {
    light: "#ff7961",
    main: "#f44336",
    dark: "#ba000d",
    contrastText: "#000"
  }
};

export const APP_THEME = createMuiTheme({
  palette: {
    ...STYLES
  },
  typography: {
    h1: {
      fontSize: "25px",
      fontWeight: 500
    },
    h2: {
      fontSize: "22px",
      fontWeight: 500
    },
    caption: {}
  }
});

const darkTheme = createMuiTheme({
  ...APP_THEME,
  palette: {
    type: "dark",
    primary: {
      main: "#fff"
    }
  }
});
const lightTheme = createMuiTheme({
  ...APP_THEME,

  palette: {
    type: "light"
  }
});

let pageTheme = (props: any): JSX.Element => {
  const {} = props;

  const useTheme = useSelector((state: StoreState) => {
    return state.app.theme;
  });

  return (
    <ThemeProvider theme={useTheme === "light" ? lightTheme : darkTheme}>
      <StyledContainer>{props.children}</StyledContainer>
    </ThemeProvider>
  );
};

export { pageTheme as PageTheme };
