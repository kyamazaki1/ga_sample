import * as React from "react";
import { cleanup, getByText } from "@testing-library/react";

import { DashboardContainer } from "@src/routes";
import { renderWithTopReduxRouter } from "@src/mock/utils.mock";
import { INIT_APP_USER } from "@src/fixtures";

describe("Access to protected page", () => {
  afterEach(cleanup);

  test("It should renders App page on user token", () => {
    const { getByTestId, history } = renderWithTopReduxRouter(DashboardContainer, "/dashboard");
    expect(getByTestId("app-bar")).toBeTruthy();
  });

  test("It should not renders App page on user null", () => {
    const { getAllByTestId } = renderWithTopReduxRouter(DashboardContainer, "/dashboard", INIT_APP_USER);
    expect(getAllByTestId("login-input")).toBeTruthy();
  });
});
