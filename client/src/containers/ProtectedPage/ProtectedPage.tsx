import * as React from "react";
import { useState } from "react";
import { Redirect } from "react-router";
import { AppContainer } from "@components/AppContainer/AppContainer.component";
import { PageTheme } from "@containers/PageTheme/PageTheme.container";
import { PUBLIC_PATH } from "@src/routes";
import { AppTopBar } from "@components/AppTopBar/AppTopBar";
import { UserResponse } from "@model/user";
import { getPersistedToken } from "@store/store.utils";
import { ErrorBoundary } from "@src/components/ErrorBoundary/ErrotBoundary";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Fab from "@material-ui/core/Fab";
import { SideList } from "@src/components/SideList/SideList.component";
import Grid from "@material-ui/core/Grid";
import HomeIcon from "@material-ui/icons/Home";
import { PageNavLink } from "@src/components/NavSubList/NavSubList.component";
import { NavList } from "@src/components/NavLIst/NavList.component";

const OVERVIEW_NAV = [
  {
    path: "/overview",
    name: "overview",
    Icon: HomeIcon
  },
  {
    path: "/recommendations",
    name: "recommendations",
    Icon: null
  }
];

const CAMPAIGNS_NAV = [
  {
    path: "/campaigns",
    name: "campaigns",
    Icon: HomeIcon
  }
];
const SETTINGS_NAV = [
  {
    path: "/overview",
    name: "settings",
    Icon: null
  },
  {
    path: "/overview",
    name: "location",
    Icon: null
  }
];
const HISTORY_NAV = [
  {
    path: "/overview",
    name: "change history",
    Icon: null
  }
];
interface NavItem {
  list: Array<PageNavLink>;
  label: string;
}
export interface NavItems {
  overview: NavItem;
  campaigns: NavItem;
  settings: NavItem;
  history: NavItem;
}
const NAV_ITEMS: NavItems = {
  overview: {
    list: OVERVIEW_NAV,
    label: "overview"
  },
  campaigns: { list: CAMPAIGNS_NAV, label: "campaigns" },
  settings: { list: SETTINGS_NAV, label: "settings" },
  history: { list: HISTORY_NAV, label: "history" }
};

const APP_BAR_HEIGHT: number = 75;
export const DRAWER_WIDTH: number = 250;
export const BTN_OFFSET: number = 7;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex"
    },
    permanentBar: {
      position: "absolute",
      height: "100vh",
      top: APP_BAR_HEIGHT
    },
    hoverBar: {
      position: "relative",
      height: "100%",
      zIndex: 10
    },

    appBar: {
      backgroundColor: "transparent",
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    appBarShift: {
      width: `calc(100% - ${DRAWER_WIDTH}px)`,
      marginLeft: DRAWER_WIDTH,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      })
    },

    drawer: {
      width: DRAWER_WIDTH,
      flexShrink: 0
    },

    content: {
      flexGrow: 1,
      paddingLeft: theme.spacing(1),
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      marginLeft: 0
    },
    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      }),
      marginLeft: DRAWER_WIDTH
    },
    nav: {
      top: APP_BAR_HEIGHT,
      backgroundColor: "transparent",
      width: DRAWER_WIDTH + 30,
      borderRight: "transparent"
    },
    float_btn: {
      zIndex: 10,
      position: "absolute",
      width: 25,
      height: 25,
      minHeight: "inherit",
      top: "45%",
      transition: theme.transitions.create("opacity", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    content_main: {
      display: "grid",
      gridTemplateColumns: "145px auto"
    },
    main: {
      height: "100vh",
      overflow: "auto",
      display: "flex",
      flexDirection: "column"
    }
  })
);

interface ProtectedPage {
  user: UserResponse;
  path: string;
  component: React.Component;
  exact?: boolean;
}

function setHoverStyle(predicat: boolean, navWdith) {
  return predicat
    ? {
        bar: {
          backgroundColor: null
        },
        btn: {
          left: -BTN_OFFSET
        }
      }
    : {
        bar: {
          backgroundColor: "#424242"
        },
        btn: {
          left: navWdith - BTN_OFFSET
        }
      };
}

let protectedPage = ({ component: Component, ...rest }): JSX.Element => {
  const { path, user } = rest;
  const classes = useStyles({});

  const [open, setOpen] = useState(false);

  function onToggleNav(b: boolean): void {
    setOpen(b);
  }
  const initialSideListWidth: number = 5;
  const [usePeakNavWidth, setUsePeakNavWidth] = useState(initialSideListWidth);

  function isHovered(appWidth: 5 | 30): void {
    setUsePeakNavWidth(appWidth);
  }

  //in case of refresh, user is null so try to get for persisted stores
  if (!user.access_token && !getPersistedToken()) {
    return <Redirect to={PUBLIC_PATH} />;
  }

  const hoveredStyle = setHoverStyle(
    usePeakNavWidth === initialSideListWidth,
    usePeakNavWidth
  );

  return (
    <ErrorBoundary>
      <PageTheme>
        <AppTopBar user={user} toggleNav={onToggleNav} slideState={open} />

        <div
          onMouseEnter={() => isHovered(30)}
          onMouseLeave={() => isHovered(initialSideListWidth as 5)}
          className={classes.permanentBar}
          style={{
            width: usePeakNavWidth
          }}
        >
          <div className={classes.hoverBar} style={hoveredStyle.bar}></div>

          <Fab
            color="primary"
            aria-label="open menu"
            className={classes.float_btn}
            style={hoveredStyle.btn}
            onClick={() => onToggleNav(true)}
          >
            <ChevronRightIcon />
          </Fab>
        </div>

        <Drawer
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open
          })}
          classes={{
            paper: classes.nav
          }}
          variant="persistent"
          anchor="left"
          open={open}
        >
          <SideList toggleNav={onToggleNav} slideState={open} />
        </Drawer>
        <div
          className={clsx(classes.content, {
            [classes.contentShift]: open
          })}
        >
          <Grid className={classes.content_main}>
            <NavList navItems={NAV_ITEMS} />
            <main className={classes.main}>
              <AppContainer exact path={path} component={Component} />
            </main>
          </Grid>
        </div>
      </PageTheme>
    </ErrorBoundary>
  );
};

export default protectedPage;
