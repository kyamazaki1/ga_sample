import { combineEpics } from "redux-observable";
import { combineReducers } from "redux";
import {
  AppReducer,
  clearTokenEpic$,
  userDetailEpic$,
  businessDetailEpic$
} from "@home/store";
import { DashboardReducer, fetchSample$ } from "@src/PageDashboard/store";

export const rootEpic = combineEpics(
  clearTokenEpic$,
  userDetailEpic$,
  businessDetailEpic$,
  fetchSample$
);

export const rootReducer = combineReducers({
  app: AppReducer,
  dashboard: DashboardReducer
});
