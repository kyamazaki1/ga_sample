import {
  getState$Token,
  makeTokenReq,
  _updateToken,
  getPersistedToken,
  getPersistedExp,
  getPersistedUser,
  tryUserAuth,
  revokeUserToken,
  autoRefreshGet,
  logout,
  clearPersistToken
} from "./store.utils";
import { AjaxResponse, AjaxRequest, ajax } from "rxjs/ajax";
import {
  OBS_MOCK_APP_STATE,
  MOCK_AUTH,
  MOCK_ENDPOINT,
  MOCK_APP_USER,
  MOCK_STORE_STATE,
  MOCK_STORE_STATE_FRESH,
  MOCK_STORE_STATE_TOKEN_TO_REFRESH
} from "@src/fixtures";
import { TestScheduler } from "rxjs/testing";
import { of } from "rxjs";
import {
  setAppUserAction,
  trySetUserDetailAction,
  refreshUserTokenAction,
  setUserDetailAction,
  tryClearUserTokenAction,
  clearUserTokenAction
} from "@home/store";
import { history } from "@src/route-history";
import { PUBLIC_PATH } from "@src/routes";

describe("sync store utils", () => {
  afterEach(() => jest.clearAllMocks());
  test("return tokens from state observable", () => {
    // mock out
    const [type, token] = getState$Token(OBS_MOCK_APP_STATE as any);
    expect(type).toEqual(OBS_MOCK_APP_STATE.value.app.user.access_token_type);
    expect(token).toEqual(OBS_MOCK_APP_STATE.value.app.user.access_token);
  });

  test("return Ajax get request", () => {
    // mock out
    const EXPECTED_PAYLOAD = {
      headers: {
        Authorization: MOCK_AUTH
      },
      method: "GET",
      url: MOCK_ENDPOINT
    };
    const payload: AjaxRequest = makeTokenReq(MOCK_AUTH, MOCK_ENDPOINT);

    expect(payload).toMatchObject(EXPECTED_PAYLOAD);
  });

  test("return Ajax post request", () => {
    // mock out
    const MOCK_PAYLOAD = "mock payload";
    const EXPECTED_PAYLOAD = {
      headers: {
        Authorization: MOCK_AUTH
      },
      method: "POST",
      url: MOCK_ENDPOINT,
      body: MOCK_PAYLOAD
    };
    const payload: AjaxRequest = makeTokenReq(MOCK_AUTH, MOCK_ENDPOINT, "POST", MOCK_PAYLOAD);

    expect(payload).toMatchObject(EXPECTED_PAYLOAD);
  });

  test("update Ajax request Authorisation", () => {
    // mock out
    let INITIAL_PAYLOAD = {
      headers: {
        Authorization: MOCK_AUTH
      },
      method: "GET",
      url: MOCK_ENDPOINT
    };
    const EXPECTED_PAYLOAD = {
      ...INITIAL_PAYLOAD,
      headers: {
        Authorization: `${MOCK_APP_USER.access_token_type} ${MOCK_APP_USER.access_token}`
      }
    };

    const payload: AjaxRequest = _updateToken(INITIAL_PAYLOAD, MOCK_APP_USER);

    expect(payload).toMatchObject(EXPECTED_PAYLOAD);
  });

  test("return appstate token ", () => {
    const result = getPersistedToken(MOCK_STORE_STATE);

    expect(result).toBe(MOCK_STORE_STATE.app.user.access_token);
  });
  test("return appstate expire time ", () => {
    const result = getPersistedExp(MOCK_STORE_STATE);

    expect(result).toBe(MOCK_STORE_STATE.app.user.expires);
  });
  test("return appstate user ", () => {
    const result = getPersistedUser(MOCK_STORE_STATE);
    expect(result).toMatchObject(MOCK_STORE_STATE.app.user);
  });
});
// test async tu
describe("async store utils", () => {
  afterEach(() => jest.clearAllMocks());

  it("should return user info", done => {
    const testScheduler = new TestScheduler((actual, expected) => {
      // somehow assert the two objects are equal
      // e.g. with chai `expect(actual).deep.equal(expected)`
      const serverResp = actual[0].notification.value;
      expect(serverResp).toMatchObject(MOCK_APP_USER);

      expect(actual.length).toBe(2);
    });

    testScheduler.run(({ hot, expectObservable }) => {
      const mockAjax = () => of({ response: MOCK_APP_USER });
      const output$ = tryUserAuth({ email: "babou", password: "valid" }, mockAjax as any);

      expectObservable(output$).toBe("-a", {
        a: { response: MOCK_APP_USER }
      });

      done();
    });
  });

  it("should clear user token", done => {
    const MOCK_REPONSE = { response: "success" };
    const testScheduler = new TestScheduler((actual, expected) => {
      const serverResp = actual[0].notification.value;
      expect(serverResp).toBe(MOCK_REPONSE.response);
      expect(actual.length).toBe(2);
    });

    testScheduler.run(({ hot, expectObservable }) => {
      const mockAjax = () => of(MOCK_REPONSE);
      const output$ = revokeUserToken({ access_token: "babou", access_token_type: "valid" }, mockAjax as any);
      expectObservable(output$).toBe("-a", {
        a: { response: MOCK_REPONSE }
      });

      done();
    });
  });
  // tests fails because of weird bugs

  /*   it("should dispatch clear token before action on token below 0", done => {
    const MOCK_REPONSE = { response: "token" };
    const testScheduler = new TestScheduler((actual, expected) => {
      const Dispatched = actual[0].notification.value;
      expect(Dispatched).toMatchObject(clearUserTokenAction());
      expect(actual.length).toBeGreaterThanOrEqual(2);
    });
    testScheduler.run(({ hot, expectObservable }) => {
      const mockAjax = () => of(MOCK_REPONSE);
      const storeState = MOCK_STORE_STATE;
      const state$ = { value: storeState };
      const output$ = autoRefreshGet(state$, "mock_url", trySetUserDetailAction(), mockAjax as any);
      expectObservable(output$).toBe("-a", {
        a: { response: MOCK_REPONSE }
      });
      done();
    });
  }); */

  it("should dispatch refresh token before action on token below treshold", done => {
    const MOCK_REPONSE = { response: "token" };
    const testScheduler = new TestScheduler((actual, expected) => {
      const Dispatched = actual[0].notification.value;
      expect(Dispatched).toMatchObject(refreshUserTokenAction("token"));
      expect(actual.length).toBeGreaterThanOrEqual(2);
    });
    testScheduler.run(({ hot, expectObservable }) => {
      const mockAjax = () => of(MOCK_REPONSE);
      const storeState = MOCK_STORE_STATE_TOKEN_TO_REFRESH;
      const state$ = { value: storeState };
      const output$ = autoRefreshGet(state$, "mock_url", trySetUserDetailAction, mockAjax as any);
      expectObservable(output$).toBe("-a", {
        a: { response: MOCK_REPONSE }
      });
      done();
    });
  });

  it("should dispatch action on token above treshold", done => {
    const MOCK_REPONSE = { response: "token" };
    const testScheduler = new TestScheduler((actual, expected) => {
      const tkDispatch = actual[0].notification.value;
      expect(tkDispatch).toMatchObject({ type: "TRY_USER_DETAIL", payload: null });
      expect(actual.length).toBeGreaterThanOrEqual(1);
    });

    testScheduler.run(({ hot, expectObservable }) => {
      const mockAjax = () => of(MOCK_REPONSE);
      const storeState = MOCK_STORE_STATE_FRESH;
      const state$ = { value: storeState };
      const output$ = autoRefreshGet(state$, "mock_url", trySetUserDetailAction, mockAjax as any);
      expectObservable(output$).toBe("-a", {
        a: { response: MOCK_REPONSE }
      });

      done();
    });
  });

  it("should dispatch logout action", done => {
    const ACTION = tryClearUserTokenAction({
      access_token: MOCK_STORE_STATE.app.user.access_token,
      access_token_type: MOCK_STORE_STATE.app.user.access_token_type
    });
    const testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });

    testScheduler.run(({ hot, expectObservable }) => {
      logout();
      const action$ = hot("-a", {
        a: ACTION
      });

      expectObservable(action$).toBe("-a", {
        a: ACTION
      });
    });
    done();
  });

  it("should redirect to public path", async () => {
    await clearPersistToken().then(res => {
      expect(history.length).toBeGreaterThanOrEqual(2);
      expect(history.location.pathname).toBe(PUBLIC_PATH);
      expect(res).toBe("storage cleared");
    });
  });
});
