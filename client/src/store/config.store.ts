import { createStore, applyMiddleware, compose } from "redux";
import { createEpicMiddleware } from "redux-observable";
import { rootEpic, rootReducer } from "./reducers.root";

import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
///redux persist test error
//https://stackoverflow.com/questions/48056631/syntaxerror-unexpected-token-import-with-jest/48056912#48056912
import autoMergeLevel1 from "redux-persist/lib/stateReconciler/autoMergeLevel1";

const epicMiddleware = createEpicMiddleware();
const composeEnhancers = window["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__"] || compose;

const persistConfig = {
  key: "root",
  storage,
  stateReconciler: autoMergeLevel1, //see state reconciler in official doc
  blacklist: [] // reducer name
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
/**
 * config store with persist in local storage (persistor)
 *
 * @export
 * @returns
 */
export default function configureStore() {
  const store = createStore(persistedReducer, composeEnhancers(applyMiddleware(epicMiddleware)));
  epicMiddleware.run(<any>rootEpic);

  let persistor = persistStore(store);

  return { store, persistor };
}
