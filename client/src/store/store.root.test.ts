import { reducer, INITIAL_STORE_STATE } from "./const";

test("return initial store state", () => {
  const expected = reducer(INITIAL_STORE_STATE, null);

  expect(expected).toMatchObject(INITIAL_STORE_STATE);
});
