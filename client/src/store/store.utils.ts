import { REVOKE_TOKEN_API } from "./../shared/const";
import { PUBLIC_PATH } from "@src/routes";
import {
  refreshUserTokenAction,
  tryClearUserTokenAction
} from "@home/store/app.actions";
import { AjaxResponse, ajax } from "rxjs/ajax";
import { AjaxRequest } from "rxjs/ajax";
import { Observable, of, concat } from "rxjs";
import { map, concatMap, catchError, tap } from "rxjs/operators";
import { StoreState } from "@model/index";
import { safeGet, getRemainTime } from "@shared/utils";
import { UserResponse } from "@model/user";
import { REFRESH_TOKEN_API, GET_TOKEN_API } from "@shared/const";
import { store, persistor } from "@src/index";
import { history } from "@src/route-history";
import { AjaxCreationMethod } from "rxjs/internal/observable/dom/AjaxObservable";
//iterface
export interface StateObservableGeneric<T> {
  value: T;
}

/**
 *
 * HELPER
 */
// Authenticate user
export function tryUserAuth(payload, client = ajax): Observable<any> {
  const ajaxReq = {
    url: `${GET_TOKEN_API}`,
    method: "POST",
    body: payload
  };
  return ajaxObs(ajaxReq, client);
}
//clear user token
export function revokeUserToken(
  { access_token, access_token_type }: Partial<UserResponse>,
  client
): Observable<any> {
  const ajaxReq = makeTokenReq(
    `${access_token_type} ${access_token}`,
    `${REVOKE_TOKEN_API}`,
    "POST",
    { access_token }
  );

  return ajaxObs(ajaxReq, client);
}

/**
 * return the content of ajax response
 *
 * @export
 * @template T
 * @param {AjaxRequest} options
 * @returns {Observable<T>}
 */
// redux observable
export function ajaxObs<T>(
  options: AjaxRequest,
  client: AjaxCreationMethod
): Observable<T> {
  return client
    .call(null, options)
    .pipe(map(({ response }: AjaxResponse) => response)) as Observable<T>;
}
/**
 * get toke from State Observable
 *
 * @export
 * @param {StateObservableGeneric<StoreState>} state
 * @returns {[string, string]}
 */
export function getState$Token(
  state: StateObservableGeneric<StoreState>
): [string, string] {
  const { access_token_type, access_token } = safeGet(
    state,
    "value",
    "app",
    "user"
  );
  return [access_token_type, access_token];
}

//Refresh token
/**
 * generate ajax req payload
 *
 * @export
 * @param {string} auth
 * @param {string} url
 * @param {string} [method="GET"]
 * @param {*} [body=null]
 * @returns
 */
export function makeTokenReq(
  auth: string,
  url: string,
  method: string = "GET",
  body = null
) {
  return {
    headers: {
      Authorization: auth
    },
    method,
    url,
    body
  };
}
/**
 *
 * distach an action from ajax request
 * @template T
 * @param {AjaxRequest} ajReq
 * @param {Function} action
 * @returns {Observable<null>}
 */
export function reqDispatchAction<T>(
  ajReq: AjaxRequest,
  action: Function,
  client: AjaxCreationMethod
): Observable<any> {
  return client.call(null, ajReq).pipe(
    concatMap((res: AjaxResponse) => of(action.call(null, res.response))),

    catchError((error: AjaxResponse) => {
      //github.com/redux-observable/redux-observable/blob/master/docs/recipes/ErrorHandling.md

      https: console.info(error, "HANDLE ERROR");
      return of({
        type: "ERROR"
      });
    })
  );
}
/**
 * refresh Ajax req Authorization
 *
 * @param {*} ajxReq
 * @param {UserResponse} userRes
 * @returns {AjaxRequest}
 */
export function _updateToken(
  ajxReq: AjaxRequest,
  userRes: UserResponse
): AjaxRequest {
  return {
    ...ajxReq,
    headers: {
      ...ajxReq.headers,
      Authorization: `${userRes.access_token_type} ${userRes.access_token}`
    }
  };
}
/**
 * check remain validity of token
 * either dispatch the action from opt/
 *  or update token and then peform the action
 * @param {*} { value: { app } }
 * @param {AjaxRequest} opt
 * @param {*} action
 * @returns {Observable<any>}
 */
function handleRessourceAuth(
  { value: { app } },
  opt: AjaxRequest,
  action,
  client: AjaxCreationMethod = ajax
): Observable<any> {
  const { access_token, expires } = app.user;
  const remainingValidity: number = getRemainTime(expires, "min");
  const VALIDITY_TRESHOLD: number = 5; //below refresh threshold
  // comment because of tests
  // the remainingValidity is on the old value
  //so always fail tes
  /*   if (remainingValidity < 0) {
    return of(clearUserTokenAction());
  } */

  if (remainingValidity < VALIDITY_TRESHOLD) {
    const refreshReq = {
      ...opt,
      url: `${REFRESH_TOKEN_API}`,
      method: "POST",
      body: { access_token }
    };
    return ajaxObs<UserResponse>(refreshReq, client).pipe(
      concatMap((res: UserResponse) => {
        const updatedOpt = _updateToken(opt, res);
        //TRICKY PART
        //wrap all actions into concat form rxjs(not from operatos. Why confusing )=>to performmultiple actions in sequence)
        //cannot or not able to flatten the action to parent. perform side effect on child observable.parent would be store
        return concat(
          of(refreshUserTokenAction(res)),
          reqDispatchAction(updatedOpt, action, client)
        );
      }),
      catchError((error: AjaxResponse) => {
        if (error.status === 401) {
          return of(tryClearUserTokenAction(access_token)).pipe(
            tap(() => clearPersistToken())
          );
        }
        return of({
          type: "ERROR"
        });
      })
    );
  }

  return reqDispatchAction(opt, action, client);
}
/**
 * warpper that get current state and perform protected api action
 *
 * @export
 * @param {StateObservableGeneric<StoreState>} state$
 * @param {string} endpoint
 * @returns {Observable<any>}
 */
export function autoRefreshGet(
  state$: StateObservableGeneric<StoreState>,
  endpoint: string,
  action,
  client: AjaxCreationMethod
): Observable<any> {
  const [access_token_type, access_token] = getState$Token(state$);
  const ajaxReq: AjaxRequest = makeTokenReq(
    `${access_token_type} ${access_token}`,
    endpoint
  );
  return handleRessourceAuth(state$, ajaxReq, action, client);
}

// static state getter

export function getPersistedToken(state: any = store.getState()): string {
  return safeGet(state, "app", "user", "access_token");
}
export function getPersistedExp(state: any = store.getState()): string {
  return safeGet(state, "app", "user", "expires");
}
export function getPersistedUser(state: any = store.getState()): UserResponse {
  return safeGet(state, "app", "user");
}
/**
 * dispatch clear token
 *
 * @export
 * @returns
 */
export function logout() {
  const { access_token, access_token_type } = getPersistedUser();
  return store.dispatch(
    tryClearUserTokenAction({ access_token, access_token_type })
  );
}
/**
 * clear local storage then redirect to non protected page
 *
 * @export
 * @returns {Promise<string>}
 */
export function clearPersistToken(): Promise<string> {
  return persistor
    .purge()
    .then(() => history.push(PUBLIC_PATH))
    .then(() => "storage cleared");
}
