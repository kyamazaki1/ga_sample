import { DispatchAction, ValidPayload } from "./../model/store";
import { StoreState } from "@model/index";

export const INITIAL_STORE_STATE: StoreState = {
  app: null,
  dashboard: null
};
export function reducer(state: StoreState, action: DispatchAction<ValidPayload>): StoreState {
  return INITIAL_STORE_STATE;
}
