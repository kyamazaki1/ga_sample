import * as React from "react";

import { storiesOf } from "@storybook/react";

import { muiTheme } from "storybook-addon-material-ui";

import { ThemeToggle } from "./../../src/components/ThemeToggle/ThemeToggle.component";
import Provider from "./../helper/Provider";
import configureStore from "@src/store/config.store";
import {
  PageTheme,
  APP_THEME
} from "@src/containers/PageTheme/PageTheme.container";
import { MOCK_APP_STATE } from "@src/fixtures";
const { store, persistor } = configureStore();

const withProvider = story => <Provider store={store}>{story()}</Provider>;

const props: any = MOCK_APP_STATE;
storiesOf("Ui", module)
  .addDecorator(withProvider)
  .addDecorator(muiTheme([APP_THEME]))
  .add(
    "theme light ",
    () => (
      <PageTheme>
        <ThemeToggle />
      </PageTheme>
    ),
    { info: { inline: true } }
  )
  .add(
    "theme dark",
    () => (
      <PageTheme>
        <ThemeToggle />
      </PageTheme>
    ),
    { info: { inline: true } }
  );
