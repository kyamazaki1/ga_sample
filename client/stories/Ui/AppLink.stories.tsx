import * as React from "react";
import { AppLink } from "../../src/components/AppLink/AppLink";
import { history } from "../../src/route-history";
import { storiesOf } from "@storybook/react";
import { Router } from "react-router";

const props: any = {
  path: "/app/",
  name: "app"
};

storiesOf("Ui", module).add(
  "navigation link",
  () => (
    <Router history={history}>
      <AppLink name={props.name} path={props.path} />
    </Router>
  ),
  { info: { inline: true } }
);
