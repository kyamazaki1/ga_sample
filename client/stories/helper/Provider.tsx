import * as React from "react";
import { Router } from "react-router";
import { Provider } from "react-redux";
import { history } from "@src/route-history";

const ProviderWrapper = ({ children, store }) => (
  <Provider store={store}>
    <Router history={history}>{children}</Router>
  </Provider>
);

export default ProviderWrapper;
