import * as React from "react";

import { storiesOf } from "@storybook/react";
import { FormGroupWrapper, FormHint } from "@ui/form";
import { FormInput } from "@src/components/FormInput/FormInput.component";
import { ERROR_MSG } from "@src/shared/utils";
import { FormGrp } from "@src/components/FormGrp/FormGrp";

const LoginInputDetail = { name: "loginInput", type: "email", placeholder: "login" };

function mockHandleUpdate(e) {}
const LoginFormGrp = props => (
  <FormGrp<string>
    spec={{
      id: "login-input",
      detail: { name: "loginInput", type: "email", placeholder: "login" },
      type: "email",
      hintTestId: "input-error"
    }}
    handleUpdate={mockHandleUpdate}
    isValid={true}
    hint={null}
    value={null}
    {...props}
  />
);

const PwFormGrp = props => (
  <FormGrp<string>
    spec={{
      id: "pwd-input",
      detail: { name: "pwdInput", type: "password", placeholder: "password" },
      type: "password",
      hintTestId: "pwd-error"
    }}
    handleUpdate={mockHandleUpdate}
    isValid={true}
    hint={null}
    value={null}
    {...props}
  />
);

storiesOf("Signin", module)
  .add("Login input initial", () => <LoginFormGrp isValid={false} />, { info: { inline: true } })
  .add(
    "Login input is empty",
    () => <LoginFormGrp isValid={false} hint={ERROR_MSG.required} />,

    { info: { inline: true } }
  )
  .add(
    "Login input has invalid mail",
    () => <LoginFormGrp value="mock" isValid={false} hint={ERROR_MSG.isEmail} />,

    { info: { inline: true } }
  )

  .add("Login input valid", () => <LoginFormGrp value="mock@mock.com" />, { info: { inline: true } })
  .add(
    "Password input initial",
    () => <PwFormGrp isValid={false} />,

    { info: { inline: true } }
  )
  .add(
    "Password input invalid",

    () => <PwFormGrp isValid={false} hint={ERROR_MSG.required} />,

    { info: { inline: true } }
  )
  .add("Password input length", () => <PwFormGrp value="mock" isValid={false} hint={ERROR_MSG.minLength} />, {
    info: { inline: true }
  })
  .add("Password input valid", () => <PwFormGrp value="mockmockmock" />, { info: { inline: true } });
