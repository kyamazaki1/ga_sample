import * as React from "react";

import { storiesOf } from "@storybook/react";

import { LoginContainer } from "@src/routes";
import { LoginForm } from "@login/components/LoginForm/LoginForm.component";
const INITIAL_STATE = {
  loginType: "sigin",
  tryAuth: () => {},
  resetValidationError: () => {},
  validState: false,
  id: "login-form",
  formTouched: false,
  formValidation: () => null
};
storiesOf("Signin", module)
  .add(
    "initial form",
    () => (
      <LoginForm
        loginType={INITIAL_STATE.loginType}
        tryAuth={INITIAL_STATE.tryAuth}
        resetValidationError={INITIAL_STATE.resetValidationError}
        validState={INITIAL_STATE.validState}
        id={INITIAL_STATE.id as any}
        formTouched={INITIAL_STATE.formTouched}
        formValidation={INITIAL_STATE.formValidation}
      />
    ),
    { info: { inline: true } }
  )
  .add(
    "active form",
    () => (
      <LoginForm
        loginType={INITIAL_STATE.loginType}
        tryAuth={() => true}
        resetValidationError={INITIAL_STATE.resetValidationError}
        validState={true}
        id={INITIAL_STATE.id as any}
        formTouched={true}
        formValidation={INITIAL_STATE.formValidation}
      />
    ),
    { info: { inline: true } }
  );
