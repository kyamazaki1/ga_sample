import * as React from "react";

import { storiesOf } from "@storybook/react";
import { FormSubmitBtn } from "@src/components/FormSubmitBtn/FormSubmitBtn";

storiesOf("Signin", module)
  .add("submit button initial", () => <FormSubmitBtn state="invalid" isInvalid />, { info: { inline: true } })
  .add("submit  button pending", () => <FormSubmitBtn state="pending" isInvalid />, { info: { inline: true } })
  .add("submit  button success", () => <FormSubmitBtn state="success" isInvalid={false} />, { info: { inline: true } })
  .add("submit  button error", () => <FormSubmitBtn state="error" isInvalid />, { info: { inline: true } });
