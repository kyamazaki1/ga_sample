import * as React from "react";

import { storiesOf } from "@storybook/react";
import { LoginContainer } from "@src/routes";

storiesOf("Signin", module).add("page", () => <LoginContainer />, { info: { inline: true } });
