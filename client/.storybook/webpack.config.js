const path = require("path");

module.exports = ({ config, mode }) => {
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    loader: require.resolve("babel-loader"),
    options: {
      presets: [["react-app", { flow: false, typescript: true }]]
    }
  });
  config.resolve.extensions = [".ts", ".tsx", ".js"];
  config.resolve.alias = {
    "@src": path.resolve(__dirname, "../src"),
    "@components": path.resolve(__dirname, "../src", "components"),
    "@containers": path.resolve(__dirname, "../src", "containers"),
    "@model": path.resolve(__dirname, "../src", "model"),
    "@store": path.resolve(__dirname, "../src", "store"),
    "@shared": path.resolve(__dirname, "../src", "shared"),
    "@hooks": path.resolve(__dirname, "../src", "hooks"),
    "@ui": path.resolve(__dirname, "../src", "Ui"),
    "@mock": path.resolve(__dirname, "../src", "mock"),
    "@fixtures": path.resolve(__dirname, "../src", "fixtures"),
    "@redux": path.resolve(__dirname, "../src", "redux"),
    "@home": path.resolve(__dirname, "../src", "PageHome"),
    "@login": path.resolve(__dirname, "../src", "PageLogin")
  };
  return config;
};
