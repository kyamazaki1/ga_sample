import { AuthGuard } from './../auth.guard';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Controller, Get, Res, HttpStatus, UseGuards } from '@nestjs/common';
import { SampleService } from './sample.service';
import { Response } from 'express';

@Controller('api/sample')
//@UseGuards(AuthGuard)
export class SampleController {
  constructor(private _sampleService: SampleService) {}

  @Get('all')
  fetch(@Res() res: Response): Observable<any> {
    return this._sampleService.fetch().pipe(
      map(r =>
        res
          .status(HttpStatus.OK)
          .header({ status: 200, 'Access-Control-Allow-Origin': '*' })
          .json({
            data: r,
          }),
      ),
    );
  }
}
