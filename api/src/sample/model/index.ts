export interface AnalyticsTotals {
  visits: string;
  hits: string;
  pageviews: string;
  timeOnSite: any;
  bounces: string;
  transactions: string;
  transactionRevenue: string;
  newVisits: string;
  screenviews: string;
  uniqueScreenviews: string;
  timeOnScreen: any;
  totalTransactionRevenue: string;
  sessionQualityDim: string;
}

export interface TrafficSource {
  referralPath: string;
  campaign: string;
  source: string;
  medium: string;
  keyword: string;
  adContent: string;
  adwordsClickInfo: {
    campaignId: string;
    adGroupId: string;
    creativeId: string;
    criteriaId: string;
    page: string;
    slot: string;
    criteriaParameters: string;
    gclId: string;
    customerId: string;
    adNetworkType: string;
    targetingCriteria: string;
    isVideoAd: boolean;
  };
  isTrueDirect: boolean;
  campaignCode: string;
}

export interface adwordsClickInfo {
  campaignId: string;
  adGroupId: string;
  creativeId: string;
  criteriaId: string;
  page: string;
  slot: string;
  criteriaParameters: string;
  gclId: string;
  customerId: string;
  adNetworkType: string;
  targetingCriteria: string;
  isVideoAd: boolean;
}
export interface Device {
  browser: string;
  browserVersion: string;
  browserSize: string;
  operatingSystem: string;
  operatingSystemVersion: string;
  isMobile: boolean;
  mobileDeviceBranding: string;
  mobileDeviceModel: string;
  mobileInputSelector: string;
  mobileDeviceInfo: string;
  mobileDeviceMarketingName: string;
  flashVersion: string;
  javaEnabled: boolean;
  language: string;
  screenColors: string;
  screenResolution: string;
  deviceCategory: string;
}
export interface GeoNetwork {
  continent: string;
  subContinent: string;
  country: string;
  region: string;
  metro: string;
  city: string;
  cityId: string;
  networkDomain: string;
  latitude: string;
  longitude: string;
  networkLocation: string;
}

export interface Page {
  pagePath: string;
  hostname: string;
  pageTitle: string;
  searchKeyword: string;
  searchCategory: string;
  pagePathLevel1: string;
  pagePathLevel2: string;
  pagePathLevel3: string;
  pagePathLevel4: string;
}
export interface Transaction {
  transactionId: string;
  transactionRevenue: string;
  transactionTax: string;
  transactionShipping: string;
  affiliation: string;
  currencyCode: string;
  localTransactionRevenue: string;
  localTransactionTax: string;
  localTransactionShipping: string;
  transactionCoupon: string;
}

export interface Item {
  transactionId: string;
  productName: string;
  productCategory: string;
  productSku: string;
  itemQuantity: string;
  itemRevenue: string;
  currencyCode: string;
  localItemRevenue: string;
}

export interface AppInfo {
  name: string;
  version: string;
  id: string;
  installerId: string;
  appInstallerId: string;
  appName: string;
  appVersion: string;
  appId: string;
  screenName: string;
  landingScreenName: string;
  exitScreenName: string;
  screenDepth: string;
}

export interface ExceptionInfo {
  description: string;
  isFatal: boolean;
  exceptions: string;
  fatalExceptions: string;
}

export interface Social {
  socialInteractionNetwork: string;
  socialInteractionAction: string;
  socialInteractions: string;
  socialInteractionTarget: string;
  socialNetwork: string;
  uniqueSocialInteractions: string;
  hasSocialSourceReferral: string;
  socialInteractionNetworkAction: string;
}
export interface Product {
  productSKU: string;
  v2ProductName: string;
  v2ProductCategory: string;
  productVariant: string;
  productBrand: string;
  productRevenue: string;
  localProductRevenue: string;
  productPrice: string;
  localProductPrice: string;
  productQuantity: string;
  productRefundAmount: string;
  localProductRefundAmount: string;
  isImpression: boolean;
  isClick: boolean;
  customDimensions: Array<any>;
  customMetrics: Array<any>;
  productListName: string;
  productListPosition: string;
  productCouponCode: string;
}

export interface ECommerceAction {
  action_type: string;
  step: string;
  option: unknown;
}

export interface ContentGroup {
  contentGroup1: string;
  contentGroup2: string;
  contentGroup3: string;
  contentGroup4: string;
  contentGroup5: string;
  previousContentGroup1: string;
  previousContentGroup2: string;
  previousContentGroup3: string;
  previousContentGroup4: string;
  previousContentGroup5: string;
  contentGroupUniqueViews1: string;
  contentGroupUniqueViews2: string;
  contentGroupUniqueViews3: string;
  contentGroupUniqueViews4: string;
  contentGroupUniqueViews5: string;
}
export interface Hit {
  hitNumber: string;
  time: string;
  hour: string;
  minute: string;
  isSecure: boolean;
  isInteraction: boolean;
  isEntrance: boolean;
  isExit: boolean;
  referer: string;
  page: Page;
  transaction: Transaction;
  item: Item;
  contentInfo: string;
  appInfo: AppInfo;
  exceptionInfo: ExceptionInfo;
  product: Array<Product>;
  promotion: Array<unknown>;
  promotionActionInfo: unknown;
  refund: unknown;
  eCommerceAction: ECommerceAction;
  experiment: Array<unknown>;
  publisher: string;
  customVariables: Array<unknown>;
  customDimensions: Array<unknown>;
  customMetrics: Array<unknown>;
  type: string;
  social: Social;
  latencyTracking: string;
  sourcePropertyInfo: string;
  contentGroup: ContentGroup;
  dataSource: string;
  publisher_infos: Array<unknown>;
  eventInfo: string;
}
export interface AnalyticsApi {
  visitorId: string;
  visitNumber: string;
  visitId: string;
  visitStartTime: string;
  date: string;
  totals: AnalyticsTotals;
  trafficSource: TrafficSource;
  device: Device;
  geoNetwork: GeoNetwork;
  customDimensions: Array<any>;
  hits: Array<Hit>;
  fullVisitorId: string;
  userId: string;
  clientId: string;
  channelGrouping: string;
  socialEngagementType: string;
}
