import { Injectable } from '@nestjs/common';
import { MOCK } from './mock/google_sample';
import { of, Observable } from 'rxjs';
import { AnalyticsApi } from './model';
@Injectable()
export class SampleService {
  constructor() {}

  fetch(): Observable<Array<AnalyticsApi>> {
    return of(MOCK);
  }
}
